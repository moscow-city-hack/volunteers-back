if [ "$#" -ne 1 ]; then
    echo "argument 1 not found"
    exit 1
fi
docker build . -t cr.yandex/crpa3ig70k5nf0mofvlq/volunteers-back:$1 && \
docker push cr.yandex/crpa3ig70k5nf0mofvlq/volunteers-back:$1 && \
yc compute instance update-container epd34l59gvgkk2bbo67n --container-image=cr.yandex/crpa3ig70k5nf0mofvlq/volunteers-back:$1
