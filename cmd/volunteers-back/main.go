package main

import (
	"context"
	"main/internal/api/handler"
	"main/internal/api/server"
	"main/internal/app/store"
	"os"
	"os/signal"
	"sync"
	"time"
)

func ServeApp(ctx context.Context, wg *sync.WaitGroup, srv *server.Server) {
	defer wg.Done()
	srv.Start()
	<-ctx.Done()
	srv.Stop()
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	ms := store.NewMemStore()
	r := handler.NewRouter(ms)
	srv := server.NewServer(
		server.Config{
			Addr:              ":8080",
			ReadTimeout:       30 * time.Second,
			WriteTimeout:      30 * time.Second,
			ReadHeaderTimeout: 30 * time.Second,
			CancelTimeout:     2 * time.Second,
		},
		r)

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go ServeApp(ctx, wg, srv)
	<-ctx.Done()
	cancel()
	wg.Wait()
}
