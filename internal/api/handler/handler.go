package handler

import (
	"main/internal/api/auth"
	"main/internal/api/categories"
	"main/internal/api/conditions"
	"main/internal/api/events"
	"main/internal/api/home"
	"main/internal/api/motivation"
	"main/internal/api/org_types"
	"main/internal/api/organizations"
	"main/internal/api/projects"
	"main/internal/api/roles"
	"main/internal/api/skills"
	"main/internal/api/types"
	"main/internal/api/volunteers"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

type Router struct {
	*chi.Mux
	store types.Store
}

func NewRouter(s types.Store) *Router {
	r := &Router{
		Mux:   chi.NewRouter(),
		store: s,
	}
	r.Use(middleware.Logger)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: false,
	}))
	r.Route("/api", func(mux chi.Router) {
		mux.Mount("/home", home.HomeRouter(r.store))
		mux.Mount("/auth", auth.AuthRouter(r.store))
		// CRUD для волонтеров
		mux.Mount("/volunteers", volunteers.VolunteersRouter(r.store))
		// CRUD для волонтеров
		mux.Mount("/organisations", organizations.OrganizationsRouter(r.store))
		// CRUD для проектов
		mux.Mount("/projects", projects.ProjectsRouter(r.store))
		// CRUD для событий (добрых дел)
		mux.Mount("/events", events.EventsRouter(r.store))
		mux.Mount("/categories", categories.CategoriesRouter(r.store))
		mux.Mount("/roles", roles.RolesRouter(r.store))
		mux.Mount("/skills", skills.SkillsRouter(r.store))
		mux.Mount("/motivation", motivation.MotivationRouter(r.store))
		mux.Mount("/conditions", conditions.ConditionsRouter(r.store))
		mux.Mount("/orgtypes", org_types.OrgTypesRouter(r.store))
	})
	return r
}
