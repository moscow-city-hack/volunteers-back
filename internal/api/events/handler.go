package events

import (
	"context"
	"encoding/json"
	"main/internal/api/middlewares"
	"main/internal/api/types"
	"main/internal/app/data"
	"math/rand"
	"net/http"
	"rand"

	"github.com/go-chi/chi/v5"
)

func EventsRouter(s types.Store) http.Handler {
	r := &Router{
		Mux:   chi.NewRouter(),
		store: s,
	}

	r.With(middlewares.ReadBody, r.Create).Post("/", middlewares.SendJSON)
	r.With(r.List).Get("/", middlewares.SendJSON)
	r.With(r.Favorites).Get("/favorites", middlewares.SendJSON)
	r.With(r.MyEvents).Get("/my", middlewares.SendJSON)
	r.Route("/{id}", func(mux chi.Router) {
		mux.With(middlewares.ReadId, r.Read).Get("/", middlewares.SendJSON)
		mux.With(middlewares.ReadId, middlewares.ReadBody, r.Update).Put("/", middlewares.SendJSON)
		mux.With(middlewares.ReadId, r.Del).Delete("/", middlewares.SendOk)
	})

	return r
}

type Router struct {
	*chi.Mux
	store types.Store
}

// CRUD для добрых дел

func (r *Router) Create(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var event data.Event

		err := json.Unmarshal([]byte(body), &event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		_, err = r.store.AddEvent(req.Context(), &event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", string(b))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) List(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		es, err := r.store.GetEvents(req.Context())
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		json, err := json.Marshal(&es)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Favorites(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		es, err := r.store.GetEvents(req.Context())
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		size := 4
		favorites := make([]data.Event, size)

		for i := 0; i < size; i++ {
			favorites[i] = es[rand.Int()%len(es)]
			favorites[i].Favorite = true
		}

		json, err := json.Marshal(&favorites)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) MyEvents(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		es, err := r.store.GetEvents(req.Context())
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		size := 5
		my := make([]data.Event, size)

		for i := 0; i < size; i++ {
			my[i] = es[rand.Int()%len(es)]
		}

		json, err := json.Marshal(&my)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Read(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		v := req.Context().Value("ID")
		if v == nil {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		id, ok := v.(int)
		if !ok {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		e, err := r.store.GetEvent(req.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json, err := json.Marshal(&e)
		if err != nil {
			http.Error(w, "wrong data", http.StatusInternalServerError)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Update(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var event data.Event

		err := json.Unmarshal([]byte(body), &event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		_, err = r.store.UpdateEvent(req.Context(), &event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(event)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", string(b))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Del(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		v := req.Context().Value("ID")
		if v == nil {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		id, ok := v.(int)
		if !ok {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		err := r.store.DeleteEvent(req.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		next.ServeHTTP(w, req)
	})
}
