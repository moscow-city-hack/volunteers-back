package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"main/internal/api/middlewares"
	"main/internal/api/types"
	"main/internal/app/data"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func AuthRouter(s types.Store) http.Handler {
	r := &Router{
		Mux:   chi.NewRouter(),
		store: s,
	}

	r.With(middlewares.ReadBody, r.Register).Post("/register", middlewares.SendJSON)
	r.With(middlewares.ReadBody, r.Login).Get("/login", middlewares.SendJSON)
	r.With(r.Logout).Get("/logout", middlewares.SendJSON)

	return r
}

type Router struct {
	*chi.Mux
	store types.Store
}

func (r *Router) Register(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var info data.RegisterInfo

		err := json.Unmarshal([]byte(body), &info)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var v data.Volunteer

		v.Name = info.Name
		v.Surname = info.Surname
		v.EMail = info.EMail
		v.BirthDate = info.BirthDate.Format("02.01.2006")
		v.Phone = info.Phone

		_, err = r.store.AddVolunteer(req.Context(), &v)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(v)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", `{ "isLoggedIn": true, "user": `+string(b)+"}")
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Login(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		_, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		user, _ := r.store.GetVolunteer(req.Context(), 164235)
		bytes, _ := json.Marshal(user)
		ctx := context.WithValue(req.Context(), "DATA", fmt.Sprintf(`{ "isLoggedIn": true, "user": %s }`, string(bytes)))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Logout(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		ctx := context.WithValue(req.Context(), "DATA", `{ "isLoggedIn": false }`)
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}
