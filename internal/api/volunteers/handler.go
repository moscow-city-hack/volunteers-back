package volunteers

import (
	"context"
	"encoding/json"
	"main/internal/api/middlewares"
	"main/internal/api/types"
	"main/internal/app/data"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func VolunteersRouter(s types.Store) http.Handler {
	r := &Router{
		Mux:   chi.NewRouter(),
		store: s,
	}

	r.With(middlewares.ReadBody, r.Create).Post("/", middlewares.SendJSON)
	r.With(r.List).Get("/", middlewares.SendJSON)
	r.Route("/{id}", func(mux chi.Router) {
		mux.With(middlewares.ReadId, r.Read).Get("/", middlewares.SendJSON)
		mux.With(middlewares.ReadId, middlewares.ReadBody, r.Update).Put("/", middlewares.SendJSON)
		mux.With(middlewares.ReadId, r.Del).Delete("/", middlewares.SendOk)
	})

	return r
}

type Router struct {
	*chi.Mux
	store types.Store
}

// CRUD для волонтеров

func (r *Router) Create(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var volunteer data.Volunteer

		err := json.Unmarshal([]byte(body), &volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		_, err = r.store.AddVolunteer(req.Context(), &volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", string(b))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) List(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		vs, err := r.store.GetVolunteers(req.Context())
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		json, err := json.Marshal(&vs)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Read(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		v := req.Context().Value("ID")
		if v == nil {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		id, ok := v.(int)
		if !ok {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		vr, err := r.store.GetVolunteer(req.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json, err := json.Marshal(&vr)
		if err != nil {
			http.Error(w, "wrong data", http.StatusInternalServerError)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Update(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var volunteer data.Volunteer

		err := json.Unmarshal([]byte(body), &volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		_, err = r.store.UpdateVolunteer(req.Context(), &volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(volunteer)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", string(b))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Del(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		v := req.Context().Value("ID")
		if v == nil {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		id, ok := v.(int)
		if !ok {
			http.Error(w, "can't get context data", http.StatusBadRequest)
			return
		}
		err := r.store.DeleteVolunteer(req.Context(), id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		next.ServeHTTP(w, req)
	})
}
