package categories

import (
	"context"
	"encoding/json"
	"main/internal/api/middlewares"
	"main/internal/api/types"
	"main/internal/app/data"
	"math/rand"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
)

func CategoriesRouter(s types.Store) http.Handler {
	r := &Router{
		Mux:   chi.NewRouter(),
		store: s,
	}
	r.With(r.List).Get("/", middlewares.SendJSON)
	r.With(middlewares.ReadBody, r.Create).Post("/", middlewares.SendJSON)
	return r
}

type Router struct {
	*chi.Mux
	store types.Store
}

func (r *Router) List(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		sel := req.URL.Query().Get("select")
		cs := data.Categories

		categories := make([]data.DisplayName, 0)

		for _, c := range cs {
			if strings.Contains(strings.ToLower(c.DisplayName), strings.ToLower(strings.TrimSpace(sel))) {
				categories = append(categories, c)
			}
		}

		json, err := json.Marshal(&categories)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		ctx := context.WithValue(req.Context(), "DATA", string(json))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func (r *Router) Create(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		body, ok := req.Context().Value("BODY").(string)
		if !ok {
			http.Error(w, "failed to read body", http.StatusBadRequest)
			return
		}

		var category data.DisplayName

		err := json.Unmarshal([]byte(body), &category)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		id := rand.Int()
		category.ID = strconv.Itoa(id)
		data.Categories = append(data.Categories, category)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		b, err := json.Marshal(category)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(req.Context(), "DATA", string(b))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}
