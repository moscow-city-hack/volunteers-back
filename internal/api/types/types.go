package types

import (
	"context"
	"main/internal/app/data"
)

type SomeData struct {
	Value string `json:"value"`
}

type Store interface {
	GetVolunteers(ctx context.Context) ([]data.Volunteer, error)
	GetVolunteer(ctx context.Context, id int) (*data.Volunteer, error)
	AddVolunteer(ctx context.Context, volunteer *data.Volunteer) (*data.Volunteer, error)
	UpdateVolunteer(ctx context.Context, volunteer *data.Volunteer) (*data.Volunteer, error)
	DeleteVolunteer(ctx context.Context, id int) error
	GetOrganizations(ctx context.Context) ([]data.Organization, error)
	GetOrganization(ctx context.Context, id int) (*data.Organization, error)
	AddOrganization(ctx context.Context, organization *data.Organization) (*data.Organization, error)
	UpdateOrganization(ctx context.Context, organization *data.Organization) (*data.Organization, error)
	DeleteOrganization(ctx context.Context, id int) error
	GetProjects(ctx context.Context) ([]data.Project, error)
	GetProject(ctx context.Context, id int) (*data.Project, error)
	AddProject(ctx context.Context, project *data.Project) (*data.Project, error)
	UpdateProject(ctx context.Context, project *data.Project) (*data.Project, error)
	DeleteProject(ctx context.Context, id int) error
	GetEvents(ctx context.Context) ([]data.Event, error)
	GetEvent(ctx context.Context, id int) (*data.Event, error)
	AddEvent(ctx context.Context, event *data.Event) (*data.Event, error)
	UpdateEvent(ctx context.Context, event *data.Event) (*data.Event, error)
	DeleteEvent(ctx context.Context, id int) error
}
