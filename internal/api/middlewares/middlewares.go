package middlewares

import (
	"context"
	"github.com/go-chi/chi/v5"
	"io"
	"net/http"
	"strconv"
)

func ReadId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		sid := chi.URLParam(req, "id")
		if len(sid) == 0 {
			http.Error(w, "id parameter not found", http.StatusBadRequest)
			return
		}
		id, err := strconv.Atoi(sid)
		if err != nil {
			http.Error(w, "wrong id parameter", http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "ID", id)
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

func ReadBody(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		var reader io.Reader
		reader = req.Body
		buf, err := io.ReadAll(reader)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		ctx := context.WithValue(req.Context(), "BODY", string(buf))
		next.ServeHTTP(w, req.WithContext(ctx))
	})
}

// Обработчики выдачи данных

func SendJSON(w http.ResponseWriter, req *http.Request) {
	data := req.Context().Value("DATA")
	if data == nil {
		http.Error(w, "can't get context data", http.StatusBadRequest)
		return
	}
	str, ok := data.(string)
	if !ok {
		http.Error(w, "can't get context data", http.StatusBadRequest)
		return
	}
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(str))
}

func SendOk(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
}
