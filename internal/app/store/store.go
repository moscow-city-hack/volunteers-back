package store

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"main/internal/app/data"
	"math/rand"
	"sync"
)

type MemStore struct {
	sync.Mutex
	Volunteers    []data.Volunteer
	Organizations []data.Organization
	Projects      []data.Project
	Events        []data.Event
}

func GenerateData(
	vs []data.DobroVolunteer,
	os []data.DobroOrganization,
	ps []data.DobroProject,
	es []data.DobroEvent) ([]data.Volunteer, []data.Organization, []data.Project, []data.Event) {

	vs_new := make([]data.Volunteer, len(vs))
	os_new := make([]data.Organization, len(os))
	ps_new := make([]data.Project, len(ps))
	es_new := make([]data.Event, len(es))

	for i, v := range vs {
		vs_new[i].Id = v.Id
		vs_new[i].Name = v.Fio.FirstName
		vs_new[i].Surname = v.Fio.LastName
		vs_new[i].BirthDate = "02.01.2006"
		vs_new[i].Gender = make([]data.Checkbox, 2)

		vs_new[i].Gender[0].ID = "F"
		vs_new[i].Gender[1].ID = "M"
		vs_new[i].Gender[0].Label = "Женский"
		vs_new[i].Gender[1].Label = "Мужской"
		if v.Gender == "F" {
			vs_new[i].Gender[0].Value = true
		} else {
			vs_new[i].Gender[1].Value = true

		}

		vs_new[i].Address.District = "Опасный"
		vs_new[i].Address.House = "25"
		vs_new[i].Address.Metro = "Тысяча чертей"
		vs_new[i].Address.Street = "Grove Street"

		vs_new[i].Phone = "+9 (999) 999-99-99"
		vs_new[i].EMail = "pussydestroyer1337@google.ru"

		vs_new[i].Categories = data.RandomDisplayNames(data.Categories)
		vs_new[i].Skills = data.RandomDisplayNames(data.Skills)

		vs_new[i].Work = "Временная работа, постоянная работа"

		vs_new[i].Format = "Онлайн, оффлайн"
		vs_new[i].Place = data.Place{Latitude: 55.751574 + (2.0*rand.Float32()-1.0)*0.1, Longitude: 37.573856 + (2.0*rand.Float32()-1.0)*0.1}
		vs_new[i].Image = v.IconFile.Url
		vs_new[i].Rating = float32(rand.Int()%5 + 1)
		vs_new[i].Reviews = rand.Int() % 200
	}

	for i, o := range os {
		os_new[i].Id = o.Id
		os_new[i].Name = o.Name
		os_new[i].Type = "Крутая"
		os_new[i].INN = o.INN
		os_new[i].Phone = "+9 (999) 999-99-99"
		os_new[i].EMail = "pussydestroyer228@google.ru"

		os_new[i].Address.District = "Опасный"
		os_new[i].Address.House = "25"
		os_new[i].Address.Metro = "Тысяча чертей"
		os_new[i].Address.Street = "Grove Street"

		os_new[i].Categories = data.RandomDisplayNames(data.Categories)
		os_new[i].Skills = data.RandomDisplayNames(data.Skills)

		os_new[i].Image = o.IconFile.Url
		os_new[i].Rating = float32(rand.Int()%5 + 1)
		os_new[i].Reviews = rand.Int() % 200
		os_new[i].Place = data.Place{Latitude: 55.751574 + (2.0*rand.Float32()-1.0)*0.1, Longitude: 37.573856 + (2.0*rand.Float32()-1.0)*0.1}
	}

	for i, e := range es {
		es_new[i].Id = e.Id
		es_new[i].Name = e.Name
		es_new[i].Image = e.ImageFile.Url
		if rand.Int()%2 == 0 {
			es_new[i].Favorite = true
		}

		es_new[i].Categories = data.RandomDisplayNames(data.Categories)
		es_new[i].Conditions = data.RandomDisplayNames(data.Conditions)
		es_new[i].Skills = data.RandomDisplayNames(data.Skills)
		es_new[i].Roles = data.RandomDisplayNames(data.Roles)
		es_new[i].Motivation = data.RandomDisplayNames(data.Motivation)
		es_new[i].Work = "Временная работа, постоянная работа"

		es_new[i].Format = "Онлайн, оффлайн"

		es_new[i].Date.From = "22.02.2022"
		es_new[i].Date.To = "05.10.2022"

		es_new[i].Address.District = "Опасный"
		es_new[i].Address.House = "25"
		es_new[i].Address.Metro = "Тысяча чертей"
		es_new[i].Address.Street = "Grove Street"

		es_new[i].Location = "г. Москва, метро " + es_new[i].Address.Metro + ", ул. " + es_new[i].Address.Street + ", " + es_new[i].Address.House

		es_new[i].TimeStart = "12:30"
		es_new[i].TimeStart = "18:00"
		es_new[i].Place = data.Place{Latitude: 55.751574 + (2.0*rand.Float32()-1.0)*0.1, Longitude: 37.573856 + (2.0*rand.Float32()-1.0)*0.1}
	}

	return vs_new, os_new, ps_new, es_new
}

func NewMemStore() *MemStore {
	var vs []data.DobroVolunteer
	err := json.Unmarshal([]byte(data.VDS), &vs)
	if err != nil {
		panic(err)
	}
	var os []data.DobroOrganization
	err = json.Unmarshal([]byte(data.ODS), &os)
	if err != nil {
		panic(err)
	}
	var ps []data.DobroProject
	err = json.Unmarshal([]byte(data.PDS), &ps)
	if err != nil {
		panic(err)
	}
	var es []data.DobroEvent
	err = json.Unmarshal([]byte(data.EDS), &es)
	if err != nil {
		panic(err)
	}

	vs_new, os_new, ps_new, es_new := GenerateData(vs, os, ps, es)

	return &MemStore{
		Volunteers:    vs_new,
		Organizations: os_new,
		Projects:      ps_new,
		Events:        es_new,
	}
}

// Операции с волонтерами

func (ms *MemStore) GetVolunteers(ctx context.Context) ([]data.Volunteer, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	return ms.Volunteers, nil
}

func (ms *MemStore) GetVolunteer(ctx context.Context, id int) (*data.Volunteer, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, v := range ms.Volunteers {
		if v.Id == id {
			return &v, nil
		}
	}

	return nil, sql.ErrNoRows
}

func (ms *MemStore) DeleteVolunteer(ctx context.Context, id int) error {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	i := 1
	for _, v := range ms.Volunteers {
		if v.Id == id {
			break
		}
		i++
	}

	if i-1 == len(ms.Volunteers) {
		return sql.ErrNoRows
	}

	ms.Volunteers = append(ms.Volunteers[:i-1], ms.Volunteers[i:]...)
	return nil
}

func (ms *MemStore) AddVolunteer(ctx context.Context, volunteer *data.Volunteer) (*data.Volunteer, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	ms.Volunteers = append(ms.Volunteers, *volunteer)
	return volunteer, nil
}

func (ms *MemStore) UpdateVolunteer(ctx context.Context, volunteer *data.Volunteer) (*data.Volunteer, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for i, v := range ms.Volunteers {
		if v.Id == volunteer.Id {
			ms.Volunteers[i] = *volunteer
			return volunteer, nil
		}
	}
	return nil, fmt.Errorf("volunteer with %d id doesn't exist", volunteer.Id)
}

// Операции с организациями

func (ms *MemStore) GetOrganizations(ctx context.Context) ([]data.Organization, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	return ms.Organizations, nil
}

func (ms *MemStore) GetOrganization(ctx context.Context, id int) (*data.Organization, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, o := range ms.Organizations {
		if o.Id == id {
			return &o, nil
		}
	}

	return nil, sql.ErrNoRows
}

func (ms *MemStore) DeleteOrganization(ctx context.Context, id int) error {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	i := 1
	for _, o := range ms.Organizations {
		if o.Id == id {
			break
		}
		i++
	}

	if i-1 == len(ms.Organizations) {
		return sql.ErrNoRows
	}

	ms.Organizations = append(ms.Organizations[:i-1], ms.Organizations[i:]...)
	return nil
}

func (ms *MemStore) AddOrganization(ctx context.Context, organization *data.Organization) (*data.Organization, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, o := range ms.Organizations {
		if o.Id == organization.Id {
			return nil, fmt.Errorf("organization with %d id already exists", organization.Id)
		}
	}
	ms.Organizations = append(ms.Organizations, *organization)
	return organization, nil
}

func (ms *MemStore) UpdateOrganization(ctx context.Context, organzation *data.Organization) (*data.Organization, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for i, o := range ms.Organizations {
		if o.Id == organzation.Id {
			ms.Organizations[i] = *organzation
			return organzation, nil
		}
	}
	return nil, fmt.Errorf("organization with %d id doesn't exist", organzation.Id)
}

// Операции с проектами

func (ms *MemStore) GetProjects(ctx context.Context) ([]data.Project, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	return ms.Projects, nil
}

func (ms *MemStore) GetProject(ctx context.Context, id int) (*data.Project, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, p := range ms.Projects {
		if p.Id == id {
			return &p, nil
		}
	}

	return nil, sql.ErrNoRows
}

func (ms *MemStore) DeleteProject(ctx context.Context, id int) error {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	i := 1
	for _, o := range ms.Projects {
		if o.Id == id {
			break
		}
		i++
	}

	if i-1 == len(ms.Projects) {
		return sql.ErrNoRows
	}

	ms.Projects = append(ms.Projects[:i-1], ms.Projects[i:]...)
	return nil
}

func (ms *MemStore) AddProject(ctx context.Context, project *data.Project) (*data.Project, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, p := range ms.Projects {
		if p.Id == project.Id {
			return nil, fmt.Errorf("project with %d id already exists", project.Id)
		}
	}
	ms.Projects = append(ms.Projects, *project)
	return project, nil
}

func (ms *MemStore) UpdateProject(ctx context.Context, project *data.Project) (*data.Project, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for i, p := range ms.Projects {
		if p.Id == project.Id {
			ms.Projects[i] = *project
			return project, nil
		}
	}
	return nil, fmt.Errorf("project with %d id doesn't exist", project.Id)
}

// Операции с событиями (добрыми делами)

func (ms *MemStore) GetEvents(ctx context.Context) ([]data.Event, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	return ms.Events, nil
}

func (ms *MemStore) GetEvent(ctx context.Context, id int) (*data.Event, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, e := range ms.Events {
		if e.Id == id {
			return &e, nil
		}
	}

	return nil, sql.ErrNoRows
}

func (ms *MemStore) DeleteEvent(ctx context.Context, id int) error {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}

	i := 1
	for _, e := range ms.Events {
		if e.Id == id {
			break
		}
		i++
	}

	if i-1 == len(ms.Events) {
		return sql.ErrNoRows
	}

	ms.Events = append(ms.Events[:i-1], ms.Events[i:]...)
	return nil
}

func (ms *MemStore) AddEvent(ctx context.Context, event *data.Event) (*data.Event, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for _, e := range ms.Events {
		if e.Id == event.Id {
			return nil, fmt.Errorf("event with %d id already exists", event.Id)
		}
	}
	ms.Events = append(ms.Events, *event)
	return event, nil
}

func (ms *MemStore) UpdateEvent(ctx context.Context, event *data.Event) (*data.Event, error) {
	ms.Lock()
	defer ms.Unlock()

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}

	for i, e := range ms.Events {
		if e.Id == event.Id {
			ms.Events[i] = *event
			return event, nil
		}
	}
	return nil, fmt.Errorf("event with %d id doesn't exist", event.Id)
}
