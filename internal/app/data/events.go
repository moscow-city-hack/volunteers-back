package data

var EDS = `[{
        "id": 10185122,
        "name": "Отбор на участие в обучающей стажировке на базе Благотворительного фонда География Добра",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 – 18 сентября 2022",
            "startDate": "2022-09-12T10:00:00+03:00",
            "endDate": "2022-09-18T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a295795f-9d0e-2ec4-393f-8c01f1abc3e0.png",
            "id": "a295795f-9d0e-2ec4-393f-8c01f1abc3e0",
            "name": "e0375562-8896-b01f-20b4-4c744bd3e7d5.png",
            "size": 28751,
            "createdAt": "2022-05-23T14:58:45+00:00",
            "updatedAt": "2022-05-23T14:58:45+00:00",
            "expiredAt": "2022-05-24T14:58:45+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Костромская обл",
            "region": 44,
            "municipality": null,
            "municipalityCode": null,
            "settlement": null,
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": null,
            "y": null
        }
    }, {
        "id": 10186258,
        "name": "Отбор волонтеров Гуманитарных миссий #МЫВМЕСТЕ c Донбассом. Проведение спортивных программ для детей.",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 июня – 5 июля 2022",
            "startDate": "2022-06-17T08:00:00+03:00",
            "endDate": "2022-07-05T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1d4a3c66-344a-b6fa-2a6e-e4d43d01c39d.jpg",
            "id": "1d4a3c66-344a-b6fa-2a6e-e4d43d01c39d",
            "name": "3bc10e90-c78c-0e75-28e3-0e18d81c4c6e.jpg",
            "size": 48287,
            "createdAt": "2022-05-25T16:52:08+00:00",
            "updatedAt": "2022-05-25T16:52:08+00:00",
            "expiredAt": "2022-05-26T16:52:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Донецкая обл, г Донецк",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Донецк",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.80224,
            "y": 48.023
        }
    }, {
        "id": 10186270,
        "name": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "30 июня – 20 июля 2022",
            "startDate": "2022-06-30T08:00:00+03:00",
            "endDate": "2022-07-20T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9fbbebdd-f4d2-8374-fabe-9e908510abb5.jpg",
            "id": "9fbbebdd-f4d2-8374-fabe-9e908510abb5",
            "name": "f489f421-07d2-a203-1111-38130a6b5fda.jpg",
            "size": 34158,
            "createdAt": "2022-06-03T14:55:47+00:00",
            "updatedAt": "2022-06-03T14:55:47+00:00",
            "expiredAt": "2022-06-04T14:55:47+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Луганская обл, г Луганск",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Луганск",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 39.32857,
            "y": 48.56952
        }
    }, {
        "id": 10186274,
        "name": "Отбор волонтеров на Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Восстановление работы музея",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "30 июня – 26 июля 2022",
            "startDate": "2022-06-30T08:00:00+03:00",
            "endDate": "2022-07-26T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db1b6964-94a1-6ab8-417e-74663b46641f.jpeg",
            "id": "db1b6964-94a1-6ab8-417e-74663b46641f",
            "name": "fad3abc3-f056-f3b0-37ab-4070eaa03b7e.jpeg",
            "size": 34943,
            "createdAt": "2022-05-27T14:11:02+00:00",
            "updatedAt": "2022-05-27T14:11:02+00:00",
            "expiredAt": "2022-05-28T14:11:02+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Донецкая обл, г Донецк",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Донецк",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.80224,
            "y": 48.023
        }
    }, {
        "id": 10186262,
        "name": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Психологическая помощь студентам и сотрудникам вузов.",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "15 сентября – 7 октября 2022",
            "startDate": "2022-09-15T08:00:00+03:00",
            "endDate": "2022-10-07T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c5cd9d49-75d6-ac56-4e0f-72fa06f52b5a.jpg",
            "id": "c5cd9d49-75d6-ac56-4e0f-72fa06f52b5a",
            "name": "c88bb39d-a83f-facf-b5b1-e42c99abb372.jpg",
            "size": 17381,
            "createdAt": "2022-05-25T17:18:25+00:00",
            "updatedAt": "2022-05-25T17:18:25+00:00",
            "expiredAt": "2022-05-26T17:18:25+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Донецкая обл, г Донецк",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Донецк",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.80224,
            "y": 48.023
        }
    }, {
        "id": 10186265,
        "name": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Организация внеучебной работы в школах-интернатах",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 сентября – 23 октября 2022",
            "startDate": "2022-09-08T10:00:00+03:00",
            "endDate": "2022-10-23T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/0cf97af4-9f86-9003-cdcf-ca313a153908.jpg",
            "id": "0cf97af4-9f86-9003-cdcf-ca313a153908",
            "name": "eeccd573-2d54-648f-23c5-c9dbe1ebe979.jpg",
            "size": 26251,
            "createdAt": "2022-05-25T17:42:08+00:00",
            "updatedAt": "2022-05-25T17:42:08+00:00",
            "expiredAt": "2022-05-26T17:42:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Донецкая обл, г Донецк",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Донецк",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.80224,
            "y": 48.023
        }
    }, {
        "id": 10147628,
        "name": "Отбор на участие в ФОРМУЛЕ 1 ГРАН-ПРИ АЗЕРБАЙДЖАНА 2022",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 – 12 июня 2022",
            "startDate": "2022-06-10T08:00:00+03:00",
            "endDate": "2022-06-12T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e28f3958-49b6-ee68-ae24-06a2746909f0.jpg",
            "id": "e28f3958-49b6-ee68-ae24-06a2746909f0",
            "name": "460a87c6-d23f-652a-0703-abcef3e54a0f.jpg",
            "size": 35842,
            "createdAt": "2022-03-15T14:39:00+00:00",
            "updatedAt": "2022-03-15T14:39:00+00:00",
            "expiredAt": "2022-03-16T14:39:00+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10144398,
        "name": "Отбор на участие в обучающей стажировке на базе Экоцентра Заповедники",
        "categories": [{
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 – 19 июня 2022",
            "startDate": "2022-06-12T10:00:00+05:00",
            "endDate": "2022-06-19T20:00:00+05:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/14977104-111f-d6c0-1e6d-f492e3509be5.jpg",
            "id": "14977104-111f-d6c0-1e6d-f492e3509be5",
            "name": "16a28e7b-3a13-184b-d4f5-e3149331b637.jpg",
            "size": 8875,
            "createdAt": "2022-03-05T12:31:40+00:00",
            "updatedAt": "2022-03-05T12:31:40+00:00",
            "expiredAt": "2022-03-06T12:31:40+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Челябинская обл",
            "region": 74,
            "municipality": null,
            "municipalityCode": null,
            "settlement": null,
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": null,
            "y": null
        }
    }, {
        "id": 10034395,
        "name": "Отбор на участие в XXXI Всемирных студенческих играх ФИСУ 2021 года в г. Чэнду",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "26 июня – 7 июля 2022",
            "startDate": "2022-06-26T08:00:00+03:00",
            "endDate": "2022-07-07T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/cd8cdd08-e76c-0500-f8be-128a37b71e90.png",
            "id": "cd8cdd08-e76c-0500-f8be-128a37b71e90",
            "name": "1cf1a307-5331-8b26-bf34-c9783494adb5.png",
            "size": 30807,
            "createdAt": "2021-01-19T08:56:13+00:00",
            "updatedAt": "2021-01-19T08:56:13+00:00",
            "expiredAt": "2021-01-20T08:56:11+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.754047
        }
    }, {
        "id": 10130124,
        "name": "Отбор на участие в European Universities Games Lodz 2022",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 – 31 июля 2022",
            "startDate": "2022-07-10T08:00:00+03:00",
            "endDate": "2022-07-31T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8686ec53-61a7-3da5-48bd-aba17c0677a3.jpeg",
            "id": "8686ec53-61a7-3da5-48bd-aba17c0677a3",
            "name": "5bc210c1-6c0c-3767-b3ce-b8adf507ca0a.jpeg",
            "size": 10359,
            "createdAt": "2022-01-25T14:32:49+00:00",
            "updatedAt": "2022-01-25T14:32:49+00:00",
            "expiredAt": "2022-01-26T14:32:49+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10123843,
        "name": "Отбор на участие в Special Olympics World Games Berlin 2023",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 – 25 июня 2023",
            "startDate": "2023-06-17T08:00:00+03:00",
            "endDate": "2023-06-25T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/36ba5ba5-69e2-46fa-4ac5-69f531c65be3.jpg",
            "id": "36ba5ba5-69e2-46fa-4ac5-69f531c65be3",
            "name": "1013f233-b411-1004-6b4f-9afc0dcb79d6.jpg",
            "size": 13997,
            "createdAt": "2021-12-28T15:26:57+00:00",
            "updatedAt": "2021-12-28T15:26:57+00:00",
            "expiredAt": "2021-12-29T15:26:57+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e588e8e7-2371-97d8-2f5c-5f6ca2c98036.jpg",
                "id": "e588e8e7-2371-97d8-2f5c-5f6ca2c98036",
                "name": "9493db58-2f54-d93f-fb1e-f629e2749a48.jpg",
                "size": 5438,
                "createdAt": "2020-04-27T18:06:39+00:00",
                "updatedAt": "2020-04-27T18:06:39+00:00",
                "expiredAt": "2020-04-28T18:06:39+00:00"
            },
            "name": "Ассоциация волонтерских центров",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10182780,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 – 18 августа 2022",
            "startDate": "2022-08-17T20:30:00+03:00",
            "endDate": "2022-08-18T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182807,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "5 – 6 сентября 2022",
            "startDate": "2022-09-05T20:30:00+03:00",
            "endDate": "2022-09-06T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182808,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "13 сентября 2022",
            "startDate": "2022-09-13T06:15:00+03:00",
            "endDate": "2022-09-13T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182749,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "29 – 30 июля 2022",
            "startDate": "2022-07-29T20:30:00+03:00",
            "endDate": "2022-07-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182746,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "9 – 10 июля 2022",
            "startDate": "2022-07-09T06:15:00+03:00",
            "endDate": "2022-07-10T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182747,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 – 11 июля 2022",
            "startDate": "2022-07-10T20:30:00+03:00",
            "endDate": "2022-07-11T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182740,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "22 июня 2022",
            "startDate": "2022-06-22T20:30:00+03:00",
            "endDate": "2022-06-22T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182869,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "22 октября 2022",
            "startDate": "2022-10-22T20:30:00+03:00",
            "endDate": "2022-10-22T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182873,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 ноября 2022",
            "startDate": "2022-11-10T20:30:00+03:00",
            "endDate": "2022-11-10T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182879,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "18 декабря 2022",
            "startDate": "2022-12-18T20:30:00+03:00",
            "endDate": "2022-12-18T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182777,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 – 17 августа 2022",
            "startDate": "2022-08-16T06:15:00+03:00",
            "endDate": "2022-08-17T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182731,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "20 июня 2022",
            "startDate": "2022-06-20T06:15:00+03:00",
            "endDate": "2022-06-20T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182871,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "9 ноября 2022",
            "startDate": "2022-11-09T06:15:00+03:00",
            "endDate": "2022-11-09T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182748,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "28 – 29 июля 2022",
            "startDate": "2022-07-28T06:15:00+03:00",
            "endDate": "2022-07-29T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182864,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "2 октября 2022",
            "startDate": "2022-10-02T06:15:00+03:00",
            "endDate": "2022-10-02T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182868,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 октября 2022",
            "startDate": "2022-10-21T06:15:00+03:00",
            "endDate": "2022-10-21T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182875,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "28 ноября 2022",
            "startDate": "2022-11-28T06:15:00+03:00",
            "endDate": "2022-11-28T08:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182866,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "3 октября 2022",
            "startDate": "2022-10-03T20:30:00+03:00",
            "endDate": "2022-10-03T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10182877,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "29 ноября 2022",
            "startDate": "2022-11-29T20:30:00+03:00",
            "endDate": "2022-11-29T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10180913,
        "name": "«Психологические гостиные»",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "13 мая – 13 июня 2022",
            "startDate": "2022-05-13T13:00:00+03:00",
            "endDate": "2022-06-13T15:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7cf1df05-35f3-6812-6707-675812f03dba.JPG",
            "id": "7cf1df05-35f3-6812-6707-675812f03dba",
            "name": "d97bcf64-d0cf-172b-d5c0-3dbfe77e7e01.JPG",
            "size": 26295,
            "createdAt": "2022-05-13T12:38:25+00:00",
            "updatedAt": "2022-05-13T12:38:25+00:00",
            "expiredAt": "2022-05-14T12:38:25+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Радио, д 24 к 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Басманный р-н",
            "settlementCodeDistrictCode": null,
            "street": "ул Радио",
            "house": "24",
            "flat": null,
            "fias": null,
            "x": 37.683221,
            "y": 55.761831
        }
    }, {
        "id": 10182733,
        "name": "«Поможем детям-инвалидам»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 июня 2022",
            "startDate": "2022-06-21T10:00:00+03:00",
            "endDate": "2022-06-21T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bb25bf32-bf31-8de3-de4a-127f770c3aec.png",
            "id": "bb25bf32-bf31-8de3-de4a-127f770c3aec",
            "name": "474018c4-95b5-4eaa-2ad5-8a1e3106d511.png",
            "size": 41965,
            "createdAt": "2022-05-12T06:00:16+00:00",
            "updatedAt": "2022-05-12T06:00:16+00:00",
            "expiredAt": "2022-05-13T06:00:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Комсомольская пл, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Красносельский р-н",
            "settlementCodeDistrictCode": null,
            "street": "Комсомольская пл",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.65567,
            "y": 55.773611
        }
    }, {
        "id": 10159278,
        "name": "Программа «Путь твоей безопасности»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "7 апреля – 31 декабря 2022",
            "startDate": "2022-04-07T13:00:00+03:00",
            "endDate": "2022-12-31T16:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e4246797-09e0-804c-4783-180717371179.png",
            "id": "e4246797-09e0-804c-4783-180717371179",
            "name": "88d3bdfd-b647-feac-c2b1-6681e939b9b5.png",
            "size": 191251,
            "createdAt": "2022-04-07T09:41:12+00:00",
            "updatedAt": "2022-04-07T09:41:12+00:00",
            "expiredAt": "2022-04-08T09:41:12+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10158513,
        "name": "Доброе тепло",
        "categories": [{
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "1 – 30 ноября 2022",
            "startDate": "2022-11-01T09:00:00+03:00",
            "endDate": "2022-11-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2ceadb38-d6f4-1c8a-2609-099f109ca6c1.jpg",
            "id": "2ceadb38-d6f4-1c8a-2609-099f109ca6c1",
            "name": "ecd69a16-0a4e-04ad-cb5e-b7f422d9c626.jpg",
            "size": 39803,
            "createdAt": "2022-04-07T10:52:01+00:00",
            "updatedAt": "2022-04-07T10:52:01+00:00",
            "expiredAt": "2022-04-08T10:52:01+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/40809571-8c45-a24b-1e1d-931ce677be42.jpg",
                "id": "40809571-8c45-a24b-1e1d-931ce677be42",
                "name": "6257e1e7-2a81-f13e-07c6-7dbcd11b0d3a.jpg",
                "size": 5051,
                "createdAt": "2022-03-02T06:03:56+00:00",
                "updatedAt": "2022-03-02T06:03:56+00:00",
                "expiredAt": "2022-03-03T06:03:56+00:00"
            },
            "name": "ОАО «РЖД»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10165548,
        "name": "Всероссийский проект #ДоброВСело",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18134,
                "title": "Коронавирус",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/corona.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/corona.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/corona.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/corona.png",
                "bgColor": "#EA6949"
            }
        ],
        "eventPeriod": {
            "shortTitle": "18 апреля – 31 октября 2022",
            "startDate": "2022-04-18T08:00:00+03:00",
            "endDate": "2022-10-31T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db5ec473-7ab9-9a23-fa1e-0ee0b7c7edfb.jpg",
            "id": "db5ec473-7ab9-9a23-fa1e-0ee0b7c7edfb",
            "name": "7545e67e-8bc0-1e40-8160-f1d0ef0a8660.jpg",
            "size": 42349,
            "createdAt": "2022-04-17T07:48:31+00:00",
            "updatedAt": "2022-04-17T07:48:31+00:00",
            "expiredAt": "2022-04-18T07:48:31+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e705db95-8c2c-3611-bd1b-e9672e96a7db.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e705db95-8c2c-3611-bd1b-e9672e96a7db.jpg",
                "id": "e705db95-8c2c-3611-bd1b-e9672e96a7db",
                "name": "fd555412-bf4e-23fc-7e31-2ac1e23eca7f.jpg",
                "size": 4934,
                "createdAt": "2020-04-24T18:45:41+00:00",
                "updatedAt": "2020-04-24T18:45:41+00:00",
                "expiredAt": "2020-04-25T18:45:41+00:00"
            },
            "name": "Всероссийское общественное движение Волонтеры - медики",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "Россия",
            "region": null,
            "municipality": null,
            "municipalityCode": null,
            "settlement": null,
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": null,
            "y": null
        }
    }, {
        "id": 10191606,
        "name": "Поездка в «Пансион семейного воспитания»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T09:00:00+03:00",
            "endDate": "2022-06-11T21:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f56c65b5-976d-5254-961b-06434d7fbac6.jpeg",
            "id": "f56c65b5-976d-5254-961b-06434d7fbac6",
            "name": "300bdabc-96c5-12f7-ea66-e1afd433ec60.jpeg",
            "size": 29477,
            "createdAt": "2022-06-06T20:08:37+00:00",
            "updatedAt": "2022-06-06T20:08:37+00:00",
            "expiredAt": "2022-06-07T20:08:37+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/9f066fa55cf54cc39cf0512c4405d308.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/9f066fa55cf54cc39cf0512c4405d308.jpg",
                "id": "35c16244-2ac8-d88f-daad-16afa5ea55fc",
                "name": "9f066fa55cf54cc39cf0512c4405d308.jpg",
                "size": null,
                "createdAt": "2020-02-29T01:59:10+00:00",
                "updatedAt": "2020-10-20T03:36:30+00:00",
                "expiredAt": null
            },
            "name": "Ресурсный центр добровольчества Волонтеры Адыгеи",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Старый Толмачёвский пер, д 6",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Замоскворечье",
            "settlementCodeDistrictCode": null,
            "street": "Старый Толмачёвский пер",
            "house": "6",
            "flat": null,
            "fias": null,
            "x": 37.631613,
            "y": 55.740061
        }
    }, {
        "id": 10139786,
        "name": "Штаб по сбору гуманитарной помощи нуждающимся #МоскваПомогает",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "22 февраля – 1 июля 2022",
            "startDate": "2022-02-22T10:00:00+03:00",
            "endDate": "2022-07-01T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ed6a18c4-79f7-1113-4602-6400e76059f1.png",
            "id": "ed6a18c4-79f7-1113-4602-6400e76059f1",
            "name": "ccf13ee7-92fc-6fbd-a459-cd083688cda1.png",
            "size": 148246,
            "createdAt": "2022-02-22T10:36:01+00:00",
            "updatedAt": "2022-02-22T10:36:01+00:00",
            "expiredAt": "2022-02-23T10:36:01+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/8b335a70acec48bc83820d9f5f9a456a.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/8b335a70acec48bc83820d9f5f9a456a.jpg",
                "id": "9c3dd8e6-199e-156f-013f-bf58fe8e0e1e",
                "name": "8b335a70acec48bc83820d9f5f9a456a.jpg",
                "size": null,
                "createdAt": "2020-02-29T02:13:22+00:00",
                "updatedAt": "2020-11-27T04:37:59+00:00",
                "expiredAt": null
            },
            "name": "Мосволонтер",
            "statistic": {
                "organizerRating": 4.9
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Россия, Москва, Ленинградский проспект, 5с1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Ленинградский пр-кт",
            "house": "5",
            "flat": null,
            "fias": null,
            "x": 37.577203,
            "y": 55.779497
        }
    }, {
        "id": 10082419,
        "name": "Развитие проекта «Следы Наций»",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 августа 2021 – 22 декабря 2025",
            "startDate": "2021-08-12T10:00:00+03:00",
            "endDate": "2025-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/aa425da0-26a9-9289-342b-0b72e1a754aa.jpg",
            "id": "aa425da0-26a9-9289-342b-0b72e1a754aa",
            "name": "aa7157bf-6a93-45c2-e179-b85afd2548ad.jpg",
            "size": 61764,
            "createdAt": "2021-08-29T07:50:51+00:00",
            "updatedAt": "2021-08-29T07:50:51+00:00",
            "expiredAt": "2021-08-30T07:50:51+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10179745,
        "name": "Военные оркестры в парках",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 мая – 21 августа 2022",
            "startDate": "2022-05-21T10:00:00+03:00",
            "endDate": "2022-08-21T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f8e4c984-07cd-3735-fa1b-69c4f6bcb672.png",
            "id": "f8e4c984-07cd-3735-fa1b-69c4f6bcb672",
            "name": "4e9e13d0-03ac-5e96-1bf5-bc99d3a06089.png",
            "size": 222329,
            "createdAt": "2022-05-11T11:52:53+00:00",
            "updatedAt": "2022-05-11T11:52:53+00:00",
            "expiredAt": "2022-05-12T11:52:53+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db877488-f472-8f03-734a-2d626d575b81.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db877488-f472-8f03-734a-2d626d575b81.png",
                "id": "db877488-f472-8f03-734a-2d626d575b81",
                "name": "f7ed44ed-85e4-254c-bf9e-4ec09139e7ec.png",
                "size": 21955,
                "createdAt": "2022-04-10T13:00:12+00:00",
                "updatedAt": "2022-04-10T13:00:12+00:00",
                "expiredAt": "2022-04-11T13:00:12+00:00"
            },
            "name": "Союз волонтеров",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10085753,
        "name": "Психологи  Добромобиля",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10177743,
        "name": "Организация школьных мероприятий",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "6 мая – 30 июня 2022",
            "startDate": "2022-05-06T12:00:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/14cdde87-8204-0fe7-d0c3-390385c94d2c.jpg",
            "id": "14cdde87-8204-0fe7-d0c3-390385c94d2c",
            "name": "07946bd4-a4f3-1f41-de7a-dbf9f9ab3be8.jpg",
            "size": 33339,
            "createdAt": "2022-05-06T07:39:39+00:00",
            "updatedAt": "2022-05-06T07:39:39+00:00",
            "expiredAt": "2022-05-07T07:39:39+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10085749,
        "name": "Адвокаты  Добромобиля",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10084565,
        "name": "Центр Сопровождения Партнеров проекта «Следы Наций»",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "25 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-25T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/35602b40-f494-7412-16b1-e70c63b9d250.jpg",
            "id": "35602b40-f494-7412-16b1-e70c63b9d250",
            "name": "b746daa7-a830-c3aa-e625-f1bfc862f6db.jpg",
            "size": 50802,
            "createdAt": "2021-08-29T13:21:05+00:00",
            "updatedAt": "2021-08-29T13:21:05+00:00",
            "expiredAt": "2021-08-30T13:21:05+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10084858,
        "name": "Амбассадоры «Следов Наций» в Москве и за рубежом",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "26 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-26T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/26389792-7188-b67c-e2a9-68aeae9ea0f7.jpg",
            "id": "26389792-7188-b67c-e2a9-68aeae9ea0f7",
            "name": "006774bc-2320-482f-b7e7-92ea72b928a1.jpg",
            "size": 50802,
            "createdAt": "2021-08-29T07:57:38+00:00",
            "updatedAt": "2021-08-29T07:57:38+00:00",
            "expiredAt": "2021-08-30T07:57:38+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10085746,
        "name": "Сбор средств на проект Добромобиль",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10085752,
        "name": "Сбор компьютеров, ноутбуков, телефонов и планшетов для  раздачи  Добромобилем",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10090034,
        "name": "Волонтерские и KPI (КПЭ) программы для школ, школьников и учителей",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 сентября 2021 – 22 декабря 2022",
            "startDate": "2021-09-16T10:00:00+03:00",
            "endDate": "2022-12-22T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e582be5e-b72d-ea64-e43c-98c041700917.jpg",
            "id": "e582be5e-b72d-ea64-e43c-98c041700917",
            "name": "7b421a0a-78cb-515b-ee8f-9eff7e5a0fc7.jpg",
            "size": 61764,
            "createdAt": "2021-09-15T13:21:51+00:00",
            "updatedAt": "2021-09-15T13:21:51+00:00",
            "expiredAt": "2021-09-16T13:21:51+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10192425,
        "name": "Волонтёрь с факультетом Ксении Собчак!",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 – 18 июня 2022",
            "startDate": "2022-06-17T09:00:00+03:00",
            "endDate": "2022-06-18T16:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/60b43a63-4b26-de26-bb16-7b12b0d68401.png",
            "id": "60b43a63-4b26-de26-bb16-7b12b0d68401",
            "name": "de7d64f8-529a-2b89-9bc4-dfef52645416.png",
            "size": 65095,
            "createdAt": "2022-06-08T09:35:21+00:00",
            "updatedAt": "2022-06-08T09:35:21+00:00",
            "expiredAt": "2022-06-09T09:35:21+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
                "id": "ca1151b4-8815-7ee2-41d4-20c1ad17b418",
                "name": "5b86f101dbd04d42b60fd991fa342a70.jpg",
                "size": null,
                "createdAt": "2020-02-29T01:59:20+00:00",
                "updatedAt": "2020-12-03T07:51:33+00:00",
                "expiredAt": null
            },
            "name": "Волонтерский центр Университета Синергия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, шоссе Энтузиастов, д 5 стр 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Лефортово",
            "settlementCodeDistrictCode": null,
            "street": "шоссе Энтузиастов",
            "house": "5",
            "flat": null,
            "fias": null,
            "x": 37.696472,
            "y": 55.7494
        }
    }, {
        "id": 10189414,
        "name": "Волонтёры метро",
        "categories": [{
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 26,
                "title": "Урбанистика",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/urban.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/urban.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/urban.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/urban.png",
                "bgColor": "#6D14C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "1 – 30 июня 2022",
            "startDate": "2022-06-01T09:00:00+03:00",
            "endDate": "2022-06-30T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/16f16dad-adbd-bd78-b142-5400ccbaf8f3.jpg",
            "id": "16f16dad-adbd-bd78-b142-5400ccbaf8f3",
            "name": "3d63f395-7332-d27b-a3ce-e3ed5bd6beae.jpg",
            "size": 26054,
            "createdAt": "2022-06-01T12:43:52+00:00",
            "updatedAt": "2022-06-01T12:43:52+00:00",
            "expiredAt": "2022-06-02T12:43:52+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
                "id": "ca1151b4-8815-7ee2-41d4-20c1ad17b418",
                "name": "5b86f101dbd04d42b60fd991fa342a70.jpg",
                "size": null,
                "createdAt": "2020-02-29T01:59:20+00:00",
                "updatedAt": "2020-12-03T07:51:33+00:00",
                "expiredAt": null
            },
            "name": "Волонтерский центр Университета Синергия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Ленинградский пр-кт",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Сокол",
            "settlementCodeDistrictCode": null,
            "street": "Ленинградский пр-кт",
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.545626,
            "y": 55.794285
        }
    }, {
        "id": 10117428,
        "name": "Мастер-класс Секреты ёлочной гирлянды",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 декабря 2021 – 30 июня 2022",
            "startDate": "2021-12-08T08:30:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/FFA800-family.png",
            "id": "16fe04e2-aa49-aa21-16cf-63e09506c70e",
            "name": "FFA800-family",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10117433,
        "name": "Мастер-класс Волшебная вода и три ее состояния",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 декабря 2021 – 30 июня 2022",
            "startDate": "2021-12-08T08:30:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/FFA800-family.png",
            "id": "16fe04e2-aa49-aa21-16cf-63e09506c70e",
            "name": "FFA800-family",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10189767,
        "name": "Мероприятие День молодежи",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "27 июня 2022",
            "startDate": "2022-06-27T10:00:00+03:00",
            "endDate": "2022-06-27T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/667d95c6-38d0-d744-d271-8b38d66f1f3a.jpg",
            "id": "667d95c6-38d0-d744-d271-8b38d66f1f3a",
            "name": "8472c6f9-639f-de88-fc6e-ab0d9144f111.jpg",
            "size": 43792,
            "createdAt": "2022-06-02T07:02:14+00:00",
            "updatedAt": "2022-06-02T07:02:14+00:00",
            "expiredAt": "2022-06-03T07:02:14+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
                "id": "ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad",
                "name": "10e601e4-c6ee-b5b1-7d25-d73dd0c58a22.png",
                "size": 17472,
                "createdAt": "2021-12-29T14:13:30+00:00",
                "updatedAt": "2021-12-29T14:13:30+00:00",
                "expiredAt": "2021-12-30T14:13:30+00:00"
            },
            "name": "Помогать с радостью",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10089316,
        "name": "Прогулки с особыми взрослыми из ПНИ 34",
        "categories": [{
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 28,
                "title": "Права человека",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.png",
                "bgColor": "#A09A00"
            }
        ],
        "eventPeriod": {
            "shortTitle": "13 сентября 2021 – 13 сентября 2022",
            "startDate": "2021-09-13T11:00:00+03:00",
            "endDate": "2022-09-13T13:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/075343dd-2c9d-36f3-4119-4a5ac38e4821.jpg",
            "id": "075343dd-2c9d-36f3-4119-4a5ac38e4821",
            "name": "2d3ebee1-8014-2c58-7fc5-5efe6e3622f6.jpg",
            "size": 32793,
            "createdAt": "2021-04-26T20:11:54+00:00",
            "updatedAt": "2021-04-26T20:11:54+00:00",
            "expiredAt": "2021-04-27T20:11:54+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
            "iconFile": {
                "width": 200,
                "height": 200,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
                "id": "b6cf0b66-61a8-801f-55db-5efc3eb21458",
                "name": "5fa1b809-a37a-589f-46af-a6703a23e563.jpg",
                "size": 6742,
                "createdAt": "2022-02-20T22:42:35+00:00",
                "updatedAt": "2022-02-20T22:42:35+00:00",
                "expiredAt": "2022-02-21T22:42:34+00:00"
            },
            "name": "РБОО Центр лечебной педагогики",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Москворечье, д 7",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Москворечье",
            "house": "7",
            "flat": null,
            "fias": null,
            "x": 37.644669,
            "y": 55.65137
        }
    }, {
        "id": 10178512,
        "name": "Прогулки с особыми детьми из детского дома-интерната",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "9 мая 2022 – 16 октября 2023",
            "startDate": "2022-05-09T11:00:00+03:00",
            "endDate": "2023-10-16T13:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/02842cac-5d2c-97b7-6671-96de471f013c.jpeg",
            "id": "02842cac-5d2c-97b7-6671-96de471f013c",
            "name": "a14a0472-43b0-e40d-d783-cee7377b1c31.jpeg",
            "size": 39836,
            "createdAt": "2021-02-09T21:38:05+00:00",
            "updatedAt": "2021-02-09T21:38:05+00:00",
            "expiredAt": "2021-02-10T21:38:05+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
            "iconFile": {
                "width": 200,
                "height": 200,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
                "id": "b6cf0b66-61a8-801f-55db-5efc3eb21458",
                "name": "5fa1b809-a37a-589f-46af-a6703a23e563.jpg",
                "size": 6742,
                "createdAt": "2022-02-20T22:42:35+00:00",
                "updatedAt": "2022-02-20T22:42:35+00:00",
                "expiredAt": "2022-02-21T22:42:34+00:00"
            },
            "name": "РБОО Центр лечебной педагогики",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Профсоюзная, д 118А",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Коньково",
            "settlementCodeDistrictCode": null,
            "street": "ул Профсоюзная",
            "house": "118А",
            "flat": null,
            "fias": null,
            "x": 37.518416,
            "y": 55.636372
        }
    }, {
        "id": 10124652,
        "name": "Доставка продуктов питания пожилым и маломобильным гражданам",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "7 января – 31 июля 2022",
            "startDate": "2022-01-07T10:00:00+03:00",
            "endDate": "2022-07-31T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1a4e20eb-cf68-f26c-96da-419dd33e12d3.jpg",
            "id": "1a4e20eb-cf68-f26c-96da-419dd33e12d3",
            "name": "25276b9c-9d31-4f63-5c6d-ba447456e6e9.jpg",
            "size": 23334,
            "createdAt": "2022-01-06T16:54:20+00:00",
            "updatedAt": "2022-01-06T16:54:20+00:00",
            "expiredAt": "2022-01-07T16:54:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
                "id": "ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad",
                "name": "10e601e4-c6ee-b5b1-7d25-d73dd0c58a22.png",
                "size": 17472,
                "createdAt": "2021-12-29T14:13:30+00:00",
                "updatedAt": "2021-12-29T14:13:30+00:00",
                "expiredAt": "2021-12-30T14:13:30+00:00"
            },
            "name": "Помогать с радостью",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "Россия, Москва, Коробейников переулок, 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Коробейников пер",
            "house": "8",
            "flat": null,
            "fias": null,
            "x": 37.600501205688,
            "y": 55.737666791423
        }
    }, {
        "id": 10185671,
        "name": "Волонтерство в ГБУЗ ГКБ №1 им.Н.И.Пирогова ДЗМ в МАЕ-ИЮНЕ 2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/09cbd10be19f4387b6bab76f63dcb0db.jpg",
            "id": "8e42d6da-f5e5-350e-468b-395f0db35b31",
            "name": "09cbd10be19f4387b6bab76f63dcb0db.jpg",
            "size": null,
            "createdAt": "2020-04-18T01:06:02+00:00",
            "updatedAt": "2020-10-29T19:39:24+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Ленинский пр-кт, д 8",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Ленинский пр-кт",
            "house": "8",
            "flat": null,
            "fias": null,
            "x": 37.604128,
            "y": 55.725518
        }
    }, {
        "id": 10189914,
        "name": "Соревнования по кроссу ко Дню России",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T08:30:00+03:00",
            "endDate": "2022-06-11T15:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/eee38127-9a93-1a6a-3b21-d4310aec7c68.jpg",
            "id": "eee38127-9a93-1a6a-3b21-d4310aec7c68",
            "name": "2a4ff20d-d8a7-a386-2462-d8f13e4839ed.jpg",
            "size": 41830,
            "createdAt": "2022-06-02T09:31:53+00:00",
            "updatedAt": "2022-06-02T09:31:53+00:00",
            "expiredAt": "2022-06-03T09:31:53+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.218349988442,
            "y": 56.004518809697
        }
    }, {
        "id": 10182364,
        "name": "Праздничная программа ко дню России в ГБУ Заря",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 июня 2022",
            "startDate": "2022-06-12T17:00:00+03:00",
            "endDate": "2022-06-12T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2950298e-cf2f-17f2-9fe2-11fba7565706.jpg",
            "id": "2950298e-cf2f-17f2-9fe2-11fba7565706",
            "name": "c2fe9a31-3da1-dabf-8622-fdb724c46279.jpg",
            "size": 37104,
            "createdAt": "2022-05-17T10:22:40+00:00",
            "updatedAt": "2022-05-17T10:22:40+00:00",
            "expiredAt": "2022-05-18T10:22:40+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "пл Юности",
            "house": "3А",
            "flat": null,
            "fias": null,
            "x": 37.212701817799,
            "y": 56.002668246751
        }
    }, {
        "id": 10181928,
        "name": "Акция Россия в моем сердце",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 июня 2022",
            "startDate": "2022-06-12T15:00:00+03:00",
            "endDate": "2022-06-12T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8ed8127d-727b-9c4b-85f3-09c356345d15.jpg",
            "id": "8ed8127d-727b-9c4b-85f3-09c356345d15",
            "name": "52fbf70a-7eae-6a4d-1c32-db807f82b873.jpg",
            "size": 21940,
            "createdAt": "2022-05-16T12:46:26+00:00",
            "updatedAt": "2022-05-16T12:46:26+00:00",
            "expiredAt": "2022-05-17T12:46:26+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "пл Юности",
            "house": "1",
            "flat": null,
            "fias": null,
            "x": 37.209089131632,
            "y": 56.00250136677
        }
    }, {
        "id": 10185660,
        "name": "Волонтерство в ГБУЗ «ГКБ им. А.К.Ерамишанцева ДЗМ» в МАЕ-ИЮНЕ  2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/be1f23f7f93b4b1c9d0d0633afc661d4.jpg",
            "id": "3e351d8e-1614-4961-d6a2-56de78ec1de2",
            "name": "be1f23f7f93b4b1c9d0d0633afc661d4.jpg",
            "size": null,
            "createdAt": "2020-04-23T08:21:02+00:00",
            "updatedAt": "2020-10-28T20:55:45+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Ленская, д 15",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Ленская",
            "house": "15",
            "flat": null,
            "fias": null,
            "x": 37.665944,
            "y": 55.865649
        }
    }, {
        "id": 10182596,
        "name": "Дежурство в магазине Детский Мир. Сбор помощи для беженцев",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "19 мая – 30 июня 2022",
            "startDate": "2022-05-19T10:00:00+03:00",
            "endDate": "2022-06-30T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/96d59f2f-83f6-50f8-472d-9a3010db5a20.jpg",
            "id": "96d59f2f-83f6-50f8-472d-9a3010db5a20",
            "name": "9ed615a8-a6a2-5320-98b8-0adfbd5b55d8.jpg",
            "size": 17767,
            "createdAt": "2022-05-17T17:50:20+00:00",
            "updatedAt": "2022-05-17T17:50:20+00:00",
            "expiredAt": "2022-05-18T17:50:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/327c8334-9dfc-ee2b-5e21-b0d205e0c957.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/327c8334-9dfc-ee2b-5e21-b0d205e0c957.jpg",
                "id": "327c8334-9dfc-ee2b-5e21-b0d205e0c957",
                "name": "377835ef-25a6-e041-cded-44e8d547c73b.jpg",
                "size": 12952,
                "createdAt": "2020-04-13T20:36:00+00:00",
                "updatedAt": "2020-04-13T20:36:00+00:00",
                "expiredAt": "2020-04-14T20:35:59+00:00"
            },
            "name": "АНО «Центр помощи и развития «ОкВеАн»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Адмирала Лазарева, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Южное Бутово",
            "settlementCodeDistrictCode": null,
            "street": "ул Адмирала Лазарева",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.543237,
            "y": 55.547036
        }
    }, {
        "id": 10124008,
        "name": "Волонтеры Зеленограда",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 20,
                "title": "Животные",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.png",
                "bgColor": "#1490C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "3 января – 31 декабря 2022",
            "startDate": "2022-01-03T10:00:00+03:00",
            "endDate": "2022-12-31T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c796227f-de12-0007-b535-b5f408109ee3.jpg",
            "id": "c796227f-de12-0007-b535-b5f408109ee3",
            "name": "d4dfd708-7c22-a53e-9eb3-b2bfe4d24ee3.jpg",
            "size": 12452,
            "createdAt": "2021-12-29T09:27:08+00:00",
            "updatedAt": "2021-12-29T09:27:08+00:00",
            "expiredAt": "2021-12-30T09:27:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "Россия, Москва, Зеленоград, к145",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": "145",
            "flat": null,
            "fias": null,
            "x": 37.207137634151,
            "y": 56.009252289337
        }
    }, {
        "id": 10185667,
        "name": "Волонтерство в ГБУЗ ГКБ им.В.В.Вересаева в  МАЕ-ИЮНЕ  2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c9638cd5-a8d5-0e64-9ef1-4bc453684b93.jpg",
            "id": "c9638cd5-a8d5-0e64-9ef1-4bc453684b93",
            "name": "5b5dfb64-55cd-6f8e-ade4-77e6253bc307.jpg",
            "size": 19846,
            "createdAt": "2020-12-03T15:47:28+00:00",
            "updatedAt": "2020-12-03T15:47:28+00:00",
            "expiredAt": "2020-12-04T15:47:28+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Лобненская, д 10",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Лобненская",
            "house": "10",
            "flat": null,
            "fias": null,
            "x": 37.532605,
            "y": 55.889738
        }
    }, {
        "id": 10138080,
        "name": "Акция С любовью и заботой",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 февраля – 30 июня 2022",
            "startDate": "2022-02-17T08:30:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a31ebe81-4aa7-a150-1e57-b6fd7822edb1.jpg",
            "id": "a31ebe81-4aa7-a150-1e57-b6fd7822edb1",
            "name": "f2c054c9-da12-31a9-9dd6-604b4205b878.jpg",
            "size": 23709,
            "createdAt": "2022-02-17T17:19:08+00:00",
            "updatedAt": "2022-02-17T17:19:08+00:00",
            "expiredAt": "2022-02-18T17:19:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10141388,
        "name": "Активные перемены",
        "categories": [{
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }
        ],
        "eventPeriod": {
            "shortTitle": "27 февраля – 30 июня 2022",
            "startDate": "2022-02-27T09:15:00+03:00",
            "endDate": "2022-06-30T16:00:00+03:00"
        },
        "imageFile": {
            "width": 200,
            "height": 155,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b26f40e5-15d9-795f-72c1-f7a4d5892537.jpg",
            "id": "b26f40e5-15d9-795f-72c1-f7a4d5892537",
            "name": "52c5f759-4008-cfc9-3201-309b441e733e.jpg",
            "size": 14036,
            "createdAt": "2022-02-27T17:11:17+00:00",
            "updatedAt": "2022-02-27T17:11:17+00:00",
            "expiredAt": "2022-02-28T17:11:17+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10193948,
        "name": "Поучительные сказки Леонида Пантелеева",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 июня 2022",
            "startDate": "2022-06-16T10:00:00+03:00",
            "endDate": "2022-06-16T13:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/E5266E-doctors.png",
            "id": "c834889f-afd5-e30d-ac36-ec1c672c3fbd",
            "name": "E5266E-doctors",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b459f97d-a4a4-f619-c49e-a375860c1625.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b459f97d-a4a4-f619-c49e-a375860c1625.jpg",
                "id": "b459f97d-a4a4-f619-c49e-a375860c1625",
                "name": "41ffde7b-bc98-cb5c-cd08-fd24f5e0a01c.jpg",
                "size": 5063,
                "createdAt": "2021-08-27T11:34:55+00:00",
                "updatedAt": "2021-08-27T11:34:55+00:00",
                "expiredAt": "2021-08-28T11:34:55+00:00"
            },
            "name": "ГБУК г. Москвы «ОКЦ ЮВАО» Библиодобровольцы",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Сормовская, д 6 к 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Выхино-Жулебино",
            "settlementCodeDistrictCode": null,
            "street": "ул Сормовская",
            "house": "6",
            "flat": null,
            "fias": null,
            "x": 37.8108,
            "y": 55.7111
        }
    }, {
        "id": 10193945,
        "name": "Делайте добро и помогайте - это гораздо проще, чем кажется '",
        "categories": [{
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T12:00:00+03:00",
            "endDate": "2022-06-11T14:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/FF5C00-doctorAndChild.png",
            "id": "cf74bb09-d396-fb2e-3030-97ea3ccfe8b4",
            "name": "FF5C00-doctorAndChild",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b459f97d-a4a4-f619-c49e-a375860c1625.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b459f97d-a4a4-f619-c49e-a375860c1625.jpg",
                "id": "b459f97d-a4a4-f619-c49e-a375860c1625",
                "name": "41ffde7b-bc98-cb5c-cd08-fd24f5e0a01c.jpg",
                "size": 5063,
                "createdAt": "2021-08-27T11:34:55+00:00",
                "updatedAt": "2021-08-27T11:34:55+00:00",
                "expiredAt": "2021-08-28T11:34:55+00:00"
            },
            "name": "ГБУК г. Москвы «ОКЦ ЮВАО» Библиодобровольцы",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Ташкентская, д 18 к 1, кв 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Выхино-Жулебино",
            "settlementCodeDistrictCode": null,
            "street": "ул Ташкентская",
            "house": "18",
            "flat": "1",
            "fias": null,
            "x": 37.8129973,
            "y": 55.7022995
        }
    }, {
        "id": 10082419,
        "name": "Развитие проекта «Следы Наций»",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 августа 2021 – 22 декабря 2025",
            "startDate": "2021-08-12T10:00:00+03:00",
            "endDate": "2025-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/aa425da0-26a9-9289-342b-0b72e1a754aa.jpg",
            "id": "aa425da0-26a9-9289-342b-0b72e1a754aa",
            "name": "aa7157bf-6a93-45c2-e179-b85afd2548ad.jpg",
            "size": 61764,
            "createdAt": "2021-08-29T07:50:51+00:00",
            "updatedAt": "2021-08-29T07:50:51+00:00",
            "expiredAt": "2021-08-30T07:50:51+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10179745,
        "name": "Военные оркестры в парках",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 мая – 21 августа 2022",
            "startDate": "2022-05-21T10:00:00+03:00",
            "endDate": "2022-08-21T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f8e4c984-07cd-3735-fa1b-69c4f6bcb672.png",
            "id": "f8e4c984-07cd-3735-fa1b-69c4f6bcb672",
            "name": "4e9e13d0-03ac-5e96-1bf5-bc99d3a06089.png",
            "size": 222329,
            "createdAt": "2022-05-11T11:52:53+00:00",
            "updatedAt": "2022-05-11T11:52:53+00:00",
            "expiredAt": "2022-05-12T11:52:53+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db877488-f472-8f03-734a-2d626d575b81.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/db877488-f472-8f03-734a-2d626d575b81.png",
                "id": "db877488-f472-8f03-734a-2d626d575b81",
                "name": "f7ed44ed-85e4-254c-bf9e-4ec09139e7ec.png",
                "size": 21955,
                "createdAt": "2022-04-10T13:00:12+00:00",
                "updatedAt": "2022-04-10T13:00:12+00:00",
                "expiredAt": "2022-04-11T13:00:12+00:00"
            },
            "name": "Союз волонтеров",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10085753,
        "name": "Психологи  Добромобиля",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10177743,
        "name": "Организация школьных мероприятий",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "6 мая – 30 июня 2022",
            "startDate": "2022-05-06T12:00:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/14cdde87-8204-0fe7-d0c3-390385c94d2c.jpg",
            "id": "14cdde87-8204-0fe7-d0c3-390385c94d2c",
            "name": "07946bd4-a4f3-1f41-de7a-dbf9f9ab3be8.jpg",
            "size": 33339,
            "createdAt": "2022-05-06T07:39:39+00:00",
            "updatedAt": "2022-05-06T07:39:39+00:00",
            "expiredAt": "2022-05-07T07:39:39+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10085749,
        "name": "Адвокаты  Добромобиля",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10084565,
        "name": "Центр Сопровождения Партнеров проекта «Следы Наций»",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "25 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-25T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/35602b40-f494-7412-16b1-e70c63b9d250.jpg",
            "id": "35602b40-f494-7412-16b1-e70c63b9d250",
            "name": "b746daa7-a830-c3aa-e625-f1bfc862f6db.jpg",
            "size": 50802,
            "createdAt": "2021-08-29T13:21:05+00:00",
            "updatedAt": "2021-08-29T13:21:05+00:00",
            "expiredAt": "2021-08-30T13:21:05+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10084858,
        "name": "Амбассадоры «Следов Наций» в Москве и за рубежом",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "26 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-26T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/26389792-7188-b67c-e2a9-68aeae9ea0f7.jpg",
            "id": "26389792-7188-b67c-e2a9-68aeae9ea0f7",
            "name": "006774bc-2320-482f-b7e7-92ea72b928a1.jpg",
            "size": 50802,
            "createdAt": "2021-08-29T07:57:38+00:00",
            "updatedAt": "2021-08-29T07:57:38+00:00",
            "expiredAt": "2021-08-30T07:57:38+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10085746,
        "name": "Сбор средств на проект Добромобиль",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10085752,
        "name": "Сбор компьютеров, ноутбуков, телефонов и планшетов для  раздачи  Добромобилем",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "31 августа 2021 – 22 декабря 2022",
            "startDate": "2021-08-31T10:00:00+03:00",
            "endDate": "2022-12-22T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/97131b0a-49fb-7149-989a-53ca3f9f3fc5.jpg",
            "id": "97131b0a-49fb-7149-989a-53ca3f9f3fc5",
            "name": "70292374-6556-cc3d-f56a-1b5bb33ffb8f.jpg",
            "size": 69605,
            "createdAt": "2021-08-30T18:56:59+00:00",
            "updatedAt": "2021-08-30T18:56:59+00:00",
            "expiredAt": "2021-08-31T18:56:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10090034,
        "name": "Волонтерские и KPI (КПЭ) программы для школ, школьников и учителей",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 сентября 2021 – 22 декабря 2022",
            "startDate": "2021-09-16T10:00:00+03:00",
            "endDate": "2022-12-22T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e582be5e-b72d-ea64-e43c-98c041700917.jpg",
            "id": "e582be5e-b72d-ea64-e43c-98c041700917",
            "name": "7b421a0a-78cb-515b-ee8f-9eff7e5a0fc7.jpg",
            "size": 61764,
            "createdAt": "2021-09-15T13:21:51+00:00",
            "updatedAt": "2021-09-15T13:21:51+00:00",
            "expiredAt": "2021-09-16T13:21:51+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/bd807205-0a65-71b7-f0d9-d2c977a375ce.jpg",
                "id": "bd807205-0a65-71b7-f0d9-d2c977a375ce",
                "name": "d2828d4a-68a4-fc6c-01ad-6cb2689778fb.jpg",
                "size": 29862,
                "createdAt": "2021-07-19T07:27:39+00:00",
                "updatedAt": "2021-07-19T07:27:39+00:00",
                "expiredAt": "2021-07-20T07:27:39+00:00"
            },
            "name": "ММООММ",
            "statistic": {
                "organizerRating": 4.8
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        }
    }, {
        "id": 10192425,
        "name": "Волонтёрь с факультетом Ксении Собчак!",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 27,
                "title": "Интеллектуальная помощь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/intellect.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/intellect.png",
                "bgColor": "#C6A914"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "17 – 18 июня 2022",
            "startDate": "2022-06-17T09:00:00+03:00",
            "endDate": "2022-06-18T16:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/60b43a63-4b26-de26-bb16-7b12b0d68401.png",
            "id": "60b43a63-4b26-de26-bb16-7b12b0d68401",
            "name": "de7d64f8-529a-2b89-9bc4-dfef52645416.png",
            "size": 65095,
            "createdAt": "2022-06-08T09:35:21+00:00",
            "updatedAt": "2022-06-08T09:35:21+00:00",
            "expiredAt": "2022-06-09T09:35:21+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
                "id": "ca1151b4-8815-7ee2-41d4-20c1ad17b418",
                "name": "5b86f101dbd04d42b60fd991fa342a70.jpg",
                "size": null,
                "createdAt": "2020-02-29T01:59:20+00:00",
                "updatedAt": "2020-12-03T07:51:33+00:00",
                "expiredAt": null
            },
            "name": "Волонтерский центр Университета Синергия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, шоссе Энтузиастов, д 5 стр 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Лефортово",
            "settlementCodeDistrictCode": null,
            "street": "шоссе Энтузиастов",
            "house": "5",
            "flat": null,
            "fias": null,
            "x": 37.696472,
            "y": 55.7494
        }
    }, {
        "id": 10189414,
        "name": "Волонтёры метро",
        "categories": [{
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 26,
                "title": "Урбанистика",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/urban.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/urban.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/urban.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/urban.png",
                "bgColor": "#6D14C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "1 – 30 июня 2022",
            "startDate": "2022-06-01T09:00:00+03:00",
            "endDate": "2022-06-30T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/16f16dad-adbd-bd78-b142-5400ccbaf8f3.jpg",
            "id": "16f16dad-adbd-bd78-b142-5400ccbaf8f3",
            "name": "3d63f395-7332-d27b-a3ce-e3ed5bd6beae.jpg",
            "size": 26054,
            "createdAt": "2022-06-01T12:43:52+00:00",
            "updatedAt": "2022-06-01T12:43:52+00:00",
            "expiredAt": "2022-06-02T12:43:52+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5b86f101dbd04d42b60fd991fa342a70.jpg",
                "id": "ca1151b4-8815-7ee2-41d4-20c1ad17b418",
                "name": "5b86f101dbd04d42b60fd991fa342a70.jpg",
                "size": null,
                "createdAt": "2020-02-29T01:59:20+00:00",
                "updatedAt": "2020-12-03T07:51:33+00:00",
                "expiredAt": null
            },
            "name": "Волонтерский центр Университета Синергия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Ленинградский пр-кт",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Сокол",
            "settlementCodeDistrictCode": null,
            "street": "Ленинградский пр-кт",
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.545626,
            "y": 55.794285
        }
    }, {
        "id": 10117428,
        "name": "Мастер-класс Секреты ёлочной гирлянды",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 декабря 2021 – 30 июня 2022",
            "startDate": "2021-12-08T08:30:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/FFA800-family.png",
            "id": "16fe04e2-aa49-aa21-16cf-63e09506c70e",
            "name": "FFA800-family",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10117433,
        "name": "Мастер-класс Волшебная вода и три ее состояния",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 декабря 2021 – 30 июня 2022",
            "startDate": "2021-12-08T08:30:00+03:00",
            "endDate": "2022-06-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/FFA800-family.png",
            "id": "16fe04e2-aa49-aa21-16cf-63e09506c70e",
            "name": "FFA800-family",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
            "iconFile": {
                "width": 100,
                "height": 100,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71daac74-0574-5fe2-bc16-d7db44d66f2c.png",
                "id": "71daac74-0574-5fe2-bc16-d7db44d66f2c",
                "name": "de9dc501-7047-e733-a562-f51766cbb5e9.png",
                "size": 29867,
                "createdAt": "2021-12-16T12:53:12+00:00",
                "updatedAt": "2021-12-16T12:53:12+00:00",
                "expiredAt": "2021-12-17T12:53:11+00:00"
            },
            "name": "Волонтерский отряд ГБОУ Школа №1770",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Коломенская наб, д 20",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Нагатинский Затон",
            "settlementCodeDistrictCode": null,
            "street": "Коломенская наб",
            "house": "20",
            "flat": null,
            "fias": null,
            "x": 37.69542,
            "y": 55.675966
        }
    }, {
        "id": 10189767,
        "name": "Мероприятие День молодежи",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "27 июня 2022",
            "startDate": "2022-06-27T10:00:00+03:00",
            "endDate": "2022-06-27T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/667d95c6-38d0-d744-d271-8b38d66f1f3a.jpg",
            "id": "667d95c6-38d0-d744-d271-8b38d66f1f3a",
            "name": "8472c6f9-639f-de88-fc6e-ab0d9144f111.jpg",
            "size": 43792,
            "createdAt": "2022-06-02T07:02:14+00:00",
            "updatedAt": "2022-06-02T07:02:14+00:00",
            "expiredAt": "2022-06-03T07:02:14+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
                "id": "ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad",
                "name": "10e601e4-c6ee-b5b1-7d25-d73dd0c58a22.png",
                "size": 17472,
                "createdAt": "2021-12-29T14:13:30+00:00",
                "updatedAt": "2021-12-29T14:13:30+00:00",
                "expiredAt": "2021-12-30T14:13:30+00:00"
            },
            "name": "Помогать с радостью",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10089316,
        "name": "Прогулки с особыми взрослыми из ПНИ 34",
        "categories": [{
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 28,
                "title": "Права человека",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.png",
                "bgColor": "#A09A00"
            }
        ],
        "eventPeriod": {
            "shortTitle": "13 сентября 2021 – 13 сентября 2022",
            "startDate": "2021-09-13T11:00:00+03:00",
            "endDate": "2022-09-13T13:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/075343dd-2c9d-36f3-4119-4a5ac38e4821.jpg",
            "id": "075343dd-2c9d-36f3-4119-4a5ac38e4821",
            "name": "2d3ebee1-8014-2c58-7fc5-5efe6e3622f6.jpg",
            "size": 32793,
            "createdAt": "2021-04-26T20:11:54+00:00",
            "updatedAt": "2021-04-26T20:11:54+00:00",
            "expiredAt": "2021-04-27T20:11:54+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
            "iconFile": {
                "width": 200,
                "height": 200,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
                "id": "b6cf0b66-61a8-801f-55db-5efc3eb21458",
                "name": "5fa1b809-a37a-589f-46af-a6703a23e563.jpg",
                "size": 6742,
                "createdAt": "2022-02-20T22:42:35+00:00",
                "updatedAt": "2022-02-20T22:42:35+00:00",
                "expiredAt": "2022-02-21T22:42:34+00:00"
            },
            "name": "РБОО Центр лечебной педагогики",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Москворечье, д 7",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Москворечье",
            "house": "7",
            "flat": null,
            "fias": null,
            "x": 37.644669,
            "y": 55.65137
        }
    }, {
        "id": 10178512,
        "name": "Прогулки с особыми детьми из детского дома-интерната",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "9 мая 2022 – 16 октября 2023",
            "startDate": "2022-05-09T11:00:00+03:00",
            "endDate": "2023-10-16T13:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/02842cac-5d2c-97b7-6671-96de471f013c.jpeg",
            "id": "02842cac-5d2c-97b7-6671-96de471f013c",
            "name": "a14a0472-43b0-e40d-d783-cee7377b1c31.jpeg",
            "size": 39836,
            "createdAt": "2021-02-09T21:38:05+00:00",
            "updatedAt": "2021-02-09T21:38:05+00:00",
            "expiredAt": "2021-02-10T21:38:05+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
            "iconFile": {
                "width": 200,
                "height": 200,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b6cf0b66-61a8-801f-55db-5efc3eb21458.jpg",
                "id": "b6cf0b66-61a8-801f-55db-5efc3eb21458",
                "name": "5fa1b809-a37a-589f-46af-a6703a23e563.jpg",
                "size": 6742,
                "createdAt": "2022-02-20T22:42:35+00:00",
                "updatedAt": "2022-02-20T22:42:35+00:00",
                "expiredAt": "2022-02-21T22:42:34+00:00"
            },
            "name": "РБОО Центр лечебной педагогики",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Профсоюзная, д 118А",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Коньково",
            "settlementCodeDistrictCode": null,
            "street": "ул Профсоюзная",
            "house": "118А",
            "flat": null,
            "fias": null,
            "x": 37.518416,
            "y": 55.636372
        }
    }, {
        "id": 10124652,
        "name": "Доставка продуктов питания пожилым и маломобильным гражданам",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "7 января – 31 июля 2022",
            "startDate": "2022-01-07T10:00:00+03:00",
            "endDate": "2022-07-31T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1a4e20eb-cf68-f26c-96da-419dd33e12d3.jpg",
            "id": "1a4e20eb-cf68-f26c-96da-419dd33e12d3",
            "name": "25276b9c-9d31-4f63-5c6d-ba447456e6e9.jpg",
            "size": 23334,
            "createdAt": "2022-01-06T16:54:20+00:00",
            "updatedAt": "2022-01-06T16:54:20+00:00",
            "expiredAt": "2022-01-07T16:54:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad.png",
                "id": "ca5ff8ea-e14f-aab9-ad3a-1d55610e62ad",
                "name": "10e601e4-c6ee-b5b1-7d25-d73dd0c58a22.png",
                "size": 17472,
                "createdAt": "2021-12-29T14:13:30+00:00",
                "updatedAt": "2021-12-29T14:13:30+00:00",
                "expiredAt": "2021-12-30T14:13:30+00:00"
            },
            "name": "Помогать с радостью",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "Россия, Москва, Коробейников переулок, 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Коробейников пер",
            "house": "8",
            "flat": null,
            "fias": null,
            "x": 37.600501205688,
            "y": 55.737666791423
        }
    }, {
        "id": 10185671,
        "name": "Волонтерство в ГБУЗ ГКБ №1 им.Н.И.Пирогова ДЗМ в МАЕ-ИЮНЕ 2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/09cbd10be19f4387b6bab76f63dcb0db.jpg",
            "id": "8e42d6da-f5e5-350e-468b-395f0db35b31",
            "name": "09cbd10be19f4387b6bab76f63dcb0db.jpg",
            "size": null,
            "createdAt": "2020-04-18T01:06:02+00:00",
            "updatedAt": "2020-10-29T19:39:24+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Ленинский пр-кт, д 8",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Ленинский пр-кт",
            "house": "8",
            "flat": null,
            "fias": null,
            "x": 37.604128,
            "y": 55.725518
        }
    }, {
        "id": 10189914,
        "name": "Соревнования по кроссу ко Дню России",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T08:30:00+03:00",
            "endDate": "2022-06-11T15:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/eee38127-9a93-1a6a-3b21-d4310aec7c68.jpg",
            "id": "eee38127-9a93-1a6a-3b21-d4310aec7c68",
            "name": "2a4ff20d-d8a7-a386-2462-d8f13e4839ed.jpg",
            "size": 41830,
            "createdAt": "2022-06-02T09:31:53+00:00",
            "updatedAt": "2022-06-02T09:31:53+00:00",
            "expiredAt": "2022-06-03T09:31:53+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.218349988442,
            "y": 56.004518809697
        }
    }, {
        "id": 10182364,
        "name": "Праздничная программа ко дню России в ГБУ Заря",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 июня 2022",
            "startDate": "2022-06-12T17:00:00+03:00",
            "endDate": "2022-06-12T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2950298e-cf2f-17f2-9fe2-11fba7565706.jpg",
            "id": "2950298e-cf2f-17f2-9fe2-11fba7565706",
            "name": "c2fe9a31-3da1-dabf-8622-fdb724c46279.jpg",
            "size": 37104,
            "createdAt": "2022-05-17T10:22:40+00:00",
            "updatedAt": "2022-05-17T10:22:40+00:00",
            "expiredAt": "2022-05-18T10:22:40+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "пл Юности",
            "house": "3А",
            "flat": null,
            "fias": null,
            "x": 37.212701817799,
            "y": 56.002668246751
        }
    }, {
        "id": 10181928,
        "name": "Акция Россия в моем сердце",
        "categories": [{
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 июня 2022",
            "startDate": "2022-06-12T15:00:00+03:00",
            "endDate": "2022-06-12T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8ed8127d-727b-9c4b-85f3-09c356345d15.jpg",
            "id": "8ed8127d-727b-9c4b-85f3-09c356345d15",
            "name": "52fbf70a-7eae-6a4d-1c32-db807f82b873.jpg",
            "size": 21940,
            "createdAt": "2022-05-16T12:46:26+00:00",
            "updatedAt": "2022-05-16T12:46:26+00:00",
            "expiredAt": "2022-05-17T12:46:26+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоградский административный округ, район Матушкино",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "пл Юности",
            "house": "1",
            "flat": null,
            "fias": null,
            "x": 37.209089131632,
            "y": 56.00250136677
        }
    }, {
        "id": 10185660,
        "name": "Волонтерство в ГБУЗ «ГКБ им. А.К.Ерамишанцева ДЗМ» в МАЕ-ИЮНЕ  2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/be1f23f7f93b4b1c9d0d0633afc661d4.jpg",
            "id": "3e351d8e-1614-4961-d6a2-56de78ec1de2",
            "name": "be1f23f7f93b4b1c9d0d0633afc661d4.jpg",
            "size": null,
            "createdAt": "2020-04-23T08:21:02+00:00",
            "updatedAt": "2020-10-28T20:55:45+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Ленская, д 15",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Ленская",
            "house": "15",
            "flat": null,
            "fias": null,
            "x": 37.665944,
            "y": 55.865649
        }
    }, {
        "id": 10182596,
        "name": "Дежурство в магазине Детский Мир. Сбор помощи для беженцев",
        "categories": [{
                "id": 16,
                "title": "ЧС",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/911.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/911.png",
                "bgColor": "#C51212"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "19 мая – 30 июня 2022",
            "startDate": "2022-05-19T10:00:00+03:00",
            "endDate": "2022-06-30T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/96d59f2f-83f6-50f8-472d-9a3010db5a20.jpg",
            "id": "96d59f2f-83f6-50f8-472d-9a3010db5a20",
            "name": "9ed615a8-a6a2-5320-98b8-0adfbd5b55d8.jpg",
            "size": 17767,
            "createdAt": "2022-05-17T17:50:20+00:00",
            "updatedAt": "2022-05-17T17:50:20+00:00",
            "expiredAt": "2022-05-18T17:50:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/327c8334-9dfc-ee2b-5e21-b0d205e0c957.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/327c8334-9dfc-ee2b-5e21-b0d205e0c957.jpg",
                "id": "327c8334-9dfc-ee2b-5e21-b0d205e0c957",
                "name": "377835ef-25a6-e041-cded-44e8d547c73b.jpg",
                "size": 12952,
                "createdAt": "2020-04-13T20:36:00+00:00",
                "updatedAt": "2020-04-13T20:36:00+00:00",
                "expiredAt": "2020-04-14T20:35:59+00:00"
            },
            "name": "АНО «Центр помощи и развития «ОкВеАн»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Адмирала Лазарева, д 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Южное Бутово",
            "settlementCodeDistrictCode": null,
            "street": "ул Адмирала Лазарева",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.543237,
            "y": 55.547036
        }
    }, {
        "id": 10124008,
        "name": "Волонтеры Зеленограда",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 20,
                "title": "Животные",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.png",
                "bgColor": "#1490C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "3 января – 31 декабря 2022",
            "startDate": "2022-01-03T10:00:00+03:00",
            "endDate": "2022-12-31T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c796227f-de12-0007-b535-b5f408109ee3.jpg",
            "id": "c796227f-de12-0007-b535-b5f408109ee3",
            "name": "d4dfd708-7c22-a53e-9eb3-b2bfe4d24ee3.jpg",
            "size": 12452,
            "createdAt": "2021-12-29T09:27:08+00:00",
            "updatedAt": "2021-12-29T09:27:08+00:00",
            "expiredAt": "2021-12-30T09:27:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/de4674d5-5170-bc83-4e7d-cdc142aa8d51.png",
                "id": "de4674d5-5170-bc83-4e7d-cdc142aa8d51",
                "name": "73d91de5-8e7b-2204-9af2-6d8a14a070de.png",
                "size": 16887,
                "createdAt": "2021-10-12T20:03:31+00:00",
                "updatedAt": "2021-10-12T20:03:31+00:00",
                "expiredAt": "2021-10-13T20:03:31+00:00"
            },
            "name": "#ВолонтерыЗеленограда",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "Россия, Москва, Зеленоград, к145",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": "145",
            "flat": null,
            "fias": null,
            "x": 37.207137634151,
            "y": 56.009252289337
        }
    }, {
        "id": 10185667,
        "name": "Волонтерство в ГБУЗ ГКБ им.В.В.Вересаева в МАЕ-ИЮНЕ 2022",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "24 мая – 30 июня 2022",
            "startDate": "2022-05-24T10:00:00+03:00",
            "endDate": "2022-06-30T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c9638cd5-a8d5-0e64-9ef1-4bc453684b93.jpg",
            "id": "c9638cd5-a8d5-0e64-9ef1-4bc453684b93",
            "name": "5b5dfb64-55cd-6f8e-ade4-77e6253bc307.jpg",
            "size": 19846,
            "createdAt": "2020-12-03T15:47:28+00:00",
            "updatedAt": "2020-12-03T15:47:28+00:00",
            "expiredAt": "2020-12-04T15:47:28+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/61bc45f1-8ff0-7e9a-efc8-7d83e32fc240.png",
                "id": "61bc45f1-8ff0-7e9a-efc8-7d83e32fc240",
                "name": "3f5e457d-38aa-d666-8513-2ce6836e512a.png",
                "size": 5512,
                "createdAt": "2021-02-12T08:00:00+00:00",
                "updatedAt": "2021-02-12T08:00:00+00:00",
                "expiredAt": "2021-02-13T08:00:00+00:00"
            },
            "name": "АНО Русская Гуманитарная Миссия",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Лобненская, д 10",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Лобненская",
            "house": "10",
            "flat": null,
            "fias": null,
            "x": 37.532605,
            "y": 55.889738
        }
    }, {
        "id": 10122570,
        "name": "Посещение приюта для кошек",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 20,
                "title": "Животные",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.png",
                "bgColor": "#1490C6"
            }, {
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }
        ],
        "eventPeriod": {
            "shortTitle": "29 декабря 2021 – 29 декабря 2022",
            "startDate": "2021-12-29T12:00:00+03:00",
            "endDate": "2022-12-29T15:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/acdbcf31-6f34-015d-4e1f-11a449178b98.jpg",
            "id": "acdbcf31-6f34-015d-4e1f-11a449178b98",
            "name": "cb32ae63-9093-dd38-d2c3-a7bd93884513.jpg",
            "size": 37642,
            "createdAt": "2021-12-23T11:36:20+00:00",
            "updatedAt": "2021-12-23T11:36:20+00:00",
            "expiredAt": "2021-12-24T11:36:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
                "id": "8c5d54a5-df77-7522-5c97-6e326e2d8a7f",
                "name": "ea9fb32e-cc15-ff89-84c3-06615fe72c64.jpeg",
                "size": 6627,
                "createdAt": "2021-01-13T19:01:29+00:00",
                "updatedAt": "2021-01-13T19:01:29+00:00",
                "expiredAt": "2021-01-14T19:01:29+00:00"
            },
            "name": "Волонтерское движение «Рука к руке»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, г Зеленоград",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.21439,
            "y": 55.991893
        }
    }, {
        "id": 10122584,
        "name": "Тепло и уют",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 20,
                "title": "Животные",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/animal.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/animal.png",
                "bgColor": "#1490C6"
            }, {
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }
        ],
        "eventPeriod": {
            "shortTitle": "27 декабря 2021 – 30 декабря 2022",
            "startDate": "2021-12-27T12:00:00+03:00",
            "endDate": "2022-12-30T16:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e33ecbe2-1980-e2ce-b1a8-5b4f61dc14cd.jpg",
            "id": "e33ecbe2-1980-e2ce-b1a8-5b4f61dc14cd",
            "name": "1250e4e3-3372-c25e-8a07-8b83671f15d5.jpg",
            "size": 38033,
            "createdAt": "2021-12-23T12:10:40+00:00",
            "updatedAt": "2021-12-23T12:10:40+00:00",
            "expiredAt": "2021-12-24T12:10:40+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
                "id": "8c5d54a5-df77-7522-5c97-6e326e2d8a7f",
                "name": "ea9fb32e-cc15-ff89-84c3-06615fe72c64.jpeg",
                "size": 6627,
                "createdAt": "2021-01-13T19:01:29+00:00",
                "updatedAt": "2021-01-13T19:01:29+00:00",
                "expiredAt": "2021-01-14T19:01:29+00:00"
            },
            "name": "Волонтерское движение «Рука к руке»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, г Зеленоград",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.21439,
            "y": 55.991893
        }
    }, {
        "id": 10128514,
        "name": "Игровые программы для детей в ТЖС и с ОВЗ",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "20 января – 31 декабря 2022",
            "startDate": "2022-01-20T10:00:00+03:00",
            "endDate": "2022-12-31T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/595b1fa6-0305-c135-c509-a9892cedd2c2.jpg",
            "id": "595b1fa6-0305-c135-c509-a9892cedd2c2",
            "name": "aebbf653-b13a-395a-8c0d-bbfb551cddd5.jpg",
            "size": 36134,
            "createdAt": "2022-01-20T13:46:10+00:00",
            "updatedAt": "2022-01-20T13:46:10+00:00",
            "expiredAt": "2022-01-21T13:46:10+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8c5d54a5-df77-7522-5c97-6e326e2d8a7f.jpeg",
                "id": "8c5d54a5-df77-7522-5c97-6e326e2d8a7f",
                "name": "ea9fb32e-cc15-ff89-84c3-06615fe72c64.jpeg",
                "size": 6627,
                "createdAt": "2021-01-13T19:01:29+00:00",
                "updatedAt": "2021-01-13T19:01:29+00:00",
                "expiredAt": "2021-01-14T19:01:29+00:00"
            },
            "name": "Волонтерское движение «Рука к руке»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, г Зеленоград",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.21439,
            "y": 55.991893
        }
    }, {
        "id": 10192586,
        "name": "Группы летней активности",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }
        ],
        "eventPeriod": {
            "shortTitle": "13 июня 2022",
            "startDate": "2022-06-13T09:00:00+03:00",
            "endDate": "2022-06-13T13:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/454afeea-37b8-92e4-0863-1a23a3e94853.jpg",
            "id": "454afeea-37b8-92e4-0863-1a23a3e94853",
            "name": "090f6fce-4a86-eb42-4f28-4597a3690a9c.jpg",
            "size": 33905,
            "createdAt": "2022-06-02T19:34:59+00:00",
            "updatedAt": "2022-06-02T19:34:59+00:00",
            "expiredAt": "2022-06-03T19:34:59+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71636bf2-cd91-e63c-c2b8-c6465223f24d.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/71636bf2-cd91-e63c-c2b8-c6465223f24d.jpg",
                "id": "71636bf2-cd91-e63c-c2b8-c6465223f24d",
                "name": "4fc70f3c-b7e1-b594-c080-5ed8738224db.jpg",
                "size": 31704,
                "createdAt": "2022-06-02T10:37:45+00:00",
                "updatedAt": "2022-06-02T10:37:45+00:00",
                "expiredAt": "2022-06-03T10:37:45+00:00"
            },
            "name": "ГБОУ Школа №1935",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, улица Авиаконструктора Миля, 18к2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Авиаконструктора Миля",
            "house": "18",
            "flat": null,
            "fias": null,
            "x": 37.855464376677,
            "y": 55.680522405835
        }
    }, {
        "id": 10180425,
        "name": "Мониторинг основного периода проведения ГИА 2022",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 28,
                "title": "Права человека",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/rights.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/rights.png",
                "bgColor": "#A09A00"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "19 мая – 9 июля 2022",
            "startDate": "2022-05-19T09:00:00+03:00",
            "endDate": "2022-07-09T16:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/fd3f01fb-da5e-02cd-980f-1db89c70ac6d.jpg",
            "id": "fd3f01fb-da5e-02cd-980f-1db89c70ac6d",
            "name": "d2f32d92-aa73-de98-0ef5-c9fe7f4e809e.jpg",
            "size": 23068,
            "createdAt": "2022-05-12T13:37:16+00:00",
            "updatedAt": "2022-05-12T13:37:16+00:00",
            "expiredAt": "2022-05-13T13:37:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/55e2fad9a00b4cf8bd459c27b0a06526.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/55e2fad9a00b4cf8bd459c27b0a06526.jpg",
                "id": "68befb23-1eeb-7d81-7024-d09a72f58873",
                "name": "55e2fad9a00b4cf8bd459c27b0a06526.jpg",
                "size": null,
                "createdAt": "2020-02-29T02:36:01+00:00",
                "updatedAt": "2020-10-29T07:11:24+00:00",
                "expiredAt": null
            },
            "name": "Общероссийская общественная организация Российский Союз Молодёжи",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10191223,
        "name": "Турнир и маневры “333 Богатыря” по Современному Мечевому Бою в честь Дня России",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 июня 2022",
            "startDate": "2022-06-12T09:00:00+03:00",
            "endDate": "2022-06-12T19:00:00+03:00"
        },
        "imageFile": {
            "width": 212,
            "height": 165,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2bc9feaa-8f52-8a69-720a-500b27824437.jpg",
            "id": "2bc9feaa-8f52-8a69-720a-500b27824437",
            "name": "023c337a-8cad-dbc1-59f6-3c49ed377c1b.jpg",
            "size": 22973,
            "createdAt": "2022-06-06T08:20:06+00:00",
            "updatedAt": "2022-06-06T08:20:06+00:00",
            "expiredAt": "2022-06-07T08:20:06+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3335ab95-c08e-6b7d-74da-9fef6ce8f198.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3335ab95-c08e-6b7d-74da-9fef6ce8f198.jpg",
                "id": "3335ab95-c08e-6b7d-74da-9fef6ce8f198",
                "name": "b3ce94bb-bb92-ce53-d199-4ef0889f18f8.jpg",
                "size": 11939,
                "createdAt": "2022-02-02T12:29:21+00:00",
                "updatedAt": "2022-02-02T12:29:21+00:00",
                "expiredAt": "2022-02-03T12:29:21+00:00"
            },
            "name": "Общероссийская Федерация современного мечевого боя России",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Лодочная, д 15 стр 1А",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Южное Тушино",
            "settlementCodeDistrictCode": null,
            "street": "ул Лодочная",
            "house": "15",
            "flat": null,
            "fias": null,
            "x": 37.45945,
            "y": 55.838955
        }
    }, {
        "id": 10105863,
        "name": "Школа Волонтёров",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "12 ноября 2021 – 14 июня 2022",
            "startDate": "2021-11-12T20:00:00+03:00",
            "endDate": "2022-06-14T21:30:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/46b491d7-dc05-a3b8-dea7-abae38074a4a.PNG",
            "id": "46b491d7-dc05-a3b8-dea7-abae38074a4a",
            "name": "16c11997-f9af-3f54-a0c7-52da18b5cd6c.PNG",
            "size": 147999,
            "createdAt": "2021-11-11T14:00:23+00:00",
            "updatedAt": "2021-11-11T14:00:23+00:00",
            "expiredAt": "2021-11-12T14:00:23+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f1b74807-3f2f-0777-b662-7559154e2a28.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f1b74807-3f2f-0777-b662-7559154e2a28.jpg",
                "id": "f1b74807-3f2f-0777-b662-7559154e2a28",
                "name": "bc6c7fb3-724f-bca5-626a-047c9dfaf4f6.jpg",
                "size": 7894,
                "createdAt": "2021-11-11T10:00:08+00:00",
                "updatedAt": "2021-11-11T10:00:08+00:00",
                "expiredAt": "2021-11-12T10:00:08+00:00"
            },
            "name": "АНОТД Содружество увлеченных",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Перовская, д 66 к 3",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Новогиреево",
            "settlementCodeDistrictCode": null,
            "street": "ул Перовская",
            "house": "66",
            "flat": null,
            "fias": null,
            "x": 37.799213,
            "y": 55.74352
        }
    }, {
        "id": 10128164,
        "name": "Раздача листовок о пользе ЗОЖ",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }
        ],
        "eventPeriod": {
            "shortTitle": "19 января 2022 – 19 января 2023",
            "startDate": "2022-01-19T09:00:00+03:00",
            "endDate": "2023-01-19T21:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/C008B9-doctorMan.png",
            "id": "041479d7-df08-a047-ee0c-e5b70f9502ac",
            "name": "C008B9-doctorMan",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/46a87f19-2c93-d208-553c-841e18596df1.jpeg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/46a87f19-2c93-d208-553c-841e18596df1.jpeg",
                "id": "46a87f19-2c93-d208-553c-841e18596df1",
                "name": "02b57190-5940-4804-d587-0ae84682e10d.jpeg",
                "size": 18429,
                "createdAt": "2021-10-25T18:15:20+00:00",
                "updatedAt": "2021-10-25T18:15:20+00:00",
                "expiredAt": "2021-10-26T18:15:20+00:00"
            },
            "name": "Мед-худ-центр",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10132784,
        "name": "Close Contact team",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "14 февраля – 30 июня 2022",
            "startDate": "2022-02-14T11:00:00+03:00",
            "endDate": "2022-06-30T13:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/3d3848672ce7439b8a1eca4d38a0fb90.jpg",
            "id": "d917ce76-bca2-acb2-2f57-1761fe40c01d",
            "name": "3d3848672ce7439b8a1eca4d38a0fb90.jpg",
            "size": null,
            "createdAt": "2020-02-29T04:50:40+00:00",
            "updatedAt": "2020-12-03T09:26:42+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/b8d9874d55bb46239b1e92e4f1b0cfd5.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/b8d9874d55bb46239b1e92e4f1b0cfd5.jpg",
                "id": "51ab321e-cf19-0eca-fa3b-1a93f77ea67e",
                "name": "b8d9874d55bb46239b1e92e4f1b0cfd5.jpg",
                "size": null,
                "createdAt": "2020-02-29T03:04:37+00:00",
                "updatedAt": "2020-10-29T02:45:10+00:00",
                "expiredAt": null
            },
            "name": "МОАУ СОШ с УИОП No10 г. Кирова",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "ул. Розы Люксембург, 57",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Розы Люксембург",
            "house": "31",
            "flat": null,
            "fias": null,
            "x": 37.8706743,
            "y": 55.7005664
        }
    }, {
        "id": 10185635,
        "name": "Школа Волонтеров",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "18 июня 2022",
            "startDate": "2022-06-18T11:00:00+03:00",
            "endDate": "2022-06-18T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/57dc3a35-dba0-f6ef-76b3-63e31a4a3d03.jpeg",
            "id": "57dc3a35-dba0-f6ef-76b3-63e31a4a3d03",
            "name": "8ae351c8-6c48-4022-5238-abe2bbaf1405.jpeg",
            "size": 17595,
            "createdAt": "2022-03-28T09:54:09+00:00",
            "updatedAt": "2022-03-28T09:54:09+00:00",
            "expiredAt": "2022-03-29T09:54:09+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5b0b0395-29d9-07bb-2120-0f32e045cc25.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5b0b0395-29d9-07bb-2120-0f32e045cc25.jpg",
                "id": "5b0b0395-29d9-07bb-2120-0f32e045cc25",
                "name": "9e3ae14a-44e4-c257-fb4a-c8a68c6dcee7.jpg",
                "size": 18270,
                "createdAt": "2022-03-31T08:47:42+00:00",
                "updatedAt": "2022-03-31T08:47:42+00:00",
                "expiredAt": "2022-04-01T08:47:42+00:00"
            },
            "name": "Благотворительный фонд поддержки семьи, материнства и детства Женщины за жизнь",
            "statistic": {
                "organizerRating": 4.3
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, 4-й Вешняковский проезд, д 1 к 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Рязанский р-н",
            "settlementCodeDistrictCode": null,
            "street": "4-й Вешняковский проезд",
            "house": "1",
            "flat": null,
            "fias": null,
            "x": 37.793247,
            "y": 55.718304
        }
    }, {
        "id": 10193731,
        "name": "Акция «Добрый короб»",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "8 июля 2022",
            "startDate": "2022-07-08T16:00:00+03:00",
            "endDate": "2022-07-08T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7110898a-e2e5-e102-7cfc-f2b8db724366.jpg",
            "id": "7110898a-e2e5-e102-7cfc-f2b8db724366",
            "name": "111429f9-4333-5781-b557-ddf903e2839f.jpg",
            "size": 25481,
            "createdAt": "2022-05-31T09:29:46+00:00",
            "updatedAt": "2022-05-31T09:29:46+00:00",
            "expiredAt": "2022-06-01T09:29:46+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5b0b0395-29d9-07bb-2120-0f32e045cc25.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5b0b0395-29d9-07bb-2120-0f32e045cc25.jpg",
                "id": "5b0b0395-29d9-07bb-2120-0f32e045cc25",
                "name": "9e3ae14a-44e4-c257-fb4a-c8a68c6dcee7.jpg",
                "size": 18270,
                "createdAt": "2022-03-31T08:47:42+00:00",
                "updatedAt": "2022-03-31T08:47:42+00:00",
                "expiredAt": "2022-04-01T08:47:42+00:00"
            },
            "name": "Благотворительный фонд поддержки семьи, материнства и детства Женщины за жизнь",
            "statistic": {
                "organizerRating": 4.3
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Авиационная, д 66",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Щукино",
            "settlementCodeDistrictCode": null,
            "street": "ул Авиационная",
            "house": "66",
            "flat": null,
            "fias": null,
            "x": 37.460098,
            "y": 55.809687
        }
    }, {
        "id": 10159677,
        "name": "Проведение кулинарных и творческих мастер-классов для детей с ОВЗ",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "7 апреля – 31 декабря 2022",
            "startDate": "2022-04-07T16:30:00+03:00",
            "endDate": "2022-12-31T17:30:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7ca734e1-5181-6f08-ee32-838dcd2574a5.jpg",
            "id": "7ca734e1-5181-6f08-ee32-838dcd2574a5",
            "name": "e2f54d93-c456-7649-6075-59d310b7e781.jpg",
            "size": 30104,
            "createdAt": "2022-04-07T21:17:30+00:00",
            "updatedAt": "2022-04-07T21:17:30+00:00",
            "expiredAt": "2022-04-08T21:17:30+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/048ff3603e244dff8c241d729c36cf5e.jpg",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/048ff3603e244dff8c241d729c36cf5e.jpg",
                "id": "fd656307-eee9-c8ee-6e5e-cd748f68bf79",
                "name": "048ff3603e244dff8c241d729c36cf5e.jpg",
                "size": null,
                "createdAt": "2020-02-29T02:20:04+00:00",
                "updatedAt": "2020-12-11T01:46:08+00:00",
                "expiredAt": null
            },
            "name": "Благотворительный фонд Цвет жизни",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Исаковского, д 2 к 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Строгино",
            "settlementCodeDistrictCode": null,
            "street": "ул Исаковского",
            "house": "2",
            "flat": null,
            "fias": null,
            "x": 37.395239,
            "y": 55.817603
        }
    }, {
        "id": 10193127,
        "name": "Квест «В поисках сокровищ»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 июня 2022",
            "startDate": "2022-06-10T09:00:00+03:00",
            "endDate": "2022-06-10T12:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/0094FF-doctorAndMan.png",
            "id": "65e766cc-e91e-e7b5-0676-d543971ed5bd",
            "name": "0094FF-doctorAndMan",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/62e540ff-353b-6ae4-d6c2-e9b71d6bc56f.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/62e540ff-353b-6ae4-d6c2-e9b71d6bc56f.jpg",
                "id": "62e540ff-353b-6ae4-d6c2-e9b71d6bc56f",
                "name": "db4bc626-a528-7fe8-8c2a-cad1f447fbc6.jpg",
                "size": 19785,
                "createdAt": "2021-09-15T11:56:12+00:00",
                "updatedAt": "2021-09-15T11:56:12+00:00",
                "expiredAt": "2021-09-16T11:56:12+00:00"
            },
            "name": "ГБОУ школа Интеграл ",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Барклая, д 5А",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Филевский парк",
            "settlementCodeDistrictCode": null,
            "street": "ул Барклая",
            "house": "5А",
            "flat": null,
            "fias": null,
            "x": 37.502669,
            "y": 55.738809
        }
    }, {
        "id": 10193596,
        "name": "ИНТЕЛЛЕКТУАЛЬНО-РАЗВЛЕКАТЕЛЬНОЙ ИГРЫ  «Летний КВИЗ»",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }
        ],
        "eventPeriod": {
            "shortTitle": "10 июня 2022",
            "startDate": "2022-06-10T13:00:00+03:00",
            "endDate": "2022-06-10T15:00:00+03:00"
        },
        "imageFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/default-images\/C008B9-hands.png",
            "id": "56df6b4e-3c84-9485-2fdb-570e264719a5",
            "name": "C008B9-hands",
            "size": null,
            "createdAt": "2020-12-28T21:05:40+00:00",
            "updatedAt": "2020-12-28T21:05:40+00:00",
            "expiredAt": null
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/62e540ff-353b-6ae4-d6c2-e9b71d6bc56f.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/62e540ff-353b-6ae4-d6c2-e9b71d6bc56f.jpg",
                "id": "62e540ff-353b-6ae4-d6c2-e9b71d6bc56f",
                "name": "db4bc626-a528-7fe8-8c2a-cad1f447fbc6.jpg",
                "size": 19785,
                "createdAt": "2021-09-15T11:56:12+00:00",
                "updatedAt": "2021-09-15T11:56:12+00:00",
                "expiredAt": "2021-09-16T11:56:12+00:00"
            },
            "name": "ГБОУ школа Интеграл",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Барклая, д 5А",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Филевский парк",
            "settlementCodeDistrictCode": null,
            "street": "ул Барклая",
            "house": "5А",
            "flat": null,
            "fias": null,
            "x": 37.502669,
            "y": 55.738809
        }
    }, {
        "id": 10153480,
        "name": "Общественный центр сбора гуманитарной помощи для жителей Донбасса",
        "categories": [{
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "30 марта – 30 сентября 2022",
            "startDate": "2022-03-30T08:00:00+03:00",
            "endDate": "2022-09-30T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f05e8c3a-dba1-cd90-65b7-ae95f966e513.jpg",
            "id": "f05e8c3a-dba1-cd90-65b7-ae95f966e513",
            "name": "566c79ce-bb6b-7cf3-3c75-854c1a72eaf6.jpg",
            "size": 32049,
            "createdAt": "2022-03-29T14:18:19+00:00",
            "updatedAt": "2022-03-29T14:18:19+00:00",
            "expiredAt": "2022-03-30T14:18:19+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
                "id": "2e238374-5c55-1676-13dc-09a9ba5e4d31",
                "name": "bb51ff73-847f-6cf5-e53f-6ae4efcfc9d4.jpg",
                "size": 19323,
                "createdAt": "2021-07-29T07:24:22+00:00",
                "updatedAt": "2021-07-29T07:24:22+00:00",
                "expiredAt": "2021-07-30T07:24:22+00:00"
            },
            "name": "Волонтерский корпус Общественной палаты РФ",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Миусская пл, д 7",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "Миусская пл",
            "house": "7",
            "flat": null,
            "fias": null,
            "x": 37.593012,
            "y": 55.778709
        }
    }, {
        "id": 10180937,
        "name": "Патриотический проект по увековечению имён погибших участников Великой Отечественной войны",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 мая – 1 сентября 2022",
            "startDate": "2022-05-16T10:00:00+03:00",
            "endDate": "2022-09-01T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9a42ec04-7a24-9fe7-19ab-54843c645900.jpg",
            "id": "9a42ec04-7a24-9fe7-19ab-54843c645900",
            "name": "7b6050db-2c8c-fa3b-c738-11904a81c627.jpg",
            "size": 29382,
            "createdAt": "2022-05-13T16:02:09+00:00",
            "updatedAt": "2022-05-13T16:02:09+00:00",
            "expiredAt": "2022-05-14T16:02:09+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
                "id": "2e238374-5c55-1676-13dc-09a9ba5e4d31",
                "name": "bb51ff73-847f-6cf5-e53f-6ae4efcfc9d4.jpg",
                "size": 19323,
                "createdAt": "2021-07-29T07:24:22+00:00",
                "updatedAt": "2021-07-29T07:24:22+00:00",
                "expiredAt": "2021-07-30T07:24:22+00:00"
            },
            "name": "Волонтерский корпус Общественной палаты РФ",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Миусская пл, д 7",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "Миусская пл",
            "house": "7",
            "flat": null,
            "fias": null,
            "x": 37.593012,
            "y": 55.778709
        }
    }, {
        "id": 10181863,
        "name": "III Общероссийская конференция «Устойчивое развитие этнокультурного сектора»",
        "categories": [{
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "16 – 17 июня 2022",
            "startDate": "2022-06-16T08:30:00+03:00",
            "endDate": "2022-06-17T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6141cadd-c600-3eca-079d-e108df12fff2.jpg",
            "id": "6141cadd-c600-3eca-079d-e108df12fff2",
            "name": "8e58f6dd-b953-6889-5243-2349dc65c8b1.jpg",
            "size": 29621,
            "createdAt": "2022-05-16T11:13:18+00:00",
            "updatedAt": "2022-05-16T11:13:18+00:00",
            "expiredAt": "2022-05-17T11:13:18+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e238374-5c55-1676-13dc-09a9ba5e4d31.jpg",
                "id": "2e238374-5c55-1676-13dc-09a9ba5e4d31",
                "name": "bb51ff73-847f-6cf5-e53f-6ae4efcfc9d4.jpg",
                "size": 19323,
                "createdAt": "2021-07-29T07:24:22+00:00",
                "updatedAt": "2021-07-29T07:24:22+00:00",
                "expiredAt": "2021-07-30T07:24:22+00:00"
            },
            "name": "Волонтерский корпус Общественной палаты РФ",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, Миусская пл, д 7",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "Миусская пл",
            "house": "7",
            "flat": null,
            "fias": null,
            "x": 37.593012,
            "y": 55.778709
        }
    }, {
        "id": 10192665,
        "name": "Благоустройство территории Бутовского полигона",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }, {
                "id": 25,
                "title": "Поиск пропавших",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/find.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/find.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/find.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/find.png",
                "bgColor": "#C63E14"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T11:00:00+03:00",
            "endDate": "2022-06-11T15:30:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/144807d9-79e4-2c34-d5d4-a04ce8dbe540.jpg",
            "id": "144807d9-79e4-2c34-d5d4-a04ce8dbe540",
            "name": "7a0a634e-5ae4-8464-e3c2-44cccd85e143.jpg",
            "size": 45318,
            "createdAt": "2022-05-14T09:05:00+00:00",
            "updatedAt": "2022-05-14T09:05:00+00:00",
            "expiredAt": "2022-05-15T09:05:00+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7e378029-b2cf-bce7-7c2d-b23423115a79.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7e378029-b2cf-bce7-7c2d-b23423115a79.jpg",
                "id": "7e378029-b2cf-bce7-7c2d-b23423115a79",
                "name": "9d80a810-34f1-ba7a-0f75-8568d083a60b.jpg",
                "size": 6793,
                "createdAt": "2020-09-09T14:39:22+00:00",
                "updatedAt": "2020-09-09T14:39:22+00:00",
                "expiredAt": "2020-09-10T14:39:22+00:00"
            },
            "name": "АНО МНПЦ Бутово",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, б-р Дмитрия Донского",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Северное Бутово",
            "settlementCodeDistrictCode": null,
            "street": "б-р Дмитрия Донского",
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.576016,
            "y": 55.569306
        }
    }, {
        "id": 10166981,
        "name": "Всероссийский день защиты памятников истории и культуры",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 апреля 2022",
            "startDate": "2022-04-21T09:00:00+03:00",
            "endDate": "2022-04-21T18:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/45b18efc-ca6d-f596-33b5-496e59e89a2c.jpg",
            "id": "45b18efc-ca6d-f596-33b5-496e59e89a2c",
            "name": "6ff62420-5287-f189-59c6-bef39267fa39.jpg",
            "size": 32102,
            "createdAt": "2022-04-19T06:53:44+00:00",
            "updatedAt": "2022-04-19T06:53:44+00:00",
            "expiredAt": "2022-04-20T06:53:44+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8098feb9-095d-1af0-24b5-055686c8eb5d.png",
            "iconFile": {
                "width": 80,
                "height": 80,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8098feb9-095d-1af0-24b5-055686c8eb5d.png",
                "id": "8098feb9-095d-1af0-24b5-055686c8eb5d",
                "name": "5d40fe85-9ba9-9ed8-cf67-f2cb1fa7f2c3.png",
                "size": 9680,
                "createdAt": "2022-03-17T08:47:58+00:00",
                "updatedAt": "2022-03-17T08:47:58+00:00",
                "expiredAt": "2022-03-18T08:47:58+00:00"
            },
            "name": "МЦХШ при РАХ",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Крымский Вал, д 8 к 2",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Якиманка",
            "settlementCodeDistrictCode": null,
            "street": "ул Крымский Вал",
            "house": "8",
            "flat": null,
            "fias": null,
            "x": 37.607763,
            "y": 55.731901
        }
    }, {
        "id": 10188453,
        "name": "Киносмена «Зеркало Будущего» для подростков",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 23,
                "title": "Природа",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/nature.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/nature.png",
                "bgColor": "#14C626"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "20 – 26 июня 2022",
            "startDate": "2022-06-20T10:00:00+03:00",
            "endDate": "2022-06-26T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/783c0329-d257-d716-0583-47726b1537a4.jpg",
            "id": "783c0329-d257-d716-0583-47726b1537a4",
            "name": "6927f882-95f4-8ed0-389b-aff1d9c517d6.jpg",
            "size": 35171,
            "createdAt": "2022-05-31T05:05:57+00:00",
            "updatedAt": "2022-05-31T05:05:57+00:00",
            "expiredAt": "2022-06-01T05:05:57+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e37429d8-651c-f0d5-b329-c08148f11189.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/e37429d8-651c-f0d5-b329-c08148f11189.jpg",
                "id": "e37429d8-651c-f0d5-b329-c08148f11189",
                "name": "194b2d8f-aff6-12b6-f3a4-5a48b73209a4.jpg",
                "size": 7669,
                "createdAt": "2021-07-05T19:02:41+00:00",
                "updatedAt": "2021-07-05T19:02:41+00:00",
                "expiredAt": "2021-07-06T19:02:40+00:00"
            },
            "name": "Фонд развития творчества «Жизнь и Дело»",
            "statistic": {
                "organizerRating": 5
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Екатерины Будановой, д 18",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "р-н Кунцево",
            "settlementCodeDistrictCode": null,
            "street": "ул Екатерины Будановой",
            "house": "18",
            "flat": null,
            "fias": null,
            "x": 37.43322,
            "y": 55.729787
        }
    }, {
        "id": 10105792,
        "name": "Помощь в проведении творческих детских мастерских в Центре инклюзивного культурного развития Тверская 15",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 29,
                "title": "Образование",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/education.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/education.png",
                "bgColor": "#2214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 ноября 2021 – 31 декабря 2022",
            "startDate": "2021-11-11T11:30:00+03:00",
            "endDate": "2022-12-31T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3f7e44f8-9ffc-eed6-70d8-18d7813fab52.jpg",
            "id": "3f7e44f8-9ffc-eed6-70d8-18d7813fab52",
            "name": "7bb6ff1a-05c5-6033-89e8-393b7517435f.jpg",
            "size": 36211,
            "createdAt": "2021-11-11T11:33:57+00:00",
            "updatedAt": "2021-11-11T11:33:57+00:00",
            "expiredAt": "2021-11-12T11:33:57+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b682f586-d960-e934-a7fe-f01cb040374c.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b682f586-d960-e934-a7fe-f01cb040374c.jpg",
                "id": "b682f586-d960-e934-a7fe-f01cb040374c",
                "name": "0be13378-9957-03a5-eb90-3f53383385ee.jpg",
                "size": 8495,
                "createdAt": "2021-11-08T13:48:24+00:00",
                "updatedAt": "2021-11-08T13:48:24+00:00",
                "expiredAt": "2021-11-09T13:48:23+00:00"
            },
            "name": "АНО содействия творческому и культурному развитию детей и взрослых Тверская 15",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Тверская, д 15",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "ул Тверская",
            "house": "15",
            "flat": null,
            "fias": null,
            "x": 37.608311,
            "y": 55.76174
        }
    }, {
        "id": 10127113,
        "name": "Помощь в проведении творческих мастерских для людей с ментальными особенностями, живущих в психоневрологических интернатах (далее ПНИ) и семьях.",
        "categories": [{
                "id": 15,
                "title": "Здравоохранение и ЗОЖ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/health.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/health.png",
                "bgColor": "#C51232"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "18 января – 31 декабря 2022",
            "startDate": "2022-01-18T11:00:00+03:00",
            "endDate": "2022-12-31T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f1b848cb-1437-eb8e-dd1d-34455000afc5.jpg",
            "id": "f1b848cb-1437-eb8e-dd1d-34455000afc5",
            "name": "fec4e2fd-0260-ebc4-5c77-d87f78434f4d.jpg",
            "size": 11602,
            "createdAt": "2022-01-17T10:39:58+00:00",
            "updatedAt": "2022-01-17T10:39:58+00:00",
            "expiredAt": "2022-01-18T10:39:58+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b682f586-d960-e934-a7fe-f01cb040374c.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b682f586-d960-e934-a7fe-f01cb040374c.jpg",
                "id": "b682f586-d960-e934-a7fe-f01cb040374c",
                "name": "0be13378-9957-03a5-eb90-3f53383385ee.jpg",
                "size": 8495,
                "createdAt": "2021-11-08T13:48:24+00:00",
                "updatedAt": "2021-11-08T13:48:24+00:00",
                "expiredAt": "2021-11-09T13:48:23+00:00"
            },
            "name": "АНО содействия творческому и культурному развитию детей и взрослых Тверская 15",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Тверская, д 15",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "ул Тверская",
            "house": "15",
            "flat": null,
            "fias": null,
            "x": 37.608311,
            "y": 55.76174
        }
    }, {
        "id": 10184636,
        "name": "Высшая школа каратэ в Зеленограде",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 19,
                "title": "Спорт и события",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/sport.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/sport.png",
                "bgColor": "#14C65B"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }
        ],
        "eventPeriod": {
            "shortTitle": "18 – 19 июня 2022",
            "startDate": "2022-06-18T11:00:00+03:00",
            "endDate": "2022-06-19T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/4aea8dc9-1f0e-f40e-7ac2-f741e78f5c47.jpg",
            "id": "4aea8dc9-1f0e-f40e-7ac2-f741e78f5c47",
            "name": "16c3a058-a378-10b6-2114-c86447019925.jpg",
            "size": 30323,
            "createdAt": "2022-05-23T07:26:20+00:00",
            "updatedAt": "2022-05-23T07:26:20+00:00",
            "expiredAt": "2022-05-24T07:26:20+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/organization\/organization-icon.png",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/organization\/organization-icon.png",
                "id": "86c0085e-5673-4b34-b6f2-4fa9e176dccf",
                "name": "organization-icon.png",
                "size": null,
                "createdAt": "2021-06-03T07:18:06+00:00",
                "updatedAt": "2021-06-03T07:18:06+00:00",
                "expiredAt": null
            },
            "name": "ОФСО Федерация каратэномичи России",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, Зеленоград, Озёрная аллея, 10",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Зеленоград",
            "settlementCode": "7700000200000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "Озерная аллея",
            "house": "10",
            "flat": null,
            "fias": null,
            "x": 37.222610472045,
            "y": 55.987388322307
        }
    }, {
        "id": 10187654,
        "name": "Встречи с ветеранами",
        "categories": [{
                "id": 17,
                "title": "Ветераны и Историческая память",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/history.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/history.png",
                "bgColor": "#C68914"
            }, {
                "id": 21,
                "title": "Старшее поколение",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/older.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/older.png",
                "bgColor": "#78B000"
            }
        ],
        "eventPeriod": {
            "shortTitle": "1 июня – 30 ноября 2022",
            "startDate": "2022-06-01T13:00:00+03:00",
            "endDate": "2022-11-30T17:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2749e77d-75fd-275b-15db-4285a3e2640b.jpg",
            "id": "2749e77d-75fd-275b-15db-4285a3e2640b",
            "name": "495e0490-dad2-d191-3f0a-6f23cbe35503.jpg",
            "size": 28606,
            "createdAt": "2022-05-29T13:30:31+00:00",
            "updatedAt": "2022-05-29T13:30:31+00:00",
            "expiredAt": "2022-05-30T13:30:31+00:00"
        },
        "organizer": {
            "name": "Давыдкин Вячеслав Вячеславович",
            "statistic": {
                "organizerRating": null
            },
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/4702e808-ff7d-81c2-18b3-38a6d3d2d1b5.jpg",
            "iconFile": {
                "width": 140,
                "height": 140,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/4702e808-ff7d-81c2-18b3-38a6d3d2d1b5.jpg",
                "id": "4702e808-ff7d-81c2-18b3-38a6d3d2d1b5",
                "name": "81ef.jpg",
                "size": 8403,
                "createdAt": "2020-03-26T11:56:42+00:00",
                "updatedAt": "2020-03-31T04:10:27+00:00",
                "expiredAt": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10191146,
        "name": "Концерт русской академической музыки ко Дню России",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }, {
                "id": 30,
                "title": "Другое",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/others.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/others.png",
                "bgColor": "#44406F"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T18:00:00+03:00",
            "endDate": "2022-06-11T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1e08a6a4-be1b-fbd2-af0d-6dcac1be4a06.JPG",
            "id": "1e08a6a4-be1b-fbd2-af0d-6dcac1be4a06",
            "name": "c8da33ef-9f3f-e4aa-b236-0a284fbf5310.JPG",
            "size": 29351,
            "createdAt": "2022-06-06T06:38:33+00:00",
            "updatedAt": "2022-06-06T06:38:33+00:00",
            "expiredAt": "2022-06-07T06:38:33+00:00"
        },
        "organizer": {
            "name": "Токарева Валерия Александровна",
            "statistic": {
                "organizerRating": null
            },
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3cf21d83-2a2b-5cd0-234d-1524599cd3f5.png",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3cf21d83-2a2b-5cd0-234d-1524599cd3f5.png",
                "id": "3cf21d83-2a2b-5cd0-234d-1524599cd3f5",
                "name": "e5ce3604-be13-cbde-d91a-e4057bfc6cc7.png",
                "size": 106885,
                "createdAt": "2022-04-09T06:50:22+00:00",
                "updatedAt": "2022-04-09T07:02:25+00:00",
                "expiredAt": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва, улица Земляной Вал, 27с3",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": "ул Земляной Вал",
            "house": "27",
            "flat": null,
            "fias": null,
            "x": 37.658823,
            "y": 55.759147
        }
    }, {
        "id": 10168156,
        "name": "Выставка-байопик Виктор Цой.Путь героя",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "21 апреля – 21 июня 2022",
            "startDate": "2022-04-21T10:00:00+03:00",
            "endDate": "2022-06-21T22:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/68fde724-6ddf-10b1-4380-2f2c91409f2b.jpg",
            "id": "68fde724-6ddf-10b1-4380-2f2c91409f2b",
            "name": "fdb6fb3d-94b6-c274-eb73-8cc2eb8395fd.jpg",
            "size": 20830,
            "createdAt": "2022-04-20T11:37:36+00:00",
            "updatedAt": "2022-04-20T11:37:36+00:00",
            "expiredAt": "2022-04-21T11:37:36+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/organization\/organization-icon.png",
            "iconFile": {
                "width": null,
                "height": null,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/organization\/organization-icon.png",
                "id": "86c0085e-5673-4b34-b6f2-4fa9e176dccf",
                "name": "organization-icon.png",
                "size": null,
                "createdAt": "2021-06-03T07:18:06+00:00",
                "updatedAt": "2021-06-03T07:18:06+00:00",
                "expiredAt": null
            },
            "name": "Планета 9",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Манежная пл, д 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Тверской р-н",
            "settlementCodeDistrictCode": null,
            "street": "Манежная пл",
            "house": "1",
            "flat": null,
            "fias": null,
            "x": 37.612299,
            "y": 55.753433
        }
    }, {
        "id": 10125933,
        "name": "Набор волонтеров в проект #спОсобенные аниматоры",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "1 марта – 25 декабря 2022",
            "startDate": "2022-03-01T10:00:00+03:00",
            "endDate": "2022-12-25T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/adc4b280-9ff3-75ba-3e33-3f80707bf9aa.jpg",
            "id": "adc4b280-9ff3-75ba-3e33-3f80707bf9aa",
            "name": "1e22e723-8411-a344-9362-6b61108520b3.jpg",
            "size": 32840,
            "createdAt": "2022-01-12T22:13:08+00:00",
            "updatedAt": "2022-01-12T22:13:08+00:00",
            "expiredAt": "2022-01-13T22:13:08+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2d472b90-fa45-cebc-4ac3-9af1682ed503.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2d472b90-fa45-cebc-4ac3-9af1682ed503.jpg",
                "id": "2d472b90-fa45-cebc-4ac3-9af1682ed503",
                "name": "38126f9d-c276-cd22-5145-32dd22497105.jpg",
                "size": 9587,
                "createdAt": "2022-01-12T08:53:41+00:00",
                "updatedAt": "2022-01-12T08:53:41+00:00",
                "expiredAt": "2022-01-13T08:53:41+00:00"
            },
            "name": "АНО Центр интеграции Дом ходуном",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": null,
            "countryName": null,
            "title": "г Москва, ул Земляной Вал, д 27 стр 3",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Басманный р-н",
            "settlementCodeDistrictCode": null,
            "street": "ул Земляной Вал",
            "house": "27",
            "flat": null,
            "fias": null,
            "x": 37.658813,
            "y": 55.759148
        }
    }, {
        "id": 10191521,
        "name": "Концерт русской академической музыки ко Дню России",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "11 июня 2022",
            "startDate": "2022-06-11T18:00:00+03:00",
            "endDate": "2022-06-11T19:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/fcb0c98a-0e38-f2a2-ce21-3e612b1918b2.JPG",
            "id": "fcb0c98a-0e38-f2a2-ce21-3e612b1918b2",
            "name": "18dc8f0e-0145-b755-b3e4-497e6cee7333.JPG",
            "size": 31020,
            "createdAt": "2022-06-06T14:44:36+00:00",
            "updatedAt": "2022-06-06T14:44:36+00:00",
            "expiredAt": "2022-06-07T14:44:35+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6ecabcbd-91c4-488a-32c2-f309e2f215f7.jpeg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6ecabcbd-91c4-488a-32c2-f309e2f215f7.jpeg",
                "id": "6ecabcbd-91c4-488a-32c2-f309e2f215f7",
                "name": "f1e84af1-b262-6d70-662d-879e717ede3b.jpeg",
                "size": 24467,
                "createdAt": "2022-06-06T07:28:06+00:00",
                "updatedAt": "2022-06-06T07:28:06+00:00",
                "expiredAt": "2022-06-07T07:28:06+00:00"
            },
            "name": "АНО ЦВРК Сирин",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, ул Земляной Вал, д 27 стр 3",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Басманный р-н",
            "settlementCodeDistrictCode": null,
            "street": "ул Земляной Вал",
            "house": "27",
            "flat": null,
            "fias": null,
            "x": 37.658813,
            "y": 55.759148
        }
    }, {
        "id": 10190048,
        "name": "Первая Московская неделя моды",
        "categories": [{
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "20 – 26 июня 2022",
            "startDate": "2022-06-20T11:00:00+03:00",
            "endDate": "2022-06-26T20:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a116688c-d48e-4093-e3c7-a7e205d23610.png",
            "id": "a116688c-d48e-4093-e3c7-a7e205d23610",
            "name": "713593e2-b87b-759e-ecc8-d6234148212f.png",
            "size": 27271,
            "createdAt": "2022-06-02T13:05:28+00:00",
            "updatedAt": "2022-06-02T13:05:28+00:00",
            "expiredAt": "2022-06-03T13:05:28+00:00"
        },
        "organizer": {
            "name": "Сутормина Анжелика Эдуардовна",
            "statistic": {
                "organizerRating": null
            },
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/DF823E-%D0%A1%D0%90.png",
            "iconFile": {
                "width": 300,
                "height": 300,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/DF823E-%D0%A1%D0%90.png",
                "id": "05fd22cd-ecbc-56cb-254a-0188260b0016",
                "name": "DF823E-СА.png",
                "size": 10856,
                "createdAt": "2022-05-31T08:58:19+00:00",
                "updatedAt": "2022-05-31T08:58:19+00:00",
                "expiredAt": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        }
    }, {
        "id": 10188650,
        "name": "Выпускной студии мультипликации Мульт - Здоровье",
        "categories": [{
                "id": 18,
                "title": "Дети и молодежь",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/children.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/children.png",
                "bgColor": "#C6147E"
            }, {
                "id": 22,
                "title": "Люди с ОВЗ",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/disabled.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/disabled.png",
                "bgColor": "#14C69B"
            }, {
                "id": 24,
                "title": "Культура и искусство",
                "bgIcon": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.svg",
                "bgIconGray": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.svg",
                "bgIconPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/culture.png",
                "bgIconGrayPNG": "https:\/\/dobro.ru\/assets\/build\/img\/icons\/category\/gray\/culture.png",
                "bgColor": "#C214C6"
            }
        ],
        "eventPeriod": {
            "shortTitle": "15 июня 2022",
            "startDate": "2022-06-15T13:00:00+03:00",
            "endDate": "2022-06-15T15:00:00+03:00"
        },
        "imageFile": {
            "width": 350,
            "height": 272,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d874ee2c-55fb-6f01-25de-27f84373ddb0.jpg",
            "id": "d874ee2c-55fb-6f01-25de-27f84373ddb0",
            "name": "00cdfd9f-b53a-03a1-97c0-9dc279ae909b.jpg",
            "size": 53762,
            "createdAt": "2022-05-31T09:06:16+00:00",
            "updatedAt": "2022-05-31T09:06:16+00:00",
            "expiredAt": "2022-06-01T09:06:16+00:00"
        },
        "organizer": {
            "iconUrl": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1ae7e5b1-9b28-cc6a-85c0-b627425a275e.jpg",
            "iconFile": {
                "width": 250,
                "height": 250,
                "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1ae7e5b1-9b28-cc6a-85c0-b627425a275e.jpg",
                "id": "1ae7e5b1-9b28-cc6a-85c0-b627425a275e",
                "name": "9bb461cd-88d6-9a54-7cd0-0491ffce643a.jpg",
                "size": 10489,
                "createdAt": "2022-04-27T09:39:39+00:00",
                "updatedAt": "2022-04-27T09:39:39+00:00",
                "expiredAt": "2022-04-28T09:39:39+00:00"
            },
            "name": "БФ ПЗ «Здоровье»",
            "statistic": {
                "organizerRating": null
            }
        },
        "location": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва, 4-й Вешняковский проезд, д 1 к 1",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": "Рязанский р-н",
            "settlementCodeDistrictCode": null,
            "street": "4-й Вешняковский проезд",
            "house": "1",
            "flat": null,
            "fias": null,
            "x": 37.793247,
            "y": 55.718304
        }
    }
]
`
