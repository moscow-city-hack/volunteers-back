package data

import "time"

type Address struct {
	District string  `json:"district"`
	Street   string  `json:"street"`
	House    string  `json:"house"`
	Metro    string  `json:"metro"`
	X        float64 `json:"x"`
	Y        float64 `json:"y"`
}

type Checkbox struct {
	ID    string `json:"id"`
	Label string `json:"label"`
	Value bool   `json:"value"`
}

type DisplayName struct {
	ID          string `json:"id"`
	DisplayName string `json:"displayname"`
}

type Place struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}

type Volunteer struct {
	Id         int           `json:"id"`
	Name       string        `json:"name"`
	Surname    string        `json:"surname"`
	BirthDate  string        `json:"birthdate"`
	Gender     []Checkbox    `json:"gender"`
	Phone      string        `json:"phone"`
	EMail      string        `json:"email"`
	Address    Address       `json:"address"`
	Format     string        `json:"format"`
	Skills     []DisplayName `json:"skills"`
	Categories []DisplayName `json:"categories"`
	Work       string        `json:"work"`
	Place      Place         `json:"place"`
	Image      string        `json:"image"`
	Rating     float32       `json:"rating"`
	Reviews    int           `json:"reviews"`
}

type Organization struct {
	Id         int           `json:"id"`
	Type       string        `json:"type"`
	Name       string        `json:"name"`
	INN        string        `json:"inn"`
	Phone      string        `json:"phone"`
	EMail      string        `json:"email"`
	Address    Address       `json:"address"`
	Skills     []DisplayName `json:"skills"`
	Categories []DisplayName `json:"categories"`
	Place      Place         `json:"place"`
	Image      string        `json:"image"`
	Rating     float32       `json:"rating"`
	Reviews    int           `json:"reviews"`
}

type Project struct {
	Id        int          `json:"id"`
	Organizer Organization `json:"organizer"`
	Name      string       `json:"name"`
}

type Period struct {
	From string `json:"from"`
	To   string `json:"to"`
}

type AgeRange struct {
	From int `json:"from"`
	To   int `json:"to"`
}

type Event struct {
	Id         int           `json:"id"`
	Name       string        `json:"text"`
	Location   string        `json:"location"`
	Image      string        `json:"image"`
	Favorite   bool          `json:"favorite"`
	Categories []DisplayName `json:"categories"`
	Conditions []DisplayName `json:"conditions"`
	Skills     []DisplayName `json:"skills"`
	Roles      []DisplayName `json:"roles"`
	Motivation []DisplayName `json:"motivation"`
	Work       string        `json:"work"`
	Format     string        `json:"format"`
	Date       Period        `json:"date"`
	Address    Address       `json:"address"`
	Place      Place         `json:"place"`
	TimeStart  string        `json:"timestart"`
	TimeEnd    string        `json:"timeend"`
	Urgently   bool          `json:"urgently"`
}

type RegisterInfo struct {
	Name           string     `json:"name"`
	Surname        string     `json:"surname"`
	BirthDate      time.Time  `json:"birthDate"`
	Gender         []Checkbox `json:"gender"`
	Phone          string     `json:"phone"`
	EMail          string     `json:"email"`
	Password       string     `json:"password"`
	RepeatPassword string     `json:"repeatPassword"`
}
