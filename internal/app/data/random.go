package data

import "math/rand"

func RandomDisplayNames(arr []DisplayName) []DisplayName {
	size := rand.Int() % len(arr)
	result := make([]DisplayName, size)

	for i := 0; i < size; i++ {
		result[i] = arr[rand.Int()%len(arr)]
	}

	return result
}
