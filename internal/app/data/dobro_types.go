package data

type DobroFIO struct {
	FirstName  string `json:"first_name"`
	SecondName string `json:"second_name"`
	LastName   string `json:"last_name"`
}

type DobroSettlement struct {
	Country                    string  `json:"country"`
	CountryName                string  `json:"countryName"`
	Title                      string  `json:"title"`
	Region                     int     `json:"region"`
	Municipality               string  `json:"municipality"`
	MunicipalityCode           string  `json:"municipalityCode"`
	Settlement                 string  `json:"settlement"`
	SettlementCode             string  `json:"settlementCode"`
	SettlementDistrict         string  `json:"settlementDistrict"`
	SettlementCodeDistrictCode string  `json:"settlementCodeDistrictCode"`
	Street                     string  `json:"street"`
	House                      string  `json:"house"`
	Flat                       string  `json:"flat"`
	Fias                       string  `json:"fias"`
	X                          float64 `json:"x"`
	Y                          float64 `json:"y"`
}

type DobroIconFile struct {
	Width     int    `json:"width"`
	Height    int    `json:"height"`
	Url       string `json:"url"`
	Id        string `json:"id"`
	Name      string `json:"name"`
	Size      int    `json:"size"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
	ExpiredAt string `json:"expiredAt"`
}

type VolunteerStatistic struct {
	EventsCount     int     `json:"eventsCount"`
	VolunteerRating float64 `json:"volunteerRating"`
	OrganizerRating float64 `json:"organizerRating"`
}

type DobroVolunteer struct {
	Id         int                `json:"id"`
	Fio        DobroFIO           `json:"fio"`
	Gender     string             `json:"gender"`
	Settlement DobroSettlement    `json:"settlement"`
	IconFile   DobroIconFile      `json:"iconFile"`
	Statistic  VolunteerStatistic `json:"statistic"`
	CanContact bool               `json:"canContact"`
}

type OrganizationStatistic struct {
	ProjectsCount                int     `json:"projectsCount"`
	VolunteersCount              int     `json:"volunteersCount"`
	OrganizerRatingCount         int     `json:"organizerRatingCount"`
	AssistantsCount              int     `json:"assistantsCount"`
	VacanciesCount               int     `json:"vacanciesCount"`
	AcceptedVacancyRequestsCount int     `json:"acceptedVacancyRequestsCount"`
	VolunteersWithHoursCount     int     `json:"volunteersWithHoursCount"`
	EventsCount                  int     `json:"eventsCount"`
	OrganizerRating              float64 `json:"organizerRating"`
	Hours                        float64 `json:"hours"`
}

type DobroOrganization struct {
	IconUrl              string                `json:"iconUrl"`
	IconFile             DobroIconFile         `json:"iconFile"`
	Description          string                `json:"description"`
	INN                  string                `json:"inn"`
	OGRN                 string                `json:"ogrn"`
	Type                 string                `json:"type"`
	FullName             string                `json:"fullName"`
	VolunteersCanAssists bool                  `json:"volunteersCanAssists"`
	AlreadyAssist        bool                  `json:"alreadyAssist"`
	Id                   int                   `json:"id"`
	Status               int                   `json:"status"`
	Name                 string                `json:"name"`
	Statistic            OrganizationStatistic `json:"statistic"`
	RemoteId             int                   `json:"remoteId"`
	Verified             bool                  `json:"verified"`
	URL                  string                `json:"url"`
	CanContact           bool                  `json:"canContact"`
	AssistantType        string                `json:"assistantType"`
}

type DobroProject struct {
	Id        int           `json:"id"`
	Organizer Organization  `json:"organizer"`
	Name      string        `json:"name"`
	ImageFile DobroIconFile `json:"imageFile"`
}

type DobroCategory struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Icon        string `json:"bgIcon"`
	IconGray    string `json:"bgIconGray"`
	IconPNG     string `json:"bgIconPNG"`
	IconGrayPNG string `json:"bgIconGrayPNG"`
	Color       string `json:"bgColor"`
}

type DobroPeriod struct {
	ShortTitle string `json:"shortTitle"`
	StartDate  string `json:"startDate"`
	EndDate    string `json:"endDate"`
}

type DobroEvent struct {
	Id          int             `json:"id"`
	Name        string          `json:"name"`
	Categories  []DobroCategory `json:"categories"`
	EventPeriod Period          `json:"eventPeriod"`
	ImageFile   DobroIconFile   `json:"imageFile"`
	Organizer   Organization    `json:"organizer"`
	Location    DobroSettlement `json:"location"`
}
