package data

var VDS = `
[{
        "id": 164235,
        "fio": {
            "first_name": "Владислав",
            "second_name": "Адрианович",
            "last_name": "Мунтяну"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a256570d-1ba2-322d-7438-472fd75b80a3.jpg",
            "id": "a256570d-1ba2-322d-7438-472fd75b80a3",
            "name": "ce4a1328-4959-7a3a-0f75-a72ba2a01c6c.jpg",
            "size": 18800,
            "createdAt": "2022-05-28T21:47:46+00:00",
            "updatedAt": "2022-05-28T21:47:50+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 277,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1376550,
        "fio": {
            "first_name": "Александра",
            "second_name": "Маликовна",
            "last_name": "Исхакова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.754047
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/26a5f693-2544-71b9-a6ad-aecfebd582ba.jpg",
            "id": "26a5f693-2544-71b9-a6ad-aecfebd582ba",
            "name": "8dec3472-75ab-a21e-c475-d6e27c33e7a3.jpg",
            "size": 4542,
            "createdAt": "2020-05-01T19:53:51+00:00",
            "updatedAt": "2020-05-01T20:07:41+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 64,
            "volunteerRating": 5,
            "organizerRating": 4.9
        },
        "canContact": true
    }, {
        "id": 241500,
        "fio": {
            "first_name": "Анна",
            "second_name": "Викторовна",
            "last_name": "Самсоненкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b5e1653b-a9cd-92c6-8343-ab600280b4b8.jpg",
            "id": "b5e1653b-a9cd-92c6-8343-ab600280b4b8",
            "name": "6b767353-bac4-fff9-421e-f5c85f374fd9.jpg",
            "size": 10417,
            "createdAt": "2021-08-20T05:06:08+00:00",
            "updatedAt": "2022-01-03T21:15:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 94,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92135284,
        "fio": {
            "first_name": "Наталия",
            "second_name": "Вячеславовна",
            "last_name": "Бухвалова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5ed29e5a-e4a8-d4ef-c3eb-2e765d098b86.jpg",
            "id": "5ed29e5a-e4a8-d4ef-c3eb-2e765d098b86",
            "name": "986d8606-d6d4-6fd7-492c-7de9a3c28fe7.jpg",
            "size": 19794,
            "createdAt": "2021-04-30T21:10:08+00:00",
            "updatedAt": "2021-04-30T21:15:24+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 39,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92135324,
        "fio": {
            "first_name": "Виктория",
            "second_name": "Андреевна",
            "last_name": "Бухвалова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1527c7c7-22fc-5d42-b6d3-daedf3b679ad.jpg",
            "id": "1527c7c7-22fc-5d42-b6d3-daedf3b679ad",
            "name": "b8f2eaba-0696-e86a-7fea-39e9cbd7d3ca.jpg",
            "size": 5946,
            "createdAt": "2021-04-06T22:03:56+00:00",
            "updatedAt": "2021-04-06T22:04:59+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 35,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 550007,
        "fio": {
            "first_name": "Владимир",
            "second_name": "Борисович",
            "last_name": "Никольский"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8f1b4d94-ac8c-7b69-3ff2-f07c82228bb1.jpg",
            "id": "8f1b4d94-ac8c-7b69-3ff2-f07c82228bb1",
            "name": "8c8fd18a-457d-ba67-17d3-7dace11283a0.jpg",
            "size": 14839,
            "createdAt": "2022-01-24T18:56:01+00:00",
            "updatedAt": "2022-01-24T18:59:26+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 268320,
        "fio": {
            "first_name": "Людмила",
            "second_name": "Игоревна",
            "last_name": "Горелова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/4d8767d3-634d-a369-a408-4e2d06d01f0f.JPG",
            "id": "4d8767d3-634d-a369-a408-4e2d06d01f0f",
            "name": "4626b503-809e-89ac-fd12-4c72b62a0c71.JPG",
            "size": 6417,
            "createdAt": "2020-05-17T12:05:36+00:00",
            "updatedAt": "2020-05-17T12:05:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 44,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 18763,
        "fio": {
            "first_name": "Наталья",
            "second_name": "Григорьевна",
            "last_name": "Федорова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "город Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/6b0a31a9ba3b457baab41ec485c9c0f2.jpgava1",
            "id": "6dd37896-a922-6c6f-6b5c-2f9c65d7398f",
            "name": "6b0a31a9ba3b457baab41ec485c9c0f2.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:02:55+00:00",
            "updatedAt": "2020-10-29T08:47:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 106,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92348051,
        "fio": {
            "first_name": "Александр",
            "second_name": null,
            "last_name": "Баринов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/cc580adf-9cdc-b7b1-3836-7d23d0b22cb3.jpeg",
            "id": "cc580adf-9cdc-b7b1-3836-7d23d0b22cb3",
            "name": "b617e9cd-14b5-cd03-b99c-f33fda24f0fa.jpeg",
            "size": 12579,
            "createdAt": "2021-07-13T09:55:28+00:00",
            "updatedAt": "2021-07-13T09:55:28+00:00",
            "expiredAt": "2021-07-14T09:55:28+00:00"
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92348087,
        "fio": {
            "first_name": "Сергей",
            "second_name": "Ильич",
            "last_name": "Путря"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/9BFA1B-%D0%9F%D0%A1.png",
            "id": "af98dc2f-cba9-b6c1-dc5f-5b14ace91259",
            "name": "9BFA1B-ПС.png",
            "size": 8260,
            "createdAt": "2021-07-13T09:43:31+00:00",
            "updatedAt": "2021-07-13T09:43:31+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91743489,
        "fio": {
            "first_name": "Регина",
            "second_name": "Юрьевна",
            "last_name": "Киреева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2ec8dc02-ac95-890c-7098-f5f7c11c34b2.jpeg",
            "id": "2ec8dc02-ac95-890c-7098-f5f7c11c34b2",
            "name": "8cefa809-5abb-cbe5-1056-bb98d4202e5b.jpeg",
            "size": 15399,
            "createdAt": "2022-02-21T15:35:41+00:00",
            "updatedAt": "2022-02-21T15:35:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1259327,
        "fio": {
            "first_name": "Любовь",
            "second_name": "Владимировна",
            "last_name": "Ефремова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/14ea821e-03bd-59bb-99d1-f16b7cd52cb0.jpg",
            "id": "14ea821e-03bd-59bb-99d1-f16b7cd52cb0",
            "name": "6d6f4d87-e2ae-db56-b5d2-e3db4119a456.jpg",
            "size": 6582,
            "createdAt": "2021-03-12T07:32:41+00:00",
            "updatedAt": "2021-03-12T07:32:52+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 72,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1378189,
        "fio": {
            "first_name": "Елена",
            "second_name": "Валерьевна",
            "last_name": "Гнилитская"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 117,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 276402,
        "fio": {
            "first_name": "Антонина",
            "second_name": "Ивановна",
            "last_name": "Иванкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 66,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 493120,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Андреевна",
            "last_name": "Брайловская"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/67698914-6f8f-3de5-112f-3477fb60a9d5.jpg",
            "id": "67698914-6f8f-3de5-112f-3477fb60a9d5",
            "name": "3e357451-5d9f-6b60-4012-72e93432165b.jpg",
            "size": 6452,
            "createdAt": "2021-02-28T12:25:14+00:00",
            "updatedAt": "2021-02-28T12:30:25+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91581763,
        "fio": {
            "first_name": "Нина",
            "second_name": "Анатольевна",
            "last_name": "Кучинская"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a35ea402-4301-9e74-b486-657d980961ea.jpg",
            "id": "a35ea402-4301-9e74-b486-657d980961ea",
            "name": "b32be771-7a1a-d7fc-7646-e8989ffc1dfa.jpg",
            "size": 11973,
            "createdAt": "2022-01-10T00:40:51+00:00",
            "updatedAt": "2022-01-10T01:09:24+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 42,
            "volunteerRating": 4.9,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 528226,
        "fio": {
            "first_name": "Дмитрий",
            "second_name": "Алексеевич",
            "last_name": "Баженов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9b035a16-5295-b82d-f4ee-40a59348f128.webp",
            "id": "9b035a16-5295-b82d-f4ee-40a59348f128",
            "name": "c8deb4f9-9012-26fa-a6c2-a9ae9bdf12c3.webp",
            "size": 4142,
            "createdAt": "2021-01-20T20:08:29+00:00",
            "updatedAt": "2021-01-20T20:08:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91704118,
        "fio": {
            "first_name": "Мария",
            "second_name": "Ивановна",
            "last_name": "Чернышева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/43664957-4bf4-37f8-e7ac-b1a841faa26c.jpg",
            "id": "43664957-4bf4-37f8-e7ac-b1a841faa26c",
            "name": "984d7adf-33f5-cd1d-f2b8-e94e2bd96c0a.jpg",
            "size": 26268,
            "createdAt": "2021-05-11T18:24:53+00:00",
            "updatedAt": "2021-05-11T18:25:29+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 10,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 10571,
        "fio": {
            "first_name": "ЕЛЕНА",
            "second_name": "НИКОЛАЕВНА",
            "last_name": "КРАВЧЕНКО"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/c190606229cf4ece8bdadfba155b8c3a.jpg",
            "id": "f2db9502-1d1b-4eee-749f-7170e5fe560e",
            "name": "c190606229cf4ece8bdadfba155b8c3a.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:01:59+00:00",
            "updatedAt": "2020-12-11T00:24:36+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 57,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 227137,
        "fio": {
            "first_name": "Римма",
            "second_name": "Константиновна",
            "last_name": "Некрасова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/e31556e9966740f4a20d7f5ef3a94dd1.jpgava1",
            "id": "0d4245c1-ad85-91ce-331d-dd780ef06060",
            "name": "e31556e9966740f4a20d7f5ef3a94dd1.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:26:51+00:00",
            "updatedAt": "2020-10-19T04:28:01+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 52,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1375194,
        "fio": {
            "first_name": "Лариса",
            "second_name": "Ивановна",
            "last_name": "Лебедева"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8b95274b-683a-faa9-808e-05eca8c4de59.png",
            "id": "8b95274b-683a-faa9-808e-05eca8c4de59",
            "name": "e930.png",
            "size": 38894,
            "createdAt": "2020-03-12T13:39:20+00:00",
            "updatedAt": "2020-03-31T04:24:37+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 39,
            "volunteerRating": 5,
            "organizerRating": 4.8
        },
        "canContact": true
    }, {
        "id": 92683655,
        "fio": {
            "first_name": "Лариса",
            "second_name": "Сергеевна",
            "last_name": "Масленкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ad65816e-7007-3c2c-f165-c264add5c60a.png",
            "id": "ad65816e-7007-3c2c-f165-c264add5c60a",
            "name": "82925c90-a7cf-448c-2182-570ba537319b.png",
            "size": 261332,
            "createdAt": "2021-10-31T11:54:35+00:00",
            "updatedAt": "2021-10-31T11:58:02+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92347335,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Александровна",
            "last_name": "Бояринова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6492c9e1-8a7e-e5f0-867a-f009f44a1793.png",
            "id": "6492c9e1-8a7e-e5f0-867a-f009f44a1793",
            "name": "9c18bb32-fd16-6ca2-2ce7-802efde6bc68.png",
            "size": 385041,
            "createdAt": "2021-07-13T07:47:21+00:00",
            "updatedAt": "2021-07-13T07:47:27+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92269823,
        "fio": {
            "first_name": "Кирилл",
            "second_name": "Владимирович",
            "last_name": "Кононаев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/fc8ca8b7-3e7c-1e84-afda-d5e0fefceed2.jpg",
            "id": "fc8ca8b7-3e7c-1e84-afda-d5e0fefceed2",
            "name": "a622c078-2550-1c91-dd17-96521a167911.jpg",
            "size": 17618,
            "createdAt": "2022-04-29T08:14:53+00:00",
            "updatedAt": "2022-04-29T08:15:27+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 38,
            "volunteerRating": 5,
            "organizerRating": 4.9
        },
        "canContact": true
    }, {
        "id": 56139,
        "fio": {
            "first_name": "Юлия",
            "second_name": "Аркадьевна",
            "last_name": "Симонова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 89,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91639754,
        "fio": {
            "first_name": "Платон",
            "second_name": "Александрович",
            "last_name": "Докин"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/074a94e4-06ea-566c-4bbb-8df6b264b76b.jpeg",
            "id": "074a94e4-06ea-566c-4bbb-8df6b264b76b",
            "name": "f75885f2-55be-365a-e036-6544fc6d932d.jpeg",
            "size": 5743,
            "createdAt": "2020-12-24T07:16:41+00:00",
            "updatedAt": "2020-12-24T07:17:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 32,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": false
    }, {
        "id": 198554,
        "fio": {
            "first_name": "Любовь",
            "second_name": "Александровна",
            "last_name": "Коробейникова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/ef69d4c2e3f94fe99e672d4ae3e08966.jpg",
            "id": "fe9296ea-0d33-1470-ba8e-1c1443eb02ca",
            "name": "ef69d4c2e3f94fe99e672d4ae3e08966.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:27:58+00:00",
            "updatedAt": "2020-12-11T01:55:27+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 71,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1510479,
        "fio": {
            "first_name": "Мария",
            "second_name": "Георгиевна",
            "last_name": "Ануфриева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 643469,
        "fio": {
            "first_name": "Александра",
            "second_name": "Вячеславовна",
            "last_name": "Оберная"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/11610f98c80841ebafaa73ec8a9015a9.jpg",
            "id": "de21c7bf-5bbf-0bf1-8770-b5153e068774",
            "name": "11610f98c80841ebafaa73ec8a9015a9.jpg",
            "size": null,
            "createdAt": "2020-02-29T03:08:54+00:00",
            "updatedAt": "2020-12-03T09:56:59+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 884736,
        "fio": {
            "first_name": "Елена",
            "second_name": "Юрьевна",
            "last_name": "Дудина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/bf70c7503de74b75b767f82d6aa600de.jpg",
            "id": "8f4b97d5-8262-3420-33f7-323e734141ba",
            "name": "bf70c7503de74b75b767f82d6aa600de.jpg",
            "size": null,
            "createdAt": "2020-02-29T03:33:34+00:00",
            "updatedAt": "2020-10-29T19:58:15+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 48,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92499648,
        "fio": {
            "first_name": "Гульшара",
            "second_name": "Кенесовна",
            "last_name": "Дабаева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/40A6CE-%D0%94%D0%93.png",
            "id": "33232628-a2f7-6080-3209-1a5fc488decd",
            "name": "40A6CE-ДГ.png",
            "size": 4285,
            "createdAt": "2021-09-29T05:17:08+00:00",
            "updatedAt": "2021-09-29T05:17:08+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 75,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92257031,
        "fio": {
            "first_name": "Людмила",
            "second_name": "Михайловна",
            "last_name": "Панова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c068b8e8-a56d-1dde-f786-1ecb564a7a67.jpg",
            "id": "c068b8e8-a56d-1dde-f786-1ecb564a7a67",
            "name": "3d8d7128-90d0-7c22-f6b5-4385a48bd113.jpg",
            "size": 11650,
            "createdAt": "2021-05-03T07:58:33+00:00",
            "updatedAt": "2021-05-03T07:58:42+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 53,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92268224,
        "fio": {
            "first_name": "ГЕОРГИЙ",
            "second_name": "СЕРГЕЕВИЧ",
            "last_name": "ОВОД"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ab0ba9ad-5e19-9a48-9010-c9450684aa79.jpeg",
            "id": "ab0ba9ad-5e19-9a48-9010-c9450684aa79",
            "name": "302c004c-e082-574e-a59c-58c43fae69f8.jpeg",
            "size": 14141,
            "createdAt": "2021-07-20T20:23:13+00:00",
            "updatedAt": "2021-07-20T20:24:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 59,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 484419,
        "fio": {
            "first_name": "Татьяна",
            "second_name": "Ивановна",
            "last_name": "Зарецкая"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8ad6ec93-c98d-4d63-1678-1a43b48a9cbc.jpg",
            "id": "8ad6ec93-c98d-4d63-1678-1a43b48a9cbc",
            "name": "da0d50d5-5f11-fffb-08dd-345855d505a3.jpg",
            "size": 6534,
            "createdAt": "2020-12-29T10:08:45+00:00",
            "updatedAt": "2020-12-29T10:09:13+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 56,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1302567,
        "fio": {
            "first_name": "Кристина",
            "second_name": "Геннадьевна",
            "last_name": "Буркова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/afb1a0e9-39ba-5145-bee4-0fa1dab4779e.jpeg",
            "id": "afb1a0e9-39ba-5145-bee4-0fa1dab4779e",
            "name": "9fad7ef1-be6b-35b8-a776-180565d59b9f.jpeg",
            "size": 16289,
            "createdAt": "2022-05-13T16:38:47+00:00",
            "updatedAt": "2022-05-13T16:38:54+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 34,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1502329,
        "fio": {
            "first_name": "Надежда",
            "second_name": "Владимировна",
            "last_name": "Иванова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91732320,
        "fio": {
            "first_name": "Динара",
            "second_name": "Надировна",
            "last_name": "Шарибова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/ab01d06e-093f-aa34-d4fd-05770b693de3.jpg",
            "id": "ab01d06e-093f-aa34-d4fd-05770b693de3",
            "name": "6eae65e4-0dcb-7b48-ec93-cae0c790b9d1.jpg",
            "size": 7015,
            "createdAt": "2020-09-02T18:00:20+00:00",
            "updatedAt": "2020-09-02T18:01:12+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 13,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91971233,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Валерьевна",
            "last_name": "Гришина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5616cc18-c8eb-5ec3-9271-470a8e7c8e7c.jpg",
            "id": "5616cc18-c8eb-5ec3-9271-470a8e7c8e7c",
            "name": "7edc79f3-d6bb-cd6a-dc15-8d3c9073e645.jpg",
            "size": 14334,
            "createdAt": "2021-07-22T19:03:21+00:00",
            "updatedAt": "2021-07-22T19:04:01+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 16,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 817541,
        "fio": {
            "first_name": "Даниель",
            "second_name": "Миргасимович",
            "last_name": "Салахов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3350d0af-9554-1666-b552-141bf7abda89.jpg",
            "id": "3350d0af-9554-1666-b552-141bf7abda89",
            "name": "aafae247-7b0f-bf86-18bd-1a48a7626bc1.jpg",
            "size": 19949,
            "createdAt": "2021-05-09T08:50:03+00:00",
            "updatedAt": "2021-05-09T09:11:23+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 40,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1366038,
        "fio": {
            "first_name": "Любовь",
            "second_name": "Валентиновна",
            "last_name": "Лякина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Россия, Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 42,
            "volunteerRating": 5,
            "organizerRating": 4.9
        },
        "canContact": true
    }, {
        "id": 91566818,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Андреевна",
            "last_name": "Минаева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91695657,
        "fio": {
            "first_name": "Изабелла",
            "second_name": "Игоревна",
            "last_name": "Коняхина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/482bc454-bc52-436a-6db1-afb23d240188.jpg",
            "id": "482bc454-bc52-436a-6db1-afb23d240188",
            "name": "6d2287ba-6e67-7f08-bdf4-ecaf283da17a.jpg",
            "size": 6806,
            "createdAt": "2020-06-30T20:08:19+00:00",
            "updatedAt": "2020-06-30T20:21:53+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 53,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91685125,
        "fio": {
            "first_name": "Анна",
            "second_name": "Олеговна",
            "last_name": "Егорычева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6f054711-e1cf-8fbf-af02-bce9cd20fb8b.jpg",
            "id": "6f054711-e1cf-8fbf-af02-bce9cd20fb8b",
            "name": "130b2b17-9e12-b938-30a8-7df524bb6be3.jpg",
            "size": 21677,
            "createdAt": "2021-11-13T08:23:33+00:00",
            "updatedAt": "2021-11-13T08:23:33+00:00",
            "expiredAt": "2021-11-14T08:23:33+00:00"
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92891742,
        "fio": {
            "first_name": "Вероника",
            "second_name": "Олеговна",
            "last_name": "Артемова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/10f464d4-82c1-a58f-c85c-30aa1a4ed080.jpg",
            "id": "10f464d4-82c1-a58f-c85c-30aa1a4ed080",
            "name": "21aea4cc-6a37-4dbc-688d-338467e67007.jpg",
            "size": 11854,
            "createdAt": "2022-04-05T21:22:32+00:00",
            "updatedAt": "2022-04-05T21:24:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 39,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91888764,
        "fio": {
            "first_name": "Мария",
            "second_name": "Алексеевна",
            "last_name": "Телегина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/eddc3372-1f28-3e1b-7236-8673882dcc25.jpg",
            "id": "eddc3372-1f28-3e1b-7236-8673882dcc25",
            "name": "9b659b0c-f4f6-8b93-f0b8-e381ca3629cf.jpg",
            "size": 8307,
            "createdAt": "2020-12-14T07:00:40+00:00",
            "updatedAt": "2020-12-14T07:01:22+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91625656,
        "fio": {
            "first_name": "Анна",
            "second_name": "Евгеньевна",
            "last_name": "Егорова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/202daadb-66c9-9a0b-ec33-6ddb590918d0.jpg",
            "id": "202daadb-66c9-9a0b-ec33-6ddb590918d0",
            "name": "ded7e50e-1d90-0264-a82a-56466ae03e0a.jpg",
            "size": 14622,
            "createdAt": "2021-11-23T02:42:17+00:00",
            "updatedAt": "2021-11-23T02:49:56+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 43,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91655983,
        "fio": {
            "first_name": "Ильяс",
            "second_name": "Гарунович",
            "last_name": "Аминов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92773649,
        "fio": {
            "first_name": "Мария",
            "second_name": "Сергеевна",
            "last_name": "Евтихова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b68bab02-cb41-0169-7655-fd707d84deeb.jpeg",
            "id": "b68bab02-cb41-0169-7655-fd707d84deeb",
            "name": "result___0_6162011015144435256.jpeg",
            "size": 86219,
            "createdAt": "2022-03-21T20:07:58+00:00",
            "updatedAt": "2022-04-06T17:53:54+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 36,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 39780,
        "fio": {
            "first_name": "Ружена",
            "second_name": "Борисовна",
            "last_name": "Пугачева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/a2587052bd3f4ef8ab89880c6f4a3628.jpg",
            "id": "97d8c880-9e74-dd92-19ec-e4144719a128",
            "name": "a2587052bd3f4ef8ab89880c6f4a3628.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:05:42+00:00",
            "updatedAt": "2020-10-29T22:31:06+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 26,
            "volunteerRating": 5,
            "organizerRating": 4.8
        },
        "canContact": false
    }, {
        "id": 959936,
        "fio": {
            "first_name": "Яна",
            "second_name": "Миннахметовна",
            "last_name": "Валиева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6dc43a72-ee26-b8d9-ffc4-f0380061d6a0.jpeg",
            "id": "6dc43a72-ee26-b8d9-ffc4-f0380061d6a0",
            "name": "5097d2a5-f9b6-e2c8-f4de-f2b63290a1ea.jpeg",
            "size": 14867,
            "createdAt": "2022-02-18T12:12:06+00:00",
            "updatedAt": "2022-02-18T12:12:06+00:00",
            "expiredAt": "2022-02-19T12:12:06+00:00"
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91719718,
        "fio": {
            "first_name": "Наталья",
            "second_name": "Владиславовна",
            "last_name": "Лысова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9342fc57-af6f-206e-94ca-e70f241bb32f.jpg",
            "id": "9342fc57-af6f-206e-94ca-e70f241bb32f",
            "name": "f591ffaa-9ee6-fdef-9b46-8fdf21d81a89.jpg",
            "size": 19590,
            "createdAt": "2021-09-01T08:48:21+00:00",
            "updatedAt": "2021-09-01T08:48:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 33,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 86598,
        "fio": {
            "first_name": "Ольга",
            "second_name": "Васильевна",
            "last_name": "Сорина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b1196c4d-01af-fc79-4ccb-8c56e09a997a.jpg",
            "id": "b1196c4d-01af-fc79-4ccb-8c56e09a997a",
            "name": "b462b71a-4df4-8825-41ce-79e8db85f529.jpg",
            "size": 13712,
            "createdAt": "2022-05-11T14:51:43+00:00",
            "updatedAt": "2022-05-11T14:52:51+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 65,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 262155,
        "fio": {
            "first_name": "Мария",
            "second_name": "Валерьевна",
            "last_name": "Дрозд"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/0bffb344d59c4bf5b210f8feb0397c45.jpg",
            "id": "1b4e2d11-bd03-cbdb-6afc-edb2b480dade",
            "name": "0bffb344d59c4bf5b210f8feb0397c45.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:16:51+00:00",
            "updatedAt": "2020-10-19T06:27:21+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91768915,
        "fio": {
            "first_name": "Полина",
            "second_name": "Павловна",
            "last_name": "Трофимова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 17,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92349422,
        "fio": {
            "first_name": "Полина",
            "second_name": "Владиславовна",
            "last_name": "Менделеева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/549E52-%D0%9C%D0%9F.png",
            "id": "d0e0dda3-f09e-deac-abd9-cdaeee67a252",
            "name": "549E52-МП.png",
            "size": 8605,
            "createdAt": "2021-07-14T11:01:47+00:00",
            "updatedAt": "2021-07-14T11:01:47+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 9,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92592271,
        "fio": {
            "first_name": "Вера",
            "second_name": "Ивановна",
            "last_name": "Исаева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f9ce9e8b-e2ae-a244-f480-c123beb92a92.jpg",
            "id": "f9ce9e8b-e2ae-a244-f480-c123beb92a92",
            "name": "1af4fd9d-2d42-6a28-e252-deed2ee6ee26.jpg",
            "size": 21850,
            "createdAt": "2021-12-05T21:01:07+00:00",
            "updatedAt": "2021-12-05T21:01:16+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 35,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 269216,
        "fio": {
            "first_name": "Илья",
            "second_name": "Андреевич",
            "last_name": "Щукин"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/16b42811-ca4b-1058-478e-e5e51c21a7be.jpeg",
            "id": "16b42811-ca4b-1058-478e-e5e51c21a7be",
            "name": "b7306d02-ee34-6063-451c-c34e464f7ad8.jpeg",
            "size": 14016,
            "createdAt": "2022-02-24T19:39:47+00:00",
            "updatedAt": "2022-02-24T19:39:57+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1491056,
        "fio": {
            "first_name": "Игорь",
            "second_name": "Вячеславович",
            "last_name": "Лапин"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": null,
            "y": null
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 683514,
        "fio": {
            "first_name": "Александра",
            "second_name": "Владимировна",
            "last_name": "Рогощенкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c34ad018-6756-1c69-580b-9aabde262db0.jpg",
            "id": "c34ad018-6756-1c69-580b-9aabde262db0",
            "name": "7dfb8d1b-7135-c3f8-1ebd-b21bc381d370.jpg",
            "size": 7919,
            "createdAt": "2020-04-05T10:45:23+00:00",
            "updatedAt": "2020-04-05T10:46:21+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 26,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93152340,
        "fio": {
            "first_name": "Дмитрий",
            "second_name": "Анатольевич",
            "last_name": "Машков"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/C33F48-%D0%9C%D0%94.png",
            "id": "07fbaee6-f39b-f15a-236c-63cbd8d03289",
            "name": "C33F48-МД.png",
            "size": 9720,
            "createdAt": "2022-03-29T05:46:19+00:00",
            "updatedAt": "2022-03-29T05:46:19+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 222975,
        "fio": {
            "first_name": "Лидия",
            "second_name": "Михайловна",
            "last_name": "Леонтьева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "МОСКВА",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "МОСКВА",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/1a9f8d2291e24dd5bfa95b7da4c1fb0c.jpg",
            "id": "2eb08d94-f9c5-e903-0800-2e6a7bc36a40",
            "name": "1a9f8d2291e24dd5bfa95b7da4c1fb0c.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:17:37+00:00",
            "updatedAt": "2020-10-20T02:32:27+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 32,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92347331,
        "fio": {
            "first_name": "Денис",
            "second_name": "Владимирович",
            "last_name": "Решмет"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/049698f5-4e58-c540-a755-86e1748d3d3f.jpeg",
            "id": "049698f5-4e58-c540-a755-86e1748d3d3f",
            "name": "cca8159d-2c2f-c75f-8971-70a6575f2cf1.jpeg",
            "size": 20350,
            "createdAt": "2021-07-13T09:14:30+00:00",
            "updatedAt": "2021-07-13T09:15:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92351891,
        "fio": {
            "first_name": "ВЕРА",
            "second_name": "ВИТАЛЬЕВНА",
            "last_name": "ЛЕБЕДЕВА"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/dcd201df-fd30-8f9c-4e82-c7a45ba64dab.jpg",
            "id": "dcd201df-fd30-8f9c-4e82-c7a45ba64dab",
            "name": "75c93610-9978-ed3d-c0a2-d3ab87bc3e66.jpg",
            "size": 13959,
            "createdAt": "2021-07-26T07:14:43+00:00",
            "updatedAt": "2021-07-26T07:15:34+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 836216,
        "fio": {
            "first_name": "Марина",
            "second_name": "Геннадьевна",
            "last_name": "Брехт"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/6881938f50164b189488d0b623101bbd",
            "id": "4325ad60-8b2a-b8af-9bfd-a05bdc301545",
            "name": "6881938f50164b189488d0b623101bbd",
            "size": null,
            "createdAt": "2020-02-29T03:30:53+00:00",
            "updatedAt": "2020-10-28T21:49:22+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 46,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 93042564,
        "fio": {
            "first_name": "Влада    не Владислава",
            "second_name": "Вадимовна",
            "last_name": "Марунова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7b0a170e-01ce-72f3-a231-57c4b1786b5b.jpg",
            "id": "7b0a170e-01ce-72f3-a231-57c4b1786b5b",
            "name": "ae2b3fa4-595f-c923-b047-37421444dd2c.jpg",
            "size": 17017,
            "createdAt": "2022-03-17T20:40:06+00:00",
            "updatedAt": "2022-03-17T20:42:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 37,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 494209,
        "fio": {
            "first_name": "Маргарита",
            "second_name": "Николаевна",
            "last_name": "Зацепина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d56083e9-8fdf-ec47-0a4a-cee6dad17967.jpeg",
            "id": "d56083e9-8fdf-ec47-0a4a-cee6dad17967",
            "name": "6317dd3c-d37b-b0e2-74ee-bdb66c59a6c1.jpeg",
            "size": 20212,
            "createdAt": "2021-10-29T09:32:08+00:00",
            "updatedAt": "2021-10-29T09:33:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92211608,
        "fio": {
            "first_name": "Анжелика",
            "second_name": "Владимировна",
            "last_name": "Кобец"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/53f0a05d-1841-3a67-442e-a795b1d39c62.jpg",
            "id": "53f0a05d-1841-3a67-442e-a795b1d39c62",
            "name": "cc572f9d-c854-8697-ff66-e91bdd198d70.jpg",
            "size": 16699,
            "createdAt": "2022-04-28T11:28:29+00:00",
            "updatedAt": "2022-04-28T11:28:38+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 19,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1030559,
        "fio": {
            "first_name": "Белла",
            "second_name": "Рустамовна",
            "last_name": "Сташ"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7e3e28f4-c002-c3fa-b8e7-f3b525dfc7e1.jpeg",
            "id": "7e3e28f4-c002-c3fa-b8e7-f3b525dfc7e1",
            "name": "a4969805-7935-67f1-6ffb-a80710ce8b18.jpeg",
            "size": 14148,
            "createdAt": "2022-05-23T19:45:26+00:00",
            "updatedAt": "2022-05-24T03:26:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 20,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 22297,
        "fio": {
            "first_name": "Марина",
            "second_name": "Юльевна",
            "last_name": "Молодчикова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/316c904e8f21414fa22171da4da2c3df.jpg",
            "id": "40222269-5d85-7cc6-3ec7-3ba9e647c94b",
            "name": "316c904e8f21414fa22171da4da2c3df.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:03:23+00:00",
            "updatedAt": "2020-10-28T21:13:50+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92396779,
        "fio": {
            "first_name": "Олег",
            "second_name": "Николаевич",
            "last_name": "Заузолков"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 202,
            "height": 202,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f934fef6-2d63-88d6-eb9d-c65e298a4c7e.jpg",
            "id": "f934fef6-2d63-88d6-eb9d-c65e298a4c7e",
            "name": "135e2b46-dce2-e920-4b33-cc193dd7f8d5.jpg",
            "size": 11986,
            "createdAt": "2021-09-03T06:16:08+00:00",
            "updatedAt": "2021-09-03T06:16:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 11929,
        "fio": {
            "first_name": "Ксения",
            "second_name": "Вадимовна",
            "last_name": "Грачева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/07957112-3861-9a68-0f6c-eec0187b09b8.jpg",
            "id": "07957112-3861-9a68-0f6c-eec0187b09b8",
            "name": "4b2d821b-1855-274e-0a72-d2282c45c80e.jpg",
            "size": 7961,
            "createdAt": "2020-07-08T18:41:04+00:00",
            "updatedAt": "2020-07-08T18:41:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 22,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91699793,
        "fio": {
            "first_name": "Юлия",
            "second_name": "Львовна",
            "last_name": "Жукова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91717635,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Сергеевна",
            "last_name": "Рассечкина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5c289855-52d2-89d9-db6b-aa2096edae43.jpg",
            "id": "5c289855-52d2-89d9-db6b-aa2096edae43",
            "name": "f7a3ba7c-b1a4-b982-b6b1-5ceb2848cbb7.jpg",
            "size": 13074,
            "createdAt": "2021-09-22T11:32:45+00:00",
            "updatedAt": "2021-09-22T11:32:45+00:00",
            "expiredAt": "2021-09-23T11:32:45+00:00"
        },
        "statistic": {
            "eventsCount": 48,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91620291,
        "fio": {
            "first_name": "Оксана",
            "second_name": "Михайловна",
            "last_name": "Цветкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9d829b7c-e497-c45b-212b-f60d8a419d3d.png",
            "id": "9d829b7c-e497-c45b-212b-f60d8a419d3d",
            "name": "937759db-e911-9927-bacb-74913fbe14d4.png",
            "size": 37268,
            "createdAt": "2020-08-23T18:57:15+00:00",
            "updatedAt": "2020-08-23T18:57:24+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 20,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91704484,
        "fio": {
            "first_name": "Людмила",
            "second_name": "Виссарионовна",
            "last_name": "Кирсанова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 29,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1552624,
        "fio": {
            "first_name": "Александра",
            "second_name": "Владимировна",
            "last_name": "Бобиченко"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/061fe9af-4bb4-83ad-267d-a7b551c6cfbc.jpg",
            "id": "061fe9af-4bb4-83ad-267d-a7b551c6cfbc",
            "name": "ba29b580-a1c7-89fa-6808-8d3dc2417127.jpg",
            "size": 4883,
            "createdAt": "2020-11-16T12:31:22+00:00",
            "updatedAt": "2021-03-18T17:24:44+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 12,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 759066,
        "fio": {
            "first_name": "Елена",
            "second_name": "Геннадьевна",
            "last_name": "Карасева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d9a5bfc9-799c-0206-b376-8bdaacfcfbe0.jpeg",
            "id": "d9a5bfc9-799c-0206-b376-8bdaacfcfbe0",
            "name": "73507ff5-92e2-f117-46ea-87aca3e81c7c.jpeg",
            "size": 6260,
            "createdAt": "2020-11-15T06:09:56+00:00",
            "updatedAt": "2020-11-15T06:10:58+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 35,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 168582,
        "fio": {
            "first_name": "Никита",
            "second_name": "Михайлович",
            "last_name": "Ольхов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1488188,
        "fio": {
            "first_name": "Дмитрий",
            "second_name": "Юрьевич",
            "last_name": "Мечников"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/57156921-47a1-4274-06e7-1a9ffa0d181f.jpg",
            "id": "57156921-47a1-4274-06e7-1a9ffa0d181f",
            "name": "e0f1cc1f-1a8e-42ba-3dfe-0627ab476d01.jpg",
            "size": 6162,
            "createdAt": "2020-09-09T09:28:02+00:00",
            "updatedAt": "2020-09-09T09:30:29+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 22,
            "volunteerRating": 4.8,
            "organizerRating": 4.4
        },
        "canContact": true
    }, {
        "id": 91732360,
        "fio": {
            "first_name": "Анна",
            "second_name": "Александровна",
            "last_name": "Ломакина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/566c6ed5-b3fd-757c-b327-2035aa046e5a.jpg",
            "id": "566c6ed5-b3fd-757c-b327-2035aa046e5a",
            "name": "9c4e1e59-1380-dcd0-c91d-801746aa9748.jpg",
            "size": 8515,
            "createdAt": "2020-10-28T07:56:32+00:00",
            "updatedAt": "2020-10-28T07:57:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 15,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92102943,
        "fio": {
            "first_name": "Ярослав",
            "second_name": "Денисович",
            "last_name": "Добренко"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c973ce33-95c4-d138-2c34-eef4f9ef9c63.jpg",
            "id": "c973ce33-95c4-d138-2c34-eef4f9ef9c63",
            "name": "178e1441-81fc-5d29-eaba-160d5704b929.jpg",
            "size": 6092,
            "createdAt": "2021-02-28T12:17:02+00:00",
            "updatedAt": "2021-02-28T12:17:46+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 10,
            "volunteerRating": 4.9,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92288792,
        "fio": {
            "first_name": "Светлана",
            "second_name": "Эрановна",
            "last_name": "Захватова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 34,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 824486,
        "fio": {
            "first_name": "Анна",
            "second_name": "Валерьевна",
            "last_name": "Арзамасова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/0418095a-69d5-5446-b8c3-714cca3686ed.jpg",
            "id": "0418095a-69d5-5446-b8c3-714cca3686ed",
            "name": "23ae07b3-771b-cca0-d4da-3d74b144b29f.jpg",
            "size": 19065,
            "createdAt": "2022-03-30T09:29:47+00:00",
            "updatedAt": "2022-03-30T09:38:27+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 25,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1297038,
        "fio": {
            "first_name": "Алёна",
            "second_name": "Евгеньевна",
            "last_name": "Качикина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/decb3e35-9b1a-c702-42fe-ee7f56c5d294.jpeg",
            "id": "decb3e35-9b1a-c702-42fe-ee7f56c5d294",
            "name": "add98e23-6a99-edb4-d6b5-a19e0dfdd308.jpeg",
            "size": 8301,
            "createdAt": "2021-04-22T12:39:21+00:00",
            "updatedAt": "2021-04-22T12:39:30+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 22,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92014829,
        "fio": {
            "first_name": "Татьяна",
            "second_name": "Алексеевна",
            "last_name": "Синельникова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 193957,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Сергеевна",
            "last_name": "Радюк"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/80e4d3bc427b443487ef89485443d151.jpg",
            "id": "4156f47d-2e21-4b0b-58b6-8bf1c522ac57",
            "name": "80e4d3bc427b443487ef89485443d151.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:18:38+00:00",
            "updatedAt": "2020-10-28T21:21:58+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91577840,
        "fio": {
            "first_name": "ТАТЬЯНА",
            "second_name": "ВИКторовна",
            "last_name": "РЕВТОВА"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92381631,
        "fio": {
            "first_name": "ИРЕНА",
            "second_name": "ВЛАДИМИРОВНА",
            "last_name": "НЕМЦОВА"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/64F056-%D0%9D%D0%98.png",
            "id": "e8167abd-ed34-a67d-734c-c6aeac031cf8",
            "name": "64F056-НИ.png",
            "size": 4634,
            "createdAt": "2021-08-20T06:23:44+00:00",
            "updatedAt": "2021-08-20T06:23:44+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92264081,
        "fio": {
            "first_name": "Денис",
            "second_name": "Алексеевич",
            "last_name": "Смирнов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/b0c10f7e-1e90-9303-4f94-bf33763a56af.png",
            "id": "b0c10f7e-1e90-9303-4f94-bf33763a56af",
            "name": "e1c4ef9e-ba5d-e3b1-c1ec-d017ca85a6a7.png",
            "size": 131162,
            "createdAt": "2021-05-01T15:22:20+00:00",
            "updatedAt": "2021-05-01T18:19:31+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 44,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 393439,
        "fio": {
            "first_name": "Полина",
            "second_name": "Ивановна",
            "last_name": "Чупина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 1440,
            "height": 1440,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/cb901dfb-9f0c-2e47-a9f4-0c6a19efb412.",
            "id": "cb901dfb-9f0c-2e47-a9f4-0c6a19efb412",
            "name": "ios-pic",
            "size": 2308606,
            "createdAt": "2022-03-18T08:03:34+00:00",
            "updatedAt": "2022-03-18T08:14:43+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92627510,
        "fio": {
            "first_name": "Елена",
            "second_name": "Николаевна",
            "last_name": "Ахтырская"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/CE9641-%D0%90%D0%95.png",
            "id": "7274fb0b-6641-fd6e-4195-fe8a398b16ce",
            "name": "CE9641-АЕ.png",
            "size": 8461,
            "createdAt": "2021-10-24T19:09:05+00:00",
            "updatedAt": "2021-10-24T19:09:05+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 33,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92701958,
        "fio": {
            "first_name": "Мария",
            "second_name": "Валерьевна",
            "last_name": "Уткина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/7379BE-%D0%A3%D0%9C.png",
            "id": "934b661a-e10b-ebd6-eac7-d3cefb9c5971",
            "name": "7379BE-УМ.png",
            "size": 10994,
            "createdAt": "2021-11-02T12:27:20+00:00",
            "updatedAt": "2021-11-02T12:27:20+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92305793,
        "fio": {
            "first_name": "Алёна",
            "second_name": "Денисовна",
            "last_name": "Одерова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6a486cff-bb45-e9cb-ca97-2f7239910340.png",
            "id": "6a486cff-bb45-e9cb-ca97-2f7239910340",
            "name": "cdd29034-b24f-d6c4-b0f1-8ce448d23180.png",
            "size": 138111,
            "createdAt": "2021-12-24T21:54:17+00:00",
            "updatedAt": "2022-01-18T16:03:01+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 32,
            "volunteerRating": 4.9,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1304260,
        "fio": {
            "first_name": "Анна",
            "second_name": "Николаевна",
            "last_name": "Сергеева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1493729,
        "fio": {
            "first_name": "Елена",
            "second_name": "Михайловна",
            "last_name": "Белова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2c1d2af7-f6f0-582e-98d5-81049394fa85.jpg",
            "id": "2c1d2af7-f6f0-582e-98d5-81049394fa85",
            "name": "ff7ab09a-e800-1511-f148-1c1a4b0a6023.jpg",
            "size": 17325,
            "createdAt": "2021-05-19T13:49:23+00:00",
            "updatedAt": "2021-05-19T13:57:39+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 10084417,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Александровна",
            "last_name": "Забрамная"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91611479,
        "fio": {
            "first_name": "Наталья",
            "second_name": "Николаевна",
            "last_name": "Братчикова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/eacf1a75-8dce-7c5d-1000-492fdcd85af9.jpg",
            "id": "eacf1a75-8dce-7c5d-1000-492fdcd85af9",
            "name": "2d96518d-6bab-264a-718d-950b1084052d.jpg",
            "size": 5121,
            "createdAt": "2020-11-24T17:42:00+00:00",
            "updatedAt": "2020-11-24T17:42:50+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 5,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92349191,
        "fio": {
            "first_name": "Софья",
            "second_name": "Германовна",
            "last_name": "Рийсман"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/CE18E1-%D0%A0%D0%A1.png",
            "id": "280be9ab-450a-25d3-dcfa-cf47b1e8c2d1",
            "name": "CE18E1-РС.png",
            "size": 9480,
            "createdAt": "2021-07-14T08:07:19+00:00",
            "updatedAt": "2021-07-14T08:07:19+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91986767,
        "fio": {
            "first_name": "Александр",
            "second_name": "Дмитриевич",
            "last_name": "Подстрижень"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/5bd88e77-0399-788b-6969-6043a3c54ae3.jpeg",
            "id": "5bd88e77-0399-788b-6969-6043a3c54ae3",
            "name": "result___0_1343538906254836180.jpeg",
            "size": 59004,
            "createdAt": "2022-01-21T18:19:11+00:00",
            "updatedAt": "2022-01-21T18:19:11+00:00",
            "expiredAt": "2022-01-22T18:19:11+00:00"
        },
        "statistic": {
            "eventsCount": 25,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 212933,
        "fio": {
            "first_name": "Софья",
            "second_name": "Ибрагимовна",
            "last_name": "Куликова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/df06e8f9b0e04d83a170815a74557286.jpgava1",
            "id": "dda2fcc9-71de-68a5-696b-bc98bcb9c170",
            "name": "df06e8f9b0e04d83a170815a74557286.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:17:04+00:00",
            "updatedAt": "2020-12-03T09:54:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 36,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1504512,
        "fio": {
            "first_name": "Анжелика",
            "second_name": "Владимировна",
            "last_name": "Мартынова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/f65a943a-66f5-5c4c-7b76-50bb66578dea.jpg",
            "id": "f65a943a-66f5-5c4c-7b76-50bb66578dea",
            "name": "200f0483-c6f3-cc81-dad8-93b553be2cc8.jpg",
            "size": 12773,
            "createdAt": "2022-05-28T07:47:26+00:00",
            "updatedAt": "2022-05-28T07:50:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 15,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92128417,
        "fio": {
            "first_name": "Анна",
            "second_name": "Андреевна",
            "last_name": "Акулинина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/7ee473e2-73de-cdf0-eaad-287da97b4822.jpeg",
            "id": "7ee473e2-73de-cdf0-eaad-287da97b4822",
            "name": "377cdc41-fd07-52de-fe83-b797ab46b588.jpeg",
            "size": 4599,
            "createdAt": "2021-03-03T11:33:02+00:00",
            "updatedAt": "2021-03-03T11:34:47+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 930379,
        "fio": {
            "first_name": "Оксана",
            "second_name": "Николаевна",
            "last_name": "Челмодеева"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/9e88a14f-566c-3a72-aa72-16bc6be79375.jpg",
            "id": "9e88a14f-566c-3a72-aa72-16bc6be79375",
            "name": "7d38d280-f681-5f63-ad05-e0486cc84407.jpg",
            "size": 19154,
            "createdAt": "2021-11-01T18:14:09+00:00",
            "updatedAt": "2021-11-01T18:16:11+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 727334,
        "fio": {
            "first_name": "Светлана",
            "second_name": "Николаевна",
            "last_name": "Таранченко"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/6ba046f7a72d4dabb4d7c3b827c58e9e.jpg",
            "id": "904bc7c1-a21e-f778-b831-046f1cd28cab",
            "name": "6ba046f7a72d4dabb4d7c3b827c58e9e.jpg",
            "size": null,
            "createdAt": "2020-02-29T03:20:42+00:00",
            "updatedAt": "2020-10-29T20:14:45+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 42,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91824334,
        "fio": {
            "first_name": "Никита",
            "second_name": "Олегович",
            "last_name": "Алексеенко"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/272933ba-8f4a-1442-2a0e-964b350fc478.jpg",
            "id": "272933ba-8f4a-1442-2a0e-964b350fc478",
            "name": "63b6e453-24b4-7350-7dab-253ba2f9e897.jpg",
            "size": 5080,
            "createdAt": "2020-10-19T17:33:38+00:00",
            "updatedAt": "2020-10-19T17:35:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 16,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91916455,
        "fio": {
            "first_name": "Ксения",
            "second_name": "Алексеевна",
            "last_name": "Южакова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 26,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92252168,
        "fio": {
            "first_name": "Кристина",
            "second_name": "Владимировна",
            "last_name": "Санфирова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 25,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92666857,
        "fio": {
            "first_name": "Юлия",
            "second_name": "Игоревна",
            "last_name": "Карманова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/0BF209-%D0%9A%D0%AE.png",
            "id": "de08d10a-e82f-ee70-edbd-4b46b00eba24",
            "name": "0BF209-КЮ.png",
            "size": 11502,
            "createdAt": "2021-10-28T18:19:52+00:00",
            "updatedAt": "2021-10-28T18:19:52+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 93187628,
        "fio": {
            "first_name": "Ника",
            "second_name": "Кирилловна",
            "last_name": "Зунина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/1DB53C-%D0%97%D0%9D.png",
            "id": "bacd55ea-3d52-bd8f-15d2-21a70a76fd0e",
            "name": "1DB53C-ЗН.png",
            "size": 9240,
            "createdAt": "2022-04-04T14:48:09+00:00",
            "updatedAt": "2022-04-04T14:48:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93208233,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Геннадьевна",
            "last_name": "Никитина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/88B444-%D0%9D%D0%90.png",
            "id": "e71816f9-6482-3747-725f-733854f5312f",
            "name": "88B444-НА.png",
            "size": 8472,
            "createdAt": "2022-04-07T13:22:29+00:00",
            "updatedAt": "2022-04-07T13:22:29+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93342007,
        "fio": {
            "first_name": "Лада",
            "second_name": "Борисовна",
            "last_name": "Костина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/FD3635-%D0%9A%D0%9B.png",
            "id": "1aacaa9e-86d6-2444-cd1c-835602814206",
            "name": "FD3635-КЛ.png",
            "size": 8764,
            "createdAt": "2022-05-13T05:59:33+00:00",
            "updatedAt": "2022-05-13T05:59:33+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93342013,
        "fio": {
            "first_name": "Гольцова",
            "second_name": "Игоревна",
            "last_name": "Полина"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/D60230-%D0%9F%D0%93.png",
            "id": "c2d923af-fb52-6f3c-c65b-07fda41793ec",
            "name": "D60230-ПГ.png",
            "size": 3218,
            "createdAt": "2022-05-13T05:59:50+00:00",
            "updatedAt": "2022-05-13T05:59:50+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93342095,
        "fio": {
            "first_name": "Денислам",
            "second_name": "Юнусович",
            "last_name": "Рашидов"
        },
        "gender": "M",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/DF45E5-%D0%A0%D0%94.png",
            "id": "74690f25-adf2-a563-0457-04b9c50d82ed",
            "name": "DF45E5-РД.png",
            "size": 5035,
            "createdAt": "2022-05-13T06:06:18+00:00",
            "updatedAt": "2022-05-13T06:06:18+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93343323,
        "fio": {
            "first_name": "Софья",
            "second_name": "Романовна",
            "last_name": "Белявцева"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/6D252D-%D0%91%D0%A1.png",
            "id": "3986b195-cf7c-0263-2d44-9d1f439d7129",
            "name": "6D252D-БС.png",
            "size": 10025,
            "createdAt": "2022-05-13T07:53:54+00:00",
            "updatedAt": "2022-05-13T07:53:54+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93408550,
        "fio": {
            "first_name": "Альбина",
            "second_name": "Айдеровна",
            "last_name": "Ибадлаева"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": "0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/EE2C38-%D0%98%D0%90.png",
            "id": "17cb4462-afa6-bcc0-aa09-c1bc29d078f3",
            "name": "EE2C38-ИА.png",
            "size": 9818,
            "createdAt": "2022-05-26T10:53:16+00:00",
            "updatedAt": "2022-05-26T10:53:16+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 233390,
        "fio": {
            "first_name": "Раиса",
            "second_name": "Акрамовна",
            "last_name": "Хайдарова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/5249865f51724363ad27ffcc3bf9b19d.jpgava1",
            "id": "07620f17-976c-22fc-1f8f-1c60efc15a17",
            "name": "5249865f51724363ad27ffcc3bf9b19d.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:19:16+00:00",
            "updatedAt": "2020-10-19T03:08:01+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 20,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91822563,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Александровна",
            "last_name": "Полякова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c7728d2b-47fe-3e2f-c9a0-926270b03f38.jpeg",
            "id": "c7728d2b-47fe-3e2f-c9a0-926270b03f38",
            "name": "94e8f071-354a-b90a-f872-9f10f50af0da.jpeg",
            "size": 7970,
            "createdAt": "2021-02-28T18:29:24+00:00",
            "updatedAt": "2021-02-28T18:32:35+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 26,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92106654,
        "fio": {
            "first_name": "Александр",
            "second_name": "Павлович",
            "last_name": "Ильинский"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8e10bd0a-64ef-7424-bddd-6a5dc9d70140.jpg",
            "id": "8e10bd0a-64ef-7424-bddd-6a5dc9d70140",
            "name": "9ef62ba7-ed4c-d4ec-62af-a388c0be1eb3.jpg",
            "size": 24668,
            "createdAt": "2021-11-13T09:31:51+00:00",
            "updatedAt": "2021-11-13T09:31:51+00:00",
            "expiredAt": "2021-11-14T09:31:51+00:00"
        },
        "statistic": {
            "eventsCount": 27,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92118130,
        "fio": {
            "first_name": "Кристина",
            "second_name": "Юрьевна",
            "last_name": "Миропольская"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/aba297c8-e963-bb92-bcbf-7bd8f89f81b5.jpeg",
            "id": "aba297c8-e963-bb92-bcbf-7bd8f89f81b5",
            "name": "63f07799-e8c3-f9a9-9e82-f248b6ee4404.jpeg",
            "size": 6581,
            "createdAt": "2021-03-06T17:13:08+00:00",
            "updatedAt": "2021-03-06T17:13:16+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 4.9,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92191232,
        "fio": {
            "first_name": "Александра",
            "second_name": "Александровна",
            "last_name": "Дробышева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/116881ed-ca0a-123d-1331-f98d8303974f.jpeg",
            "id": "116881ed-ca0a-123d-1331-f98d8303974f",
            "name": "caff42c7-1d61-ee67-8698-8cf4979fe73c.jpeg",
            "size": 18939,
            "createdAt": "2021-05-27T17:24:34+00:00",
            "updatedAt": "2021-05-27T17:26:29+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 19171,
        "fio": {
            "first_name": "Ольга",
            "second_name": "Ивановна",
            "last_name": "Кузина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/773d86dd8ab740348088eff30319e081.jpgava1",
            "id": "44a556c0-3936-3740-f90a-5d1a5ac884ab",
            "name": "773d86dd8ab740348088eff30319e081.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:02:58+00:00",
            "updatedAt": "2020-10-28T22:23:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 39,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 567961,
        "fio": {
            "first_name": "Борохович",
            "second_name": "Денисовна",
            "last_name": "Евгения"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a9c5a215-825f-481c-add9-a946f2123e8f.jpg",
            "id": "a9c5a215-825f-481c-add9-a946f2123e8f",
            "name": "c0d688d3-f983-e6ad-2291-507d384245bc.jpg",
            "size": 16872,
            "createdAt": "2021-05-28T20:51:24+00:00",
            "updatedAt": "2021-05-28T20:53:37+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91723114,
        "fio": {
            "first_name": "Катя",
            "second_name": null,
            "last_name": "Водопьянова"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91743126,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Саид",
            "last_name": "Хермез"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 16,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91969419,
        "fio": {
            "first_name": "Марина",
            "second_name": "Робертовна",
            "last_name": "Лян"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 12,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92291977,
        "fio": {
            "first_name": "Елена",
            "second_name": "Анатольевна",
            "last_name": "Самаркина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 19,
            "volunteerRating": 5,
            "organizerRating": 4.9
        },
        "canContact": true
    }, {
        "id": 92360641,
        "fio": {
            "first_name": "Родион",
            "second_name": "Сергеевич",
            "last_name": "Боголюбов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/3CBD7A-%D0%91%D0%A0.png",
            "id": "9a7b9df4-477d-1d6f-0c17-d2c43b822781",
            "name": "3CBD7A-БР.png",
            "size": 8502,
            "createdAt": "2021-07-27T08:17:14+00:00",
            "updatedAt": "2021-07-27T08:17:14+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92432296,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Леонидовна",
            "last_name": "Камендровская"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/6EC0A2-%D0%9A%D0%94.png",
            "id": "d89c543c-c522-8639-583b-995c9059a6c4",
            "name": "6EC0A2-КД.png",
            "size": 8965,
            "createdAt": "2021-09-12T05:51:40+00:00",
            "updatedAt": "2021-09-12T05:51:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 19,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 161411,
        "fio": {
            "first_name": "Ярослав",
            "second_name": "Сергеевич",
            "last_name": "Блажеев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "г Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/c9d75363f34847e7a7e034b05d36874c.jpgava1",
            "id": "33a28923-54b6-44fb-469a-f34da4a05dfe",
            "name": "c9d75363f34847e7a7e034b05d36874c.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:10:45+00:00",
            "updatedAt": "2020-10-20T03:13:46+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 5,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91749744,
        "fio": {
            "first_name": "Анна",
            "second_name": "Львовна",
            "last_name": "Чернышова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 29,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 33535,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Александровна",
            "last_name": "Бацманова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/54888020d6be4f6d8ee87e6804847f52.jpg",
            "id": "0118bf78-c579-9098-8fcf-3eb5838604a7",
            "name": "54888020d6be4f6d8ee87e6804847f52.jpg",
            "size": null,
            "createdAt": "2020-02-29T02:05:03+00:00",
            "updatedAt": "2020-10-18T17:43:28+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 43,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1501134,
        "fio": {
            "first_name": "Вера",
            "second_name": "Дмитриевна",
            "last_name": "Ушакова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/1ae9a2db-b881-4fae-4ef6-b9825b81b191.jpg",
            "id": "1ae9a2db-b881-4fae-4ef6-b9825b81b191",
            "name": "c7029f44-9f5c-a980-eb91-bae80e0bed5c.jpg",
            "size": 18852,
            "createdAt": "2022-03-15T18:43:55+00:00",
            "updatedAt": "2022-03-15T18:44:12+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92264721,
        "fio": {
            "first_name": "Милана",
            "second_name": "Исламовна",
            "last_name": "Яхиханова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/039e6ccf-284f-48aa-6af7-dbd7fe7e3cb0.jpg",
            "id": "039e6ccf-284f-48aa-6af7-dbd7fe7e3cb0",
            "name": "663406a1-161d-1fb6-1c41-bb66727a083a.jpg",
            "size": 14770,
            "createdAt": "2021-05-29T21:10:32+00:00",
            "updatedAt": "2021-06-28T20:48:54+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92280804,
        "fio": {
            "first_name": "Милана",
            "second_name": "Аркадьевна",
            "last_name": "Бутунц"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 376218,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Сергеевна",
            "last_name": "Романчук"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/760577f5-b71e-c921-b7a9-e42cc6f35f87.jpg",
            "id": "760577f5-b71e-c921-b7a9-e42cc6f35f87",
            "name": "26821242-1ed7-ea63-15dc-8e02ffeb8957.jpg",
            "size": 7619,
            "createdAt": "2020-11-05T10:22:41+00:00",
            "updatedAt": "2020-11-05T10:30:48+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92406495,
        "fio": {
            "first_name": "Марина",
            "second_name": "Алексеевна",
            "last_name": "Кузьминых"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/3D42D7-%D0%9A%D0%9C.png",
            "id": "c4e1e6ca-5e8c-830a-602d-e7ddc535b875",
            "name": "3D42D7-КМ.png",
            "size": 10328,
            "createdAt": "2021-09-05T16:21:57+00:00",
            "updatedAt": "2021-09-05T16:21:57+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93055027,
        "fio": {
            "first_name": "Алексей",
            "second_name": "Юрьевич",
            "last_name": "Кудашев"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/E5179D-%D0%9A%D0%90.png",
            "id": "8d4ee56e-f189-6c53-f47f-0aa98d5276dc",
            "name": "E5179D-КА.png",
            "size": 10174,
            "createdAt": "2022-02-26T21:00:02+00:00",
            "updatedAt": "2022-02-26T21:00:02+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93058084,
        "fio": {
            "first_name": "Алексей",
            "second_name": "Борисович",
            "last_name": "Алексеев"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/17BF54-%D0%90%D0%90.png",
            "id": "f9042286-45c1-52d7-43f0-29dc10d42188",
            "name": "17BF54-АА.png",
            "size": 8704,
            "createdAt": "2022-02-28T10:45:23+00:00",
            "updatedAt": "2022-02-28T10:45:23+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 234444,
        "fio": {
            "first_name": "Гульназ",
            "second_name": "Минегалиевна",
            "last_name": "Ибрагимова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/a78702a9fd034381b5c30f110d4678c9.jpgava1",
            "id": "23442dda-8303-41a1-28fb-70b29b52e3a8",
            "name": "a78702a9fd034381b5c30f110d4678c9.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:21:31+00:00",
            "updatedAt": "2020-10-19T07:18:36+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 741110,
        "fio": {
            "first_name": "Сергей",
            "second_name": "Андреевич",
            "last_name": "Кирюшатов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/4e750aba-dcfe-a520-4d09-6c0ccbf2670f.jpeg",
            "id": "4e750aba-dcfe-a520-4d09-6c0ccbf2670f",
            "name": "result___0_5794897766362945278.jpeg",
            "size": 81916,
            "createdAt": "2022-05-28T06:15:21+00:00",
            "updatedAt": "2022-05-28T06:15:21+00:00",
            "expiredAt": "2022-05-29T06:15:21+00:00"
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": 3
        },
        "canContact": true
    }, {
        "id": 91767528,
        "fio": {
            "first_name": "Наталья",
            "second_name": "Павловна",
            "last_name": "Корчуганова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 34,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91829741,
        "fio": {
            "first_name": "Валерия",
            "second_name": "Васильевна",
            "last_name": "Косьяненко"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92109933,
        "fio": {
            "first_name": "Руслан",
            "second_name": "Исамович",
            "last_name": "Барзак"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e3bfd86-bede-6347-65ae-be39bc72861a.jpg",
            "id": "2e3bfd86-bede-6347-65ae-be39bc72861a",
            "name": "96efa442-9fed-a92e-cd93-255122c78c2d.jpg",
            "size": 18268,
            "createdAt": "2022-02-26T19:57:44+00:00",
            "updatedAt": "2022-02-26T19:58:01+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 4,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92282100,
        "fio": {
            "first_name": "Мария",
            "second_name": "Александровна",
            "last_name": "Нечмирь"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2029119e-b3eb-65ba-e095-3a93c08d4a0a.jpg",
            "id": "2029119e-b3eb-65ba-e095-3a93c08d4a0a",
            "name": "55f62d2d-082d-a150-38bc-4e9eb068b071.jpg",
            "size": 13322,
            "createdAt": "2021-05-20T19:19:40+00:00",
            "updatedAt": "2021-05-20T19:20:31+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93071192,
        "fio": {
            "first_name": "Хава",
            "second_name": "Рамазановна",
            "last_name": "Темирсултанова"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/4AFFFA-%D0%A2%D0%A5.png",
            "id": "5333ff37-5fd3-e382-5ad6-02e28b68f8ff",
            "name": "4AFFFA-ТХ.png",
            "size": 5089,
            "createdAt": "2022-03-06T20:11:08+00:00",
            "updatedAt": "2022-03-06T20:11:08+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93182509,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Сергеевна",
            "last_name": "Рябова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/376a1a8e-a019-ea25-70eb-ef5d07c709ef.jpeg",
            "id": "376a1a8e-a019-ea25-70eb-ef5d07c709ef",
            "name": "result___0_345108346162735952.jpeg",
            "size": 78715,
            "createdAt": "2022-04-14T11:49:55+00:00",
            "updatedAt": "2022-04-14T11:49:55+00:00",
            "expiredAt": "2022-04-15T11:49:55+00:00"
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 995042,
        "fio": {
            "first_name": "Егор",
            "second_name": "Юрьевич",
            "last_name": "Николаев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/3d9a83d4-5162-f81f-2488-40ad4b73e4af.jpeg",
            "id": "3d9a83d4-5162-f81f-2488-40ad4b73e4af",
            "name": "1d8fb03d-68c4-356a-65b1-15353a4e4fd1.jpeg",
            "size": 14039,
            "createdAt": "2021-09-08T12:06:42+00:00",
            "updatedAt": "2021-09-08T12:08:47+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1048312,
        "fio": {
            "first_name": "Татьяна",
            "second_name": "Алексеевна",
            "last_name": "Огай"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/0b07df5bcb1241758bc5ab6d6e3a98cc.jpg",
            "id": "38d515e4-5b88-dad1-b3cf-f65ce63a4738",
            "name": "0b07df5bcb1241758bc5ab6d6e3a98cc.jpg",
            "size": null,
            "createdAt": "2020-02-29T03:48:00+00:00",
            "updatedAt": "2020-10-20T04:06:44+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1120332,
        "fio": {
            "first_name": "Камила",
            "second_name": "Рашидовна",
            "last_name": "Казиева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1120344,
        "fio": {
            "first_name": "Феликс",
            "second_name": "Самсонович",
            "last_name": "Пилоян"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d8c7dc2f-d21e-f909-0263-c5002ace3ed2.jpeg",
            "id": "d8c7dc2f-d21e-f909-0263-c5002ace3ed2",
            "name": "f8cb9d60-54c7-f4a7-3947-b1f1e1f7e7b2.jpeg",
            "size": 3346,
            "createdAt": "2020-09-11T11:00:51+00:00",
            "updatedAt": "2020-09-11T11:01:02+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92381801,
        "fio": {
            "first_name": "Мария",
            "second_name": "Александровна",
            "last_name": "Смирнова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/111f217b-0bfa-a84a-b768-78bb6f227b55.jpg",
            "id": "111f217b-0bfa-a84a-b768-78bb6f227b55",
            "name": "91cb1e0d-be29-6d07-97e7-b2bc03442b5c.jpg",
            "size": 19479,
            "createdAt": "2021-08-20T08:52:52+00:00",
            "updatedAt": "2021-08-20T08:56:32+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 10058025,
        "fio": {
            "first_name": "Светлана",
            "second_name": "Викторовна",
            "last_name": "Ржанухина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Троицк",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Троицк",
            "settlementCode": "7700000500000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.3067639,
            "y": 55.4846387
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 5,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92599513,
        "fio": {
            "first_name": "Маргарита",
            "second_name": "Каковна",
            "last_name": "Ониани"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/94C7E2-%D0%9E%D0%9C.png",
            "id": "3056e6c3-9d37-ec4f-4ca6-eb5a210579d2",
            "name": "94C7E2-ОМ.png",
            "size": 6712,
            "createdAt": "2021-10-20T14:12:55+00:00",
            "updatedAt": "2021-10-20T14:12:55+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 15,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1313041,
        "fio": {
            "first_name": "Мария",
            "second_name": "Сергеевна",
            "last_name": "Мазалова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92167918,
        "fio": {
            "first_name": "Данияр",
            "second_name": "Рустемович",
            "last_name": "Закиров"
        },
        "gender": "M",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/737cdb8f-16b3-eb89-5801-632eff9dea1e.jpeg",
            "id": "737cdb8f-16b3-eb89-5801-632eff9dea1e",
            "name": "450ea8cd-e130-8fa1-221a-363609fc7523.jpeg",
            "size": 20661,
            "createdAt": "2021-06-09T07:03:48+00:00",
            "updatedAt": "2021-06-09T07:03:52+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 33,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92226607,
        "fio": {
            "first_name": "Полина",
            "second_name": "Николаевна",
            "last_name": "Беспалова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 22,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92451923,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Денисовна",
            "last_name": "Сторонова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/AC38FC-%D0%A1%D0%95.png",
            "id": "7cf6137a-9366-1e96-8ff7-1e8c22d23443",
            "name": "AC38FC-СЕ.png",
            "size": 8108,
            "createdAt": "2021-09-16T12:11:02+00:00",
            "updatedAt": "2021-09-16T12:11:02+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 715391,
        "fio": {
            "first_name": "Irina",
            "second_name": "A",
            "last_name": "Smetanina"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 38,
            "volunteerRating": 4.9,
            "organizerRating": 5
        },
        "canContact": false
    }, {
        "id": 92803944,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Анатольевна",
            "last_name": "Петрова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/61D118-%D0%9F%D0%94.png",
            "id": "5287f4ab-fbeb-a620-2e9b-8292c7baea91",
            "name": "61D118-ПД.png",
            "size": 4403,
            "createdAt": "2021-11-21T19:50:14+00:00",
            "updatedAt": "2021-11-21T19:50:14+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 593520,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Сергеевна",
            "last_name": "Борисова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/bbacf108dc2f4b0e93aafe37ea319a80.jpgava1",
            "id": "8123d800-47eb-ea41-3881-f73d897474bb",
            "name": "bbacf108dc2f4b0e93aafe37ea319a80.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T03:02:43+00:00",
            "updatedAt": "2020-10-29T15:22:42+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91822787,
        "fio": {
            "first_name": "Елизавета",
            "second_name": "Виталиевна",
            "last_name": "Ведмеденко"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/94727e52-cd88-a16b-8dfa-f48ee70e3489.jpeg",
            "id": "94727e52-cd88-a16b-8dfa-f48ee70e3489",
            "name": "9150f658-a49e-c228-8591-2c41dbbc722b.jpeg",
            "size": 8190,
            "createdAt": "2021-03-31T09:41:35+00:00",
            "updatedAt": "2021-03-31T09:41:40+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 21,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92326460,
        "fio": {
            "first_name": "Юлия",
            "second_name": "Сергеевна",
            "last_name": "Жулина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d207b438-93b6-1f61-babc-8c59eb0d721f.jpg",
            "id": "d207b438-93b6-1f61-babc-8c59eb0d721f",
            "name": "a0f1ff69-4488-e4ca-afd0-1f498f54e7ec.jpg",
            "size": 13788,
            "createdAt": "2022-02-18T13:12:10+00:00",
            "updatedAt": "2022-02-18T13:12:10+00:00",
            "expiredAt": "2022-02-19T13:12:09+00:00"
        },
        "statistic": {
            "eventsCount": 9,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92457984,
        "fio": {
            "first_name": "Мария",
            "second_name": "Евгеньевна",
            "last_name": "Цветкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/A97E30-%D0%A6%D0%9C.png",
            "id": "6b9d8ff1-be3a-c548-a111-d9d674b657a1",
            "name": "A97E30-ЦМ.png",
            "size": 8579,
            "createdAt": "2021-09-19T06:27:19+00:00",
            "updatedAt": "2021-09-19T06:27:19+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 12,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 272278,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Сергеевна",
            "last_name": "Шевцова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/8f71115b-a56d-d8bb-b345-35f9a946b2eb.jpeg",
            "id": "8f71115b-a56d-d8bb-b345-35f9a946b2eb",
            "name": "result___0_8934910113440523247.jpeg",
            "size": 95402,
            "createdAt": "2022-04-20T04:08:28+00:00",
            "updatedAt": "2022-06-03T08:10:57+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 17,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 463387,
        "fio": {
            "first_name": "Нина",
            "second_name": "Степановна",
            "last_name": "Блинкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 32,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92503799,
        "fio": {
            "first_name": "Кристина",
            "second_name": "Денисовна",
            "last_name": "Белозорова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/faa5df87-a6a7-2e7a-405b-8b99188eb6b0.jpeg",
            "id": "faa5df87-a6a7-2e7a-405b-8b99188eb6b0",
            "name": "result___0_6622128776481071284.jpeg",
            "size": 73461,
            "createdAt": "2022-02-22T19:08:06+00:00",
            "updatedAt": "2022-02-22T19:08:06+00:00",
            "expiredAt": "2022-02-23T19:08:06+00:00"
        },
        "statistic": {
            "eventsCount": 41,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91590455,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Антоновна",
            "last_name": "Матвеева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 238,
            "height": 238,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a40221d9-56ca-d9ba-9673-948f40261d36.jpeg",
            "id": "a40221d9-56ca-d9ba-9673-948f40261d36",
            "name": "1c48260b-5460-cd60-cf82-8ebe8bc1ae42.jpeg",
            "size": 19504,
            "createdAt": "2022-04-14T06:21:50+00:00",
            "updatedAt": "2022-04-14T06:22:00+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 26,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92112554,
        "fio": {
            "first_name": "Никита",
            "second_name": "Юрьевич",
            "last_name": "Никитин"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92223597,
        "fio": {
            "first_name": "Сергей",
            "second_name": "Павлович",
            "last_name": "Ескин"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92387078,
        "fio": {
            "first_name": "Полина",
            "second_name": "Олеговна",
            "last_name": "Михайлова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/038e32cc-0191-9bc2-2f85-4fc5cb18393e.jpg",
            "id": "038e32cc-0191-9bc2-2f85-4fc5cb18393e",
            "name": "33889c41-622b-ba20-2dd2-bc83c03ea191.jpg",
            "size": 9834,
            "createdAt": "2021-08-26T10:30:53+00:00",
            "updatedAt": "2021-08-26T10:31:39+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 6,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92429765,
        "fio": {
            "first_name": "Яна",
            "second_name": "Арменовна",
            "last_name": "Аракелян"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/15f37d30-a3b2-412e-79d6-7a628207f3da.jpg",
            "id": "15f37d30-a3b2-412e-79d6-7a628207f3da",
            "name": "9e62b1e7-ad96-fefb-8792-278ca2f82610.jpg",
            "size": 12536,
            "createdAt": "2021-09-10T15:01:19+00:00",
            "updatedAt": "2021-09-10T15:02:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91705274,
        "fio": {
            "first_name": "Игорь",
            "second_name": "Андреевич",
            "last_name": "Демиденко"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/851640d9-acc4-93c0-96bd-406fef84eaa4.jpeg",
            "id": "851640d9-acc4-93c0-96bd-406fef84eaa4",
            "name": "result___0_3113652410056636295.jpeg",
            "size": 63687,
            "createdAt": "2022-03-26T05:17:00+00:00",
            "updatedAt": "2022-04-15T14:45:45+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": 3
        },
        "canContact": true
    }, {
        "id": 92067910,
        "fio": {
            "first_name": "Дарья",
            "second_name": "Антоновна",
            "last_name": "Ефимова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/a62043b0-e0c8-5866-8f92-5fe74fd8e362.jpg",
            "id": "a62043b0-e0c8-5866-8f92-5fe74fd8e362",
            "name": "c476c541-bf61-2801-d53d-4dfb66c03b09.jpg",
            "size": 6557,
            "createdAt": "2021-03-07T09:07:51+00:00",
            "updatedAt": "2021-03-07T09:08:04+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 23,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 10047264,
        "fio": {
            "first_name": "Ирина",
            "second_name": "Николаевна",
            "last_name": "Захарова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/6d635dea-d057-211b-ec77-84896457748c.jpeg",
            "id": "6d635dea-d057-211b-ec77-84896457748c",
            "name": "8083eeec-3a5c-0904-7825-9e558e7612eb.jpeg",
            "size": 8918,
            "createdAt": "2020-09-18T22:12:40+00:00",
            "updatedAt": "2020-09-18T22:12:40+00:00",
            "expiredAt": "2020-09-19T22:12:40+00:00"
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92061071,
        "fio": {
            "first_name": "Анна",
            "second_name": "Аркадьевна",
            "last_name": "Хортюнова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/d1508d03-79e4-4e33-9db5-1b2238a259a4.jpg",
            "id": "d1508d03-79e4-4e33-9db5-1b2238a259a4",
            "name": "7501344c-cc63-a715-90be-a27f5416e430.jpg",
            "size": 6284,
            "createdAt": "2021-01-10T10:24:47+00:00",
            "updatedAt": "2021-01-10T10:26:09+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 11,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92112365,
        "fio": {
            "first_name": "Алёна",
            "second_name": "Игоревна",
            "last_name": "Храпкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92294057,
        "fio": {
            "first_name": "Татьяна",
            "second_name": "Олеговна",
            "last_name": "Ермакова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92357062,
        "fio": {
            "first_name": "Бекзод",
            "second_name": "Искандарович",
            "last_name": "Джумабаев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/7F0C64-%D0%94%D0%91.png",
            "id": "efb9dc33-7270-e0cb-980d-a37c050b2bc6",
            "name": "7F0C64-ДБ.png",
            "size": 8607,
            "createdAt": "2021-07-22T16:30:59+00:00",
            "updatedAt": "2021-07-22T16:30:59+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92420666,
        "fio": {
            "first_name": "Юлия",
            "second_name": "Максимовна",
            "last_name": "Галкина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/0ad9fbbc-3ea7-1502-70e5-960fd31c9440.jpg",
            "id": "0ad9fbbc-3ea7-1502-70e5-960fd31c9440",
            "name": "7b659af1-4a82-105d-be31-3f0f00d50349.jpg",
            "size": 17451,
            "createdAt": "2021-09-21T14:46:10+00:00",
            "updatedAt": "2021-09-21T14:48:22+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 34,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92498266,
        "fio": {
            "first_name": "Кристина",
            "second_name": "-",
            "last_name": "Болигарь"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/868FA5-%D0%91%D0%9A.png",
            "id": "8d5b41a1-3153-21db-7f1c-c318db9249ef",
            "name": "868FA5-БК.png",
            "size": 9083,
            "createdAt": "2021-09-28T16:53:03+00:00",
            "updatedAt": "2021-09-28T16:53:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 10057527,
        "fio": {
            "first_name": "Елена",
            "second_name": "Сергеевна",
            "last_name": "Лебедева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 960,
            "height": 960,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/2e1b63d3-890e-cf84-3bdc-f000b49921b7.",
            "id": "2e1b63d3-890e-cf84-3bdc-f000b49921b7",
            "name": "ios-pic",
            "size": 1582792,
            "createdAt": "2022-04-07T12:33:27+00:00",
            "updatedAt": "2022-04-07T12:33:27+00:00",
            "expiredAt": "2022-04-08T12:33:27+00:00"
        },
        "statistic": {
            "eventsCount": 2,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 10070478,
        "fio": {
            "first_name": "Антон",
            "second_name": "Борисович",
            "last_name": "Мальцев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 20,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91725122,
        "fio": {
            "first_name": "Диана",
            "second_name": "Олеговна",
            "last_name": "Кашкина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 25,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91821383,
        "fio": {
            "first_name": "Мария",
            "second_name": "Михайловна",
            "last_name": "Антонова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 14,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 78111,
        "fio": {
            "first_name": "Авигея",
            "second_name": "Денисовна",
            "last_name": "Долгушина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "г Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/3b94b294555d4e6eb1a9ac95a2c4dc26.jpgava1",
            "id": "a18d182f-8a5d-c8d4-9713-d94836759d47",
            "name": "3b94b294555d4e6eb1a9ac95a2c4dc26.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:18:05+00:00",
            "updatedAt": "2020-11-27T06:08:50+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 58,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 163249,
        "fio": {
            "first_name": "Екатерина",
            "second_name": "Олеговна",
            "last_name": "Гурьева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 93032634,
        "fio": {
            "first_name": "Мариам",
            "second_name": "Мишовна",
            "last_name": "Саргсян"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва, Красная пл, д 9",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/9CA841-%D0%A1%D0%9C.png",
            "id": "3c727358-f3ff-01ea-5a5a-04f418726998",
            "name": "9CA841-СМ.png",
            "size": 11126,
            "createdAt": "2022-02-17T12:25:59+00:00",
            "updatedAt": "2022-02-17T12:25:59+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 10,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 104559,
        "fio": {
            "first_name": "Эльвира",
            "second_name": "Григорьевна",
            "last_name": "Пташкина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/75ad22fda4c34403bbf419af9fa0bd27.jpgava1",
            "id": "e330a0ac-f436-cf87-5e7c-5446cedca11b",
            "name": "75ad22fda4c34403bbf419af9fa0bd27.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:14:39+00:00",
            "updatedAt": "2020-12-03T10:39:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 48,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 166697,
        "fio": {
            "first_name": "Алёна",
            "second_name": "Павловна",
            "last_name": "Семёнова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/2621eced40e94ebba701e158f0a7fe74.jpgava1",
            "id": "74b192b1-51ea-855f-ea73-a846aca33c0f",
            "name": "2621eced40e94ebba701e158f0a7fe74.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:21:22+00:00",
            "updatedAt": "2020-10-29T11:09:25+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 315112,
        "fio": {
            "first_name": "Урусова",
            "second_name": "Сослановна",
            "last_name": "Альбина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 7,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1292904,
        "fio": {
            "first_name": "Алиса",
            "second_name": "Геннадьевна",
            "last_name": "Миненкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": null,
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/d7cb64e0c1374c8ab4b6faa36c6169a8.jpg",
            "id": "b1ad55a5-5015-b6d3-b3c3-7fe6ffc267e8",
            "name": "d7cb64e0c1374c8ab4b6faa36c6169a8.jpg",
            "size": null,
            "createdAt": "2020-02-29T04:17:15+00:00",
            "updatedAt": "2020-11-27T11:16:48+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 1460538,
        "fio": {
            "first_name": "Наталия",
            "second_name": "Викторовна",
            "last_name": "Карасева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/1b29ffb66755493fa8e5bf74b371814a.jpg",
            "id": "2b23fe2b-72b8-edfe-bbcf-5ae453fb0d7a",
            "name": "1b29ffb66755493fa8e5bf74b371814a.jpg",
            "size": null,
            "createdAt": "2020-05-02T19:12:08+00:00",
            "updatedAt": "2020-10-20T02:04:07+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 13,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91564614,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Андреевна",
            "last_name": "Кожина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/11e278c8-b1c0-953e-de79-dcd72639910a.jpg",
            "id": "11e278c8-b1c0-953e-de79-dcd72639910a",
            "name": "7110b7e9-5c8c-3a8a-ffbd-cf777f93b7d0.jpg",
            "size": 17408,
            "createdAt": "2021-05-20T17:47:52+00:00",
            "updatedAt": "2021-05-20T17:48:31+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 92217524,
        "fio": {
            "first_name": "Ирина",
            "second_name": "Дмитриевна",
            "last_name": "Маргорина"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 12,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92220814,
        "fio": {
            "first_name": "Милана",
            "second_name": "Исмаиловна",
            "last_name": "Шептукаева"
        },
        "gender": null,
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 3,
            "volunteerRating": 4.3,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92349144,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Игоревна",
            "last_name": "Кикот"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/ABEF9F-%D0%9A%D0%90.png",
            "id": "254fd5db-f981-1f23-2ff7-1c1535cffb59",
            "name": "ABEF9F-КА.png",
            "size": 5753,
            "createdAt": "2021-07-14T07:27:51+00:00",
            "updatedAt": "2021-07-14T07:27:51+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 10,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92390459,
        "fio": {
            "first_name": "Амина",
            "second_name": "Башировна",
            "last_name": "Тимурзиева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/A2B854-%D0%A2%D0%90.png",
            "id": "9615721b-8dde-e0b4-1d0d-50b09b39b7cc",
            "name": "A2B854-ТА.png",
            "size": 8328,
            "createdAt": "2021-08-28T15:03:57+00:00",
            "updatedAt": "2021-08-28T15:03:57+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 45,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 198944,
        "fio": {
            "first_name": "Тимур",
            "second_name": "Ильдарович",
            "last_name": "Шафеев"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/imported\/pictures\/8371588356734695874acd2a9f17fc73.jpgava1",
            "id": "d284d931-e138-5e8c-38ed-962354d2e793",
            "name": "8371588356734695874acd2a9f17fc73.jpgava1",
            "size": null,
            "createdAt": "2020-02-29T02:28:35+00:00",
            "updatedAt": "2020-12-03T08:42:12+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 16,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 93060308,
        "fio": {
            "first_name": "Сергей",
            "second_name": "Вячеславович",
            "last_name": "Хрячков"
        },
        "gender": "M",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/c0c9fe79-ab18-2de3-6827-bdc3b99d264c.jpg",
            "id": "c0c9fe79-ab18-2de3-6827-bdc3b99d264c",
            "name": "074619d4-597f-5612-134e-728becfbfe2f.jpg",
            "size": 14548,
            "createdAt": "2022-05-16T20:06:38+00:00",
            "updatedAt": "2022-05-16T20:13:03+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 1016735,
        "fio": {
            "first_name": "Елена",
            "second_name": "Сергеевна",
            "last_name": "Корниенко"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 1,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91655964,
        "fio": {
            "first_name": "Чаян",
            "second_name": "Мергенович",
            "last_name": "Монгуш"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 25,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91711119,
        "fio": {
            "first_name": "Ольга",
            "second_name": "Александровна",
            "last_name": "Смельтер"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 28,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91742021,
        "fio": {
            "first_name": "Анна",
            "second_name": "Александровна",
            "last_name": "Захарова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 19,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92195080,
        "fio": {
            "first_name": "Виктория",
            "second_name": "Алексеевна",
            "last_name": "Бычкова"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": null,
            "height": null,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/default\/volunteer\/volunteer-icon.png",
            "id": "b171615d-91d2-4e79-8655-20a5845a24e8",
            "name": "volunteer-icon.png",
            "size": null,
            "createdAt": "2021-07-09T04:13:49+00:00",
            "updatedAt": "2021-07-09T04:13:49+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 16,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92291988,
        "fio": {
            "first_name": "Алексей",
            "second_name": "Денисович",
            "last_name": "Климов"
        },
        "gender": "M",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/65390471-8382-fea3-115c-19421f877517.jpeg",
            "id": "65390471-8382-fea3-115c-19421f877517",
            "name": "b948d1e2-2e65-7577-b1c5-5433ceb107ce.jpeg",
            "size": 19891,
            "createdAt": "2022-05-08T21:51:01+00:00",
            "updatedAt": "2022-05-08T21:51:05+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 12,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92413396,
        "fio": {
            "first_name": "Ангелина",
            "second_name": "Геловна",
            "last_name": "Гриневицкая"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "г Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620405,
            "y": 55.7540471
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/2A81F3-%D0%93%D0%90.png",
            "id": "af5cde41-ca04-b0f0-50b7-c3c2d3bd9c50",
            "name": "2A81F3-ГА.png",
            "size": 8122,
            "createdAt": "2021-09-07T11:44:46+00:00",
            "updatedAt": "2021-09-07T11:44:46+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 8,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 10090526,
        "fio": {
            "first_name": "Анастасия",
            "second_name": "Сергеевна",
            "last_name": "Слобода"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 250,
            "height": 250,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/77b9af90-e965-78e2-81fb-3399213c88cf.JPG",
            "id": "77b9af90-e965-78e2-81fb-3399213c88cf",
            "name": "428372eb-6fa2-43cc-2236-63934b1d040f.JPG",
            "size": 17329,
            "createdAt": "2022-04-17T09:46:10+00:00",
            "updatedAt": "2022-04-17T09:46:23+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 65,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 91971466,
        "fio": {
            "first_name": "Диана",
            "second_name": "Леонидовна",
            "last_name": "Рябикова"
        },
        "gender": "F",
        "settlement": {
            "country": "RU",
            "countryName": "Россия",
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/59623bf3-0bc5-5c1e-f5e7-4fcb901bc9c3.jpg",
            "id": "59623bf3-0bc5-5c1e-f5e7-4fcb901bc9c3",
            "name": "a48ee408-c497-1a39-e58b-77a107e2bfd7.jpg",
            "size": 9935,
            "createdAt": "2021-04-09T14:08:46+00:00",
            "updatedAt": "2021-04-09T14:09:20+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 18,
            "volunteerRating": 5,
            "organizerRating": 5
        },
        "canContact": true
    }, {
        "id": 91975500,
        "fio": {
            "first_name": "Ольга",
            "second_name": "Вячеславовна",
            "last_name": "Крейчик"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 140,
            "height": 140,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/images\/59c7cb0c-850c-a3b8-f37e-276142477c1f.jpg",
            "id": "59c7cb0c-850c-a3b8-f37e-276142477c1f",
            "name": "9e1d464d-ff05-f32b-99c0-c1843f0ca56e.jpg",
            "size": 7533,
            "createdAt": "2020-12-11T10:58:48+00:00",
            "updatedAt": "2020-12-11T10:59:24+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 15,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }, {
        "id": 92583630,
        "fio": {
            "first_name": "Мария",
            "second_name": "Максимовна",
            "last_name": "Ардашева"
        },
        "gender": "F",
        "settlement": {
            "country": null,
            "countryName": null,
            "title": "Москва",
            "region": 77,
            "municipality": null,
            "municipalityCode": null,
            "settlement": "Москва",
            "settlementCode": "7700000000000",
            "settlementDistrict": null,
            "settlementCodeDistrictCode": null,
            "street": null,
            "house": null,
            "flat": null,
            "fias": null,
            "x": 37.620393,
            "y": 55.75396
        },
        "iconFile": {
            "width": 300,
            "height": 300,
            "url": "https:\/\/storage.yandexcloud.net\/dobro-static\/prod\/avatars\/E3B5B7-%D0%90%D0%9C.png",
            "id": "0715e6c8-4ef0-76f8-5ecb-70141905a67f",
            "name": "E3B5B7-АМ.png",
            "size": 6218,
            "createdAt": "2021-10-19T03:36:04+00:00",
            "updatedAt": "2021-10-19T03:36:04+00:00",
            "expiredAt": null
        },
        "statistic": {
            "eventsCount": 5,
            "volunteerRating": 5,
            "organizerRating": null
        },
        "canContact": true
    }
]
`
