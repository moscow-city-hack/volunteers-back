package data

const HomeData = `
{
  "advantages": [
    {
      "id": "search",
      "title": "Удобство поиска",
      "description": "У нас настолько удобный поиск, что вы сможете найти все, что хотите и не хотите"
    },
    {
      "id": "star",
      "title": "Система оценок",
      "description": "У каждого волонтера и организации есть рейтинг, благодаря которому вы можете выбрать наиболее ответственных волонтеров"
    },
    {
      "id": "chat",
      "title": "Коммуникация",
      "description": "Организации и волонтеры могут общаться как между собой, так и с нуждающимися в помощи"
    }
  ],
  "reviews": [
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 5,
      "date": "07.06.2022"
    },
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 4,
      "date": "07.06.2022"
    },
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 5,
      "date": "07.06.2022"
    },
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 5,
      "date": "07.06.2022"
    },
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 4,
      "date": "07.06.2022"
    },
    {
      "image": "https://otvet.imgsmail.ru/download/6f91f9736f057f087d9219d38ed50fc1_i-23.jpg",
      "description": "Ну просто нет слов, лучший сервис на свете! Я воспользовался сервисом уже 18 раз!",
      "name": "Анна Белова",
      "stars": 5,
      "date": "07.06.2022"
    }
  ],
  "news": [
    {
      "text": "Игровые программы для детей в ТЖС и с ОВЗ",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "minutes": 6
    },
    {
      "text": "Игровые программы для детей в ТЖС и с ОВЗ",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "minutes": 1
    },
    {
      "text": "Игровые программы для детей в ТЖС и с ОВЗ",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "minutes": 45
    },
    {
      "text": "Игровые программы для детей в ТЖС и с ОВЗ",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "minutes": 2
    },
    {
      "text": "Игровые программы для детей в ТЖС и с ОВЗ",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "minutes": 12
    }
  ],
  "events": [
    {
      "location": "г. Москва, метро Оконченное, ул. Рощи, 255",
      "date": {
        "from": "15.06.2022",
        "to": "22.06.2022"
      },
      "text": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "favorite": true
    },
    {
      "location": "г. Москва, метро Оконченное, ул. Рощи, 255",
      "date": {
        "from": "15.06.2022",
        "to": "22.06.2022"
      },
      "text": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "favorite": false
    },
    {
      "location": "г. Москва, метро Оконченное, ул. Рощи, 255",
      "date": {
        "from": "15.06.2022",
        "to": "22.06.2022"
      },
      "text": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
      "image": "https://www.ya-roditel.ru/upload/medialibrary/1db/1db020c9dbd961252cea3e11331190e6.jpg",
      "favorite": false
    },
    {
      "location": "г. Москва, метро Оконченное, ул. Рощи, 255",
      "date": {
        "from": "15.06.2022",
        "to": "22.06.2022"
      },
      "text": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
      "favorite": false
    },
    {
      "location": "г. Москва, метро Оконченное, ул. Рощи, 255",
      "date": {
        "from": "15.06.2022",
        "to": "22.06.2022"
      },
      "text": "Отбор волонтеров Гуманитарные миссии #МЫВМЕСТЕ c Донбассом. Оказание помощи детям с девиантным поведением.",
      "favorite": true
    }
  ]
}
`
