FROM golang:1.17-buster as go
ENV CGO_ENABLED=0

FROM go as build
WORKDIR /build
COPY go.mod go.sum ./
COPY . .
RUN go build -o /bin/app ./cmd/volunteers-back/main.go

FROM alpine
COPY --from=build /bin/app /bin/app
ENTRYPOINT ["/bin/app"]